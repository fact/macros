function [b,t,p,dof,r] = calpls(x,y,nf)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
  
  t=ones(size(x,1),nf);
  p=ones(size(x,2),nf);
  w=ones(size(x,2),nf);
  c=ones(nf,1);
  b=ones(size(x,2),nf);

  // standard (Wold's) algorithm 
  
  w(:,1)=x'*y;
  w(:,1)=w(:,1)/sqrt(w(:,1)'*w(:,1));
  t(:,1)=x*w(:,1); 
  c(1)=y'*t(:,1)/(t(:,1)'*t(:,1));
  p(:,1)=x'*t(:,1)/(t(:,1)'*t(:,1));
  x=x-t(:,1)*p(:,1)';
  b(:,1)=w(:,1)*inv(p(:,1)'*w(:,1))*c(1);
  
  for ind=2:nf;
  w(:,ind)=x'*y;
  w(:,ind)=w(:,ind)/sqrt(w(:,ind)'*w(:,ind));
  t(:,ind)=x*w(:,ind); 
  c(ind)=y'*t(:,ind)/(t(:,ind)'*t(:,ind));
  p(:,ind)=x'*t(:,ind)/(t(:,ind)'*t(:,ind));
  x=x-t(:,ind)*p(:,ind)';
  b(:,ind)=w(:,1:ind)*inv(p(:,1:ind)'*w(:,1:ind))*c(1:ind);
  end

  dof=[1:1:nf]';
  
  // rajout de r: 28juil16
  // r correspond au r de De Jong: t=x*r
  // ici: t=x*w*inv(p'*w) donc r=w*inv(p'*w)
  
  r=w*inv(p'*w);

endfunction
