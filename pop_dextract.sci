function [dmatrix] = pop_dextract(method,x,varargin)

   // distributed under the CeCILL-C license
   // copyright INRA-IRSTEA 2013
  
  

    if  method=='iirp' & argn(2)==2 then
    elseif (method=='eros'| method=='epoe')  & argn(2)==3 then 
        classes_echant0=string(varargin(1));
    elseif (method=='epo'| method=='epoi') & argn(2)==4 then 
        classes_echant0=string(varargin(1));
        classes_perturb=string(varargin(2));
        classes_echXpert=classes_echant0+classes_perturb;
    else error('wrong number of input arguments in the pop_d_extract method');
    end
 
 
    
    if method ~='iirp' then
        classes_echant=str2conj(classes_echant0);
    end
    
    if  method ~='iirp' & method ~='eros' &method ~='epoe' then
        classes_perturb=str2conj(classes_perturb);
        classes_echXpert=str2conj(classes_echXpert);
    end
 
    xg=div(x);
    [nxg,pxg]=size(xg.d); 
     
    if method=='epo'| method=='epoi' then 
      [groups_perturb] = groupcreate(xg,classes_perturb);     //groups_perturb est une liste 
      ncl_perturb=size(groups_perturb);                        //ex: nclasses=8 car 8 niveaux de température  
      disj_perturb=conj2dis(classes_perturb); 
    end;
    if method=='epoi' then  
      [groups_echXpert] = groupcreate(xg,classes_echXpert);               //groups_echXpert est une liste 
      [codeech_echXpert]= groupcreate(classes_echant,classes_echXpert);      // pour faire suivre le code échantillon
      ncl_echXpert=size(groups_echXpert);                          
      disj_echXpert=conj2dis(classes_echXpert);         
    end 
    if method~='iirp' then 
      disj_echant=conj2dis(classes_echant);     
      [groups_echant] = groupcreate(xg,classes_echant);      //groups_echant est une liste
      ncl_echant=size(groups_echant);                         //ex: nclasses3=10 car 10 pommes
      [groups_classes_ech]=groupcreate([1:1:nxg]',classes_echant);
           
      for i=1:ncl_echant;
        if ~isdef('xgtotal_classesepo2') then
            xgtotal_classesepo2=groups_classes_ech(i).d;
        else 
            xgtotal_classesepo2=[xgtotal_classesepo2;groups_classes_ech(i).d];
        end
      end
    end                       



    // calcul de D: 
      
    if method=='iirp' then
      // Cas de IIR: D est donnée directement  
      D=xg;
      
     
    elseif method =='epo' then 
      // 1ere construction de D obtenue en moyennant chaque groupe de temperature puis en soustrayant la 1° moyenne aux autres
      // = calcul de l'EPO; nclasses=regroupement par grandeur d'influence (ex: 8 températures     
      for i=1:ncl_perturb;
        xgi=mean(groups_perturb(i).d,1);     // moyennes par groupes 
        if ~isdef('xgtotal') then
            xgtotal=xgi;
        else    
            xgtotal=[xgtotal;xgi];
        end
      end
      // calcul de D conforme à la publi EPO; entre autre D a 8 obs. dont une nulle qu'on enlève ici   
      D.d=xgtotal(2:size(xgtotal,1),:)-ones(size(xgtotal,1)-1,1)*xgtotal(1,:);
      D.v=xg.v;
      D.i='mean of group' + string([2:1:ncl_perturb]');
      // NB: D a comme dimension/lignes le nbre de niveaux de la GI - 1 



    elseif method=='epoe' then
      // 2eme construction de D obtenue en centrant les mesures faites sur un même objet
      // proposée initialement; permet de retrouver Wilks de la publi EPO/2003
      // NB: EPO2 donne un résultat identique à EROS après projection orthogonale 
      // différences: D est + grande; plus de vecteurs propres avec EPO2 qu'avec EROS 
      // les valeurs propres de EPO2 et EROS sont différentes     
      for i=1:ncl_echant;
        xgi=centering(groups_echant(i));
        if ~isdef('xgtotal') then
            xgtotal=xgi.d;
        else 
            xgtotal=[xgtotal;xgi.d];
        end
      end
      D.d=xgtotal;
      D.v=xg.v;
      D.i=xgtotal_classesepo2;
      // NB: (1) on est OBLIGES de centrer groups_echant(i) pour enlever l'effet échantillon;
      //     (2) ensuite on ne PEUT PAS moyenner, car vecteur nul!
      //     (3) donc xgtotal et D ont même dimension que x!
    
    
      
    elseif method=='epoi'; 
      // 3eme construction de D, intermédiaire entre les deux précédentes
      // avec des groupes définis par: ech. x perturb. 
      for i=1:ncl_echXpert;
        xgi=mean(groups_echXpert(i).d,1);   
        code_matrix=codeech_echXpert(i);
        code2_matrix=code_matrix(1);          // on ne garde qu'une valeur de code_matrix
        if ~isdef('xgtotal') then
            xgtotal=xgi;
        else 
            xgtotal=[xgtotal;xgi];
        end
        if ~isdef('codeech_xgtotal') then
            codeech_xgtotal=code2_matrix;
        else
            codeech_xgtotal=[codeech_xgtotal;code2_matrix];  
        end
      end 
      groups_xgtotal=groupcreate(xgtotal,str2dis(codeech_xgtotal));   // nouveau regroupement, par échantillon
      for i=1:size(groups_xgtotal);
        xgtotal_i=centering(groups_xgtotal(i));
        if ~isdef('xgtotal_centre') then
            xgtotal_centre=xgtotal_i.d;
        else 
            xgtotal_centre=[xgtotal_centre;xgtotal_i.d];
        end
      end  
      D.d=xgtotal_centre;
      D.v=xg.v;
      D.i=codeech_xgtotal;

    
        
    elseif method=='eros' then   
      for i=1:ncl_echant;
        xgi=centering(groups_echant(i));
        if ~isdef('xgtotal') then
            xgtotal=xgi.d'*xgi.d;
        else 
            xgtotal=xgtotal+ xgi.d'*xgi.d;   //cumul des matrices de variance-covariance
        end
      end  
      D.d=xgtotal;
      D.v=xg.v;
      D.i=xg.v;   
          
    end   

    // sorties Div:
    D=div(D);

    dmatrix=D;

    
endfunction


