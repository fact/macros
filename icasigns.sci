function [ICs,Scores2]=icasigns(signal,scores,Options);


  // Adjust signs of loadings and scores
  // USAGE :
  // [Signal,Scores]=icasigns(Signal,Scores,Options);


  signal=div(signal);
  scores=div(scores);
  
  Signal=signal.d;
  Scores=scores.d;

  if argn(2)>2 then

       if isfield(Options,'criterion');
          select Options.criterion
            // Max intensity (1) / Sum surface (2)
            case 1
                criterion=1;
            case 2
                criterion=2;
            else
                criterion=1;
            end
          else
            criterion=1;
          end;

       if isfield(Options,'basis') then
          select Options.basis
            case 1
                basis=1;   // scores
            case 2
                basis=2;   // signals
            else
                basis=1;
           end;
       else
           basis=1;
       end
    
    
  else
        criterion=1;
        basis=1;  
  end

  ICs=size(Signal,2);

  for nIC=1:ICs
    select criterion
        case 1         // Max Intensity
            select basis 
                case 1 // Scores
                    maxInd=find(abs(Scores(:,nIC))==max(abs(Scores(:,nIC))));    
                    if Scores(maxInd,nIC) < 0 then
                        Scores(:,nIC)=-1*Scores(:,nIC);
                        Signal(:,nIC)=-1*Signal(:,nIC);
                    end
                case 2 // Signals
                    maxInd=find(abs(Signal(:,nIC))==max(abs(Signal(:,nIC))));
                    if Signal(maxInd,nIC) < 0 then
                        Signal(:,nIC)=-1*Signal(:,nIC);
                        Scores(:,nIC)=-1*Scores(:,nIC);
                    end
            end
        case 2         // Surface
            select basis 
                case 1 // Scores
                    SumVals=sum(Scores(:,nIC));
                    if SumVals < 0
                        Scores(:,nIC)=-1*Scores(:,nIC);
                        Signal(:,nIC)=-1*Signal(:,nIC);
                    end
                case 2 // Signals
                    SumVals=sum(Signal(:,nIC));
                    if SumVals < 0 then
                        Scores(:,nIC)=-1*Scores(:,nIC);
                        Signal(:,nIC)=-1*Signal(:,nIC);
                    end
            end
    end

  end

  Scores2.d=Scores;
  Scores2.i=scores.i;
  Scores2.v=scores.v;
  
  ICs.d=Signal;
  ICs.i=signal.i;
  ICs.v=signal.v;
  
  Scores2=div(Scores2);
  
  ICs=div(ICs);


endfunction
