function c=%div_e(a,b,x)

 
         //function c=%div_e(a,b,x)
 
         // extraction de données 
         x=div(x);
         c.d=x.d(a,b);
         c.i=x.i(a);
         c.v=x.v(b);
 
         c=div(c); 
         
  
         if size(c.d,1)==1 then
             c=c';
         end
       
         c.infos=x.infos;


endfunction
            

