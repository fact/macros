function accom1_type =acom1(col,ndim,centred)

// fonction mise à jour le 6/06/16: elle correspond à acom3
//accom1    - analyse en co-inertie et composante commune
//function accom1_type =accom1(collection,ndim);
//
//Input argument:
//==============
//collection: ARRAY OF SAISIR matrices
//example of building a collection : collection col(1)=table1;col(2)=table2; col(3)=table3
//in which table1, table2, table are SAISIR structure
//Each table must include the same observations, but not necessarily the same variables.
//ndim : integer . Number of dimensions computed
//
//Output argument:
//===============
//(Let n be the number of observations, and t the number of tables)
//(Let WHOLE be the matrix of appended tables(dimensions n x m))
//
//accom1_type with fields:
//   - global with fields:
//           -loadings: global loadings applicable on WHOLE (m x ndim)
//           -score: global scores (n x ndim)
//   - individual 1x t struct array with fields:
//           -score: local scores of each table (n x ndim)
//           -loadings: local loadings of each table
//           -mean: mean of each table
//           -TableNorm: Frobenius norm of each table
//           -Table_weight
//   -ExplainedSumSquare:explained sum of square for each table and each
//           dimension (txndim)
//
//   -Trajectory: concatened matrix of individual scores
//    (nxt rows and ndim columns), useful for barycenter representation (see example)
//
//   -Size: number of variables in each table(1 x t)
//
//Adapted from
//Chessel D, Hanafi M: Analyses de la Co-inertie de K nuages de points.
//Revue de Statistique Appliquée XLVI: 35-60 (1996).
//
//Typical example:
//================
//let T1,T2,T3 be SAISIR matrices with the same number of
//rows
//col(1)=T1;col(2)=T2;col(3)=T3;
//res=accom1(col,5); //// 5 dimensions asked
//map(res.global.score,1,2);               // plot of global score , dimensions 1 and 2
//figure
//map(res.individual(3).score,1,2);            // plot of local score table 3 (T3)
//[n,p]=size(res.trajectory.i);
//id=create_xgroup1(res.trajectory,3,p);       // identifying the tables
//figure
//barycenter_map(res.trajectory,1,2,id);       // barycenter representation



  if typeof(col) ~= 'list' then
      error('the input should be a list')
  end

  if argn(2)<3 then   // centrage par défaut 
      centred=1;
  end

  ntable=length(col);
  U=list(); 
  xmean=list();
  whole=[];
  
  // initialisation des sorties 
  accom1_type.global_scores=[];
  accom1_type.global_loadings=[];
  accom1_type.individual_scores=list();
  accom1_type.individual_loadings=list();
  accom1_type.individual_mean=list();
  accom1_type.individual_tablenorm.d=[];
  accom1_type.individual_tablenorm.i=[];
  accom1_type.individual_tableweight=list();
  accom1_type.trajectory=[];
  accom1_type.tables_size=[];
  accom1_type.tables_scores=[];
  accom1_type.tables_corr=[];
  
  
  // centering, initialization
  for i=1:ntable;
     col(i)=div(col(i));
     aux=col(i);
     if centred==1 then 
        [aux, mean_tmp]=centering(aux); // aux est centrée comme acom1
     elseif centred==0 then 
        [nul, mean_tmp]=centering(aux); // aux n'est pas centrée, comme accom3.m; 6/06/16
     end
     xmean(i)=mean_tmp;
     xnorm(i)=sqrt(sum(aux.d.*aux.d));
     col(i).d=aux.d/xnorm(i);
     if i==1 then
         whole=col(1);
     else
         whole=[whole col(i)];
     end
     [n1,p1]=size(col(i).d);
     Ui.entries=zeros(p1,1); 
     U(i)=Ui;
     xsize(1,i)=p1;      		// respective number of variables of each table
  end;
  
  xwhole=whole;
  
  for dim=1:ndim
     p=pcana(xwhole);
     eigenvec1=p.eigenvec.d(:,1);
     gloadings(:,dim)=eigenvec1 ;     		  // global loadings
     xstart=1;
     for table=1:ntable;
         xxend=(xstart+xsize(table)-1);
         aux=eigenvec1(xstart:xxend);
         table_weight(table,dim)=sqrt(sum(aux.*aux));  // added 11 june 2009
         vec=aux/table_weight(table,dim);              // portion of eigenvector
         aux1=U(table).entries;
         aux1(:,dim)=vec;
         U(table).entries=aux1;
         xwhole.d(:,xstart:xxend)=xwhole.d(:,xstart:xxend)-xwhole.d(:,xstart:xxend)*vec*vec';
         xstart=xxend+1;
     end
  end;

  // --- Computing the scores ------
  xdim='dim'+ string([1:1:ndim]')
  glob_load.d=gloadings;
  glob_load.i=whole.v;
  glob_load.v=xdim;
  
  accom1_type.global_loadings=div(glob_load); 
  accom1_type.global_scores=whole * accom1_type.global_loadings;

  xstart=1;
  trajectory=[];
  clear aux;
  for table=1:ntable;
      xxend=xstart+xsize(table)-1;
      aux1=U(table).entries;
      aux.d=matrix(aux1,size(aux1));
      aux.i=col(table).v;
      aux.v=xdim;
      aux=div(aux);   
      accom1_type.individual_scores(table)=col(table) * aux;
      
      ss=sum(col(table).d.*col(table).d);              // total sum of square of this table
      aux6=accom1_type.individual_scores(table).d;
      explainedss=sum(aux6.*aux6,1)/ss;       // proportion sum of square explained by the individual scores
      explained(table,:)=explainedss;

      accom1_type.individual_loadings(table)=aux;

      accom1_type.individual_mean(table)=xmean(table);
      accom1_type.individual_tablenorm.d=[accom1_type.individual_tablenorm.d; xnorm(table)];
      accom1_type.individual_tablenorm.v='Frobenius norm';
      accom1_type.individual_tablenorm.i= [accom1_type.individual_tablenorm.i;'table'+string(table)];
      accom1_type.individual_tablenorm=div(accom1_type.individual_tablenorm);
      
      aux4=accom1_type.global_loadings(xstart:xxend,:);
      aux5=col(table)*aux4;
      aux5.i=string(table)+aux5.i;
      if table==1 then 
          trajectory=aux5;
      else
          trajectory=[trajectory ;aux5];
      end
      xstart=xxend+1;
      
      accom1_type.individual_tableweight(table)= div(table_weight(table,:),'table weight','dim'+string([1:1:ndim]'));
      accom1_type.individual_tableweight(table).i='table'+string(table);
  end

  accom1_type.explained_sum_squares.d=explained;
  accom1_type.explained_sum_squares.v=xdim;
  aux=num2str((1:ntable)',2);
  accom1_type.explained_sum_squares.i='table'+aux;
  accom1_type.explained_sum_squares = div(accom1_type.explained_sum_squares);
  
  accom1_type.trajectory=trajectory;
  
  accom1_type.tables_size.d=xsize';
  accom1_type.tables_size.v='number of variables';
  accom1_type.tables_size.i='table'+string([1:1:max(size(xsize))]');
  accom1_type.tables_size=div(accom1_type.tables_size);

  globalscore=accom1_type.global_scores;

  for table=1:ntable;
      localscore=accom1_type.individual_scores(table);
      aux=covmat(globalscore,localscore);
      tablescore.d(table,:)=diag(aux.d)';
      aux=corrmat(globalscore,localscore);
      tablecor.d(table,:)=diag(aux.d)';
  end
  tablescore.i=accom1_type.explained_sum_squares.i;
  tablescore.v='dim'+string((1:ndim)');
  tablecor.i=accom1_type.explained_sum_squares.i;
  tablecor.v=tablescore.v;

  accom1_type.tables_scores=div(tablescore);
  accom1_type.tables_corr=div(tablecor);

  endfunction

