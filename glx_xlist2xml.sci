function glx_xlist2xml(x,xmlfile)

    // x est une matrice 3D, de dimensions (n x p x q)
    doc = xmlDocument(xmlfile);
    root=xmlElement(doc,"root");
    doc.root=root;
   
    
    // Initialisation
    n=length(x);
    

    // construction de la structure
    for i=1:n;
        name='plan'+string(i)
        plani=x(i);
        [ni,qi]=size(plani);
        
        // mise au format csv: separation par ','
        // note: doit pouvoir bien s'améliorer!
        xd=[];
        for j=1:ni;
            xdj=[];
            for k=1:qi-1;
                xdj=xdj + string(plani(j,k)) +',';
            end
            xdj=xdj+string(plani(j,qi)); 
            xd=[xd;xdj];
        end
        
       
        execstr(msprintf("%s=xmlElement(doc,name)",name))
        
        execstr(msprintf('doc.root.children(i)=%s;',name));
   
        doc.root.children(i).content=string(xd);
       
    end
    
    xmlWrite(doc,xmlfile,%t)

    xmlDelete('all')

endfunction



