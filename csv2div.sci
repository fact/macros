function [data] = csv2div(filename,separator,decimal)                                    

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
  // reads a .csv file and converts it to .div
  // by default: separator = ';' decimal = ',' 
  // the first line and the first column of the .csv file must be labels                                                
  // the cell in position (1,1) is omited 
  // updated: 2015, december, 18 by JMR
  
  if argn(2)<3 then
      decimal=',';
  end
  
  if argn(2)<2 then
      separator=';';
  end
   
  m=csvRead(filename,separator,decimal,'string');
  
  data.d=strtod(strsubst(m(2:$,2:$),'D','E'));  // notation scientifique 
  data.i=m(2:$,1);
  
  //data.i=stripblanks(data.i);
  data.v=m(1,2:$)';
  //data.v=stripblanks(data.v);

  data=div(data);

endfunction                                                                              
                                        

