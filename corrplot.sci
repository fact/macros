function correl_matrix=corrplot(x_scores,col1,col2,varargin)
    
  //correlation_plot       - Draw a correlation between scores and tables
  //function handle=correlation_plot(scores,col1,col2, X1,X2, ...);
  //
  // Input arguments:
  // ==============
  // scores:                  - ORTHOGONAL scores obtained by multidimensional analysis
  // col1 and col2:           - Indices (ranks) of the scores to be plotted (integer number)
  // X, X2, ...    - Arbitrary number of tables giving the variables to be plotted
  // The number of rows in the scores and other tables must be identical
  //
  //The function displays the correlation circle, with a different color for
  //each table in X1, X2 ...
  //A dotted line gives the level of 50// explained variable.
  //If the input argument "scores" have non-orthogonal columns the graph is normally incorrect
  //and an error message is displayed.

  // identification des 2 axes 

  global newfig;

  scores=div(x_scores);
  scores = scores(:,[col1;col2]);  
  
  // nombre de tables 
  ntable = argn(2)-3;    

  // couleurs pour représenter les variables des différentes tables 
 // couleur = [0,0,0;0,0,1;0,0.7,0;1,0,0;0.5,0.5,0;0.5,0,0.5;0,0.5,0.5;0.25,0.25,0.25];
 // couleur=[couleur;0.5,0,0;0,0.5,0;0,0.5,0;0.1,0.2,0.3;0.3,0.2,0.1;0.5,0.5,0.8;0.1,0.8,0.1];
  
  if newfig=='T' then
     figure();
  end
  
  // cercles de corrélation: rayon = 1 et 0,71
  pi = 3.1416;
  t = 0:pi/20:2*pi;
  plot2d(sin(t),cos(t));
     plot([0 0],[-1.2 1.2],'k');
  plot([-1.2 1.2],[0 0],'k'); 
  
  
  //test d'orthogonalité entre scores
  orthog=scores.d(:,1)'*scores.d(:,2);    // JCB 1 juillet (on estime l'orthogonalité, pas la corrélation)
  aux=orthog**2;
  if (aux**2)>0.05 then
    disp(["Inner product between the 2 scores = ",string(aux*aux)]);
    error("Warning : the scores are not sufficiently orthogonal to allow a correct representation");
  end;
  
  // ajustement général de la figure 
  a=gca(); 
  a.data_bounds=[-1.2,-1.2;1.2,1.2];
  a.x_label.text=scores.v(1);
  a.x_label.font_size=2;           
  a.y_label.text=scores.v(2);
  a.y_label.font_size=2;
  a.font_size=0;
  a.font_foreground=0;
  a.box='on';
  b=gcf();
  b.background=-2;      // blanc!
  
  // impression des variables
  
  // si plus d'une table
  if ntable>1 then
    for i = 2:ntable
      table_i=div(varargin(i));
      [n,q]=size(table_i.d);
      t = gca();
      t.font_size=1;
      t.font_foreground = pmodulo(i-1,15)+1;
      cor = corrmat(table_i,scores);       // corrélation entre les tables et les scores
    
      for j = 1:q;   
        xstring(cor.d(j,1),cor.d(j,2),cor.i(j));   
      end;
    end
  end 
   
  // 1° table  - faite à la fin pour avoir les légendes en noir 
  table_1=div(varargin(1));
  [n,q]=size(table_1.d);
  t = gca();
  t.font_size=1;
  t.font_foreground = 1;
  cor = corrmat(table_1,scores);       // corrélation entre les tables et les scores
  correl_matrix=cor;
    
  for j = 1:q;   
      xstring(cor.d(j,1),cor.d(j,2),cor.i(j));   
  end;
   
 

endfunction

