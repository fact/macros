function ca_type=ca(x_contingency);
//ca         - CORRESPONDENCE ANALYSIS
//function ca_type=ca(x_contingency);
//Compute correspondence analysis from the contingency table in x_contingency
//If only xgroupings are available, the contigency table must be computed
//before using this function (see for example function "contingency_table")
//
// ==============================================================
//Fields of the output
//score          :CA scores of rows followed by CA scores of columns
//eigenval       :eigenvalues, percentage of inertia, cumulated percentage
//contribution   :contribution to the component rows, then columns
//squared_cos    :squared cosinus row, then columns
//khi2           :khi2 of the contingency table
//df             :degree of freedom
//probability    :probability of random values in contigency table
// ==============================================================
//
//The identifiers of rows of 'score' (which are the identifiers of rows and columns of N (N.i and N.v)
//are preceded with the letter 'r' or 'c'.
//It is therefore possible to use color for emphazising row and columns in
//the simultaneous biplot of rows and columns
//
//Source         : G. Saporta. Probabilités, analyse des données et statistiques.
//               : Edition Technip, page 198 and followings.
//REMARK : use function "ca_map" to plot the biplot observation/variable


  N=div(x_contingency)
  [n,p]=size(N.d);
  transpose_flag=0;

  // working on the more convenient matrix
  if(n<p)
    transpose_flag=1;
    N=N';
   [n,p]=size(N.d);
  end

  D2=sum(N.d,1);  // (p x 1)
  D1=sum(N.d,2);   // (n x 1)
  tot=sum(N.d);

  D1_1=diag(ones(n,1)./D1);
  D2_1=diag(ones(p,1)./D2');

  aux=D2_1*N.d'*D1_1*N.d;

  [S,D,V]=svd(aux);
  clear S;

  // Partie pouvant être simplifiée (svd classe les valeurs-propres)
  // question: pourquoi enlever le prenier vecteur-propre?
  // Warning function eig does not sort the eigenvalues and eigenvectors!!
  [eigenval.d, index]=gsort(abs(diag(D)'));// eigenvalues in increasing order!
  [n1,p1]=size(eigenval.d);
  eigenval.d=eigenval.d(2:p1); // removing trivial eigenvalue
  eigenval.i='eigenvalues';
  eigenvec.d=V(:,index);
  eigenvec.d(:,1)=[];// removing trivial eigenvector

  // computing score for factorial representation
  diag_ev=diag((eigenvec.d'*diag(D2)*eigenvec.d)/tot);
  [n_diagev,q_diagev]=size(diag_ev);
  r=sqrt(ones(n_diagev,q_diagev)./diag_ev);//Normalizing factor (see Saporta , page 204));
  r=diag(r)*diag(sqrt(eigenval.d));
  column_score.d=eigenvec.d*r;
  [n2,p2]=size(eigenval.d);
  row_score.d=D1_1*N.d*column_score.d*diag(ones(n2,p2)./(sqrt(eigenval.d)));
  [n,p]=size(row_score.d);
  row_score.i=N.i;
  column_score.i=N.v;
  row_score.v=string([1:1:p]')+'PC#';
  column_score.v=row_score.v;
  column_score.i=column_score.i +'c';
  row_score.i=row_score.i + 'r';
  
  row_score=div(row_score);
  column_score=div(column_score);

  if(transpose_flag==0)   
     score=[row_score; column_score];  //rappend
  else
     score=[column_score;row_score];  //rappend
  end

  ca_type.stats=[];

  ca_type.scores=[];
  ca_type.scores=score;// a single saisir file by merging scores of rows and columns
  
  [n_score,q_score]=size(score.d);

  row_contribution.d=diag(D1)*row_score.d.*row_score.d* diag(ones(n2,p2)./eigenval.d)/tot
  row_contribution.i=N.i;
  row_contribution.v=row_score.v;
  row_contribution=div(row_contribution);

  column_contribution.d=diag(D2)*column_score.d.*column_score.d*diag(ones(n2,p2)./eigenval.d)/tot
  column_contribution.i=N.v;
  column_contribution.v=column_score.v;
  column_contribution=div(column_contribution);


  //computing squared cos
  
  aux=score.*score;

  norm2=sum(aux.d,'c');
  squared_cos.d=diag(ones(n_score,1)./norm2)*aux.d;
  squared_cos.i=score.i;
  squared_cos.v=score.v;

  //computing cumulated eigenvalues and inertia
  clear aux;
  aux(1)=eigenval.d(1);

  for i=2:p
    aux(i)=aux(i-1)+eigenval.d(i);
  end

  aux=aux';
  khi2=sum(eigenval.d)*tot;
  eigenval.d=[eigenval.d;eigenval.d/aux(p);aux/aux(p)];
  eigenval.i=['eigenvalues';'percentage of inertia';'percentage of cumulated inertia'];
  eigenval.v=row_score.v;
  ca_type.stats=eigenval;

  if(transpose_flag==0)
    ca_type.contributions=[];
    ca_type.contributions=[row_contribution;column_contribution]; //rappend
  else
    ca_type.contributions=[];
    ca_type.contributions=[column_contribution;row_contribution]; //rappend
  end

  ca_type.squared_cos=squared_cos;
  ca_type.khi2.d=khi2;
  ca_type.khi2.i='Khi2 value of the contingency table';
  ca_type.khi2.v='Khi2 value of the contingency table';

  [n,p]=size(N.d);
  df.d=(n-1)*(p-1);
  df.i='degree of freedom';
  df.v='degree of freedom';
  P.d=(1-cdfchi("PQ",ca_type.khi2.d,df.d));
  P.i='Probability';
  P.v='Probability';
  ca_type.df=df;
  ca_type.probability=P;

  // mise au format Div:
  ca_type.stats=div(ca_type.stats);
  ca_type.scores=div(ca_type.scores);
  ca_type.contributions=div(ca_type.contributions);
  ca_type.khi2=div(ca_type.khi2);



endfunction

