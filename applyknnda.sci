function [res] = applyknnda(knnmodel,testx0,varargin)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
  //initialisation
  calx=knnmodel.xcal;
  caly=knnmodel.ycal;
  nn=knnmodel.knn;
  scal=knnmodel.scale;
  
  testx=div(testx0);
  
  if argn(2)==3 then
      testy0=varargin(1);
      //pause
      testy=div(testy0);
  end
  
  nbcl = size(caly.d,2);

  [n,p] = size(calx.d);
  [m,q] = size(testx.d);
  
  if p~=q then
      error('not the same number of variables in cal and test datasets')
  end

  yh.d=classknnda(calx.d,caly.d,testx.d,nn,scal);
  yh.i=testx.i;
  yh.v=caly.v;
  yh=div(yh);


  res.yclass=[];
  res.yclass=yh;
  
  if argn(2)==3 then
    conf.d = yh.d'*testy.d;
    conf.i='pred_'+string([1:1:nbcl]');
    conf.v='ref'+string([1:1:nbcl]');
    conf=div(conf);
    err_pcent=100*(m-sum(diag(conf.d)))/m;
    res.conf=[];
    res.conf=conf;
    res.err_pcent=err_pcent;    
  end
  

 
 
endfunction
