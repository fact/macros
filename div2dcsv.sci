function x_out=div2dcsv(x_in,filename,separator,decimal)

   // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
  // x_out = div2dcsv(filename,x_in,separator,decimal)
 
  // writes a div structure (x_in) on the csv file (filename)
  // decimal and separator are optional parameters 
  // updated: 2015, august, 06. by JMR


  x=div(x_in);

  if argn(2) < 4 then
      decimal = '.';
  end
  if argn(2) < 3 then
      separator = ',';
  end

  m = string(x.d);

  if decimal~='.' then
      m=strsubst(m,'.',decimal);    
  end

  csvWrite(m, filename, separator);
    
  // sortie div
  x_out=div(x);  // ne sert que pour valider le script de test 


endfunction

 
