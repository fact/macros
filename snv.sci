function [ xsnv ] = snv(xi)

  //function[xsnv]=s_snv(xi)
  //
  // This function performs a standard normal variate correction
  // i.e. for each spectrum, mean=0 and std=1
  //
   //Inputs:
  //-------
  // x_cal:              calibration dataset
  //
  //
  //Outputs:
  //--------
  // xsnv.d:                 corrected input matrix
  // xsnv.v:                 variables identification
  // xsnv.i:                 samples identification
  //
  //
  // references: Barnes, 1989
  //
  // JC Boulet updated 2014, sept, 5


  x=div(xi)


  [n,q]=size(x.d);

  x_min=min(x.d);

  x_mean=mean(x.d,'c');

  x1=x.d- x_mean*ones(1,q);   //correction par ligne=spectre
  var=variance(x1,'c');
   
  x2=x.d;
  for i=1:n;
     if abs(var(i)) > %eps then 
        x2(i,:)=x1(i,:)./(sqrt(var(i))*ones(1,q)); 
     end
  end


  xsnv.d=x2;
  xsnv.i=x.i;
  xsnv.v=x.v;
  
  
  xsnv=div(xsnv);


endfunction

