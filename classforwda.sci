function [res_classforwardda] = classforwda(x,y,lv)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  [n,q]=size(x);    //nrow=n     ncol=q 

  selected=zeros(q,1);     // selected = variables déjà choisies ou à ne pas prendre (si =1)
  y_conj=dis2conj(y);
  

  for i=1:lv    // lv=nbre max de variables à introduire
    bestcorrect=0;
    flag=0         // pas choisi de nlle variable 
    for j=1:q           //col=j
      ncorrect=zeros(q,1);               // nbre de bien classés pour chaque variable
      if selected(j)==0 then             // pour ne pas rechoisir une variable
         if ~isdef('bestchoice')  then
             currentchoice=j;
         else  
             currentchoice=[bestchoice;j];   // modèle testé avec la variable i
         end

         // classify de Matlab remplacé par memberpred de Fact
         classed=memberpred(x(:,currentchoice),y,x(:,currentchoice),y,0,1);
         classed2=dis2conj(classed);        // classed est maintenant conjonctif 
         ncorrect=sum(classed2==y_conj);    // nbre total de bien classés   
         
         if ncorrect>bestcorrect then 
           flag=1;                            // on a choisi une nlle variable
           bestcorrect=ncorrect;              // ajustement du niveau de bonnes classifications
           candidate=j;                       // sélection (temporaire) de la variable 
         end;   
      end;      
    end;
   
   // mise à jour de la sélection 
    
    if flag==1  then
      selected(candidate)=1;               // vecteur (q x 1) avec des 1 pour les variables choisies
      if ~isdef('bestchoice') then
          bestchoice=candidate;
      else   
          bestchoice=[bestchoice;candidate];   // vecteur (lv x 1) avec les n° des variables choisies
      end 
      if ~isdef('loadings') then 
          loadings=zeros(q,1);
      else 
          loadings=[loadings zeros(q,1)]; 
      end
      loadings(candidate,i)=1; 
    end
    flag=0;


  end
 
  
  // sorties: 

  res_classforwardda.scores=x*loadings;
  res_classforwardda.rloadings=loadings;


   
endfunction
