function dotcmap(x,col1,col2,startpos,endpos,varargin)
  
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013 

  if argn(2)==5 then
      colored_map_options(x,col1,col2,startpos,endpos,'dot')
  elseif argn(2)==6 then
      colored_map_options(x,col1,col2,startpos,endpos,'dot',varargin(1),'')
  elseif argn(2)==7 then
      colored_map_options(x,col1,col2,startpos,endpos,'dot',varargin(1), varargin(2))
  else
      error('wrong number of input arguments in colored_map') 
  end
  
endfunction


