function [res_pred]=popapply(popmodel,n_pop_eigenvect,xtest,varargin)
    
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
  
   model_reg=popmodel.pls_models(n_pop_eigenvect+1);
   if argn(2)==3 then
     res_pred=regapply(model_reg,xtest);
   elseif argn(2)==4 then
     ytest=varargin(1);
     res_pred=regapply(model_reg,xtest,ytest);
   end   
   
   
    
endfunction
    
    
