function[sep,pred,bs,last_argout]=caltestxx(calx,caly,testx,testy,func,cent,varargin)
  

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013




  // this function computes:    sep     standard error of prdiection
  //                            bs      bias
  //                            pred    predicted values
  // for linear and non-linear models
 
  // for linear regressions:   
  //     function[sep,pred,bs,bcoeffs,tscores,ploads,dof]=caltestxx(calx,caly,testx,testy,func,cent,varargin)
  
  // for neural networks:
  //     function[sep,pred,bs,wh,wo]=caltestxx(calx,caly,testx,testy,func,cent,varargin)
 
  //
  // Input:
  // ======
  // calx:        matrix n x p for calibration
  // caly:        vector p X 1 of reference values associated to calx
  // testx:       matrix nv x p for validation
  // testy:       vector nv x 1 of reference values for testx
  // 'func':      function called for each step of the cross validation 
  //              linear methods:         'pls' , 'pcr', 'vodka' ,'ridge' 
  //              non linear methods:     'nnpls'    
  // cent:        if 1, data are centered 
  //              if 0, data are not centered 
  //
  // Output:
  // =======
  // sep:        vector, standard error of prediction 
  // pred:       matrix nv X nf of the predicted values for testx and the nf models
  //
  // Miscellaneous:
  // ==============
  // JC Boulet (INRA) & JM Roger (IRSTEA), France
  // Last update: 2012, March, 30
  // ---------------------------------------------


  // linear vs. non linear models-------------
  if  part(func,[1 2])~='nn' then      
    flag='L';
  else
    flag='NL';
  end
  
  

  // centering option applied-----------------
  [n,p]=size(calx);
  
  xm=mean(calx,1);
  ym=mean(caly,1);
  calx=calx- ones(size(calx,1),1)*xm*cent;
  testx=testx-ones(size(testx,1),1)*xm*cent;
  caly=caly-ones(size(calx,1),1)*ym*cent;



  // model calculation (and specially prediction = pred) -----------------------------
  functionname='cal'+func;
  if flag=='NL'       then            // 2 lines added in v.0.6  
      execstr(msprintf('[pred2,ploads,wh,wo]= %s(calx,caly,testx,varargin(:));',functionname));
      pred=pred2.d;
      //wh.d=wh2;
      //wo.d=wo2.d;  
  elseif flag=='L' then
      execstr(msprintf('[bcoeffs,tscores,ploads,dof]= %s(calx,caly,varargin(:));',functionname)); 
      pred = testx*bcoeffs + ones(size(testx,1),size(bcoeffs,2))*ym*cent; 
  end
 
 
 
  // calculation of sep and bs: -----------------------
  nf = size(pred,2);  

  for j = 1:nf
     press(j) = sum(sum((pred(:,j)-testy).^2));
  end
  
  sep = sqrt(press / size(testy,1));
  bs = mean( pred - testy*ones(1,nf),'r')';
  
  
  // output variables:-------
  
  last_argout=struct();
  
  if flag=='L' then
      last_argout.bcoeffs=bcoeffs;
      last_argout.tscores=tscores;
      last_argout.ploads=ploads;
      last_argout.dof=dof;

  elseif flag=='NL' then
     //pause
      last_argout.ploads=ploads;
      last_argout.wh=wh;
      last_argout.wo=wo;
  end

  
  

endfunction





