function [ypred,ploads,wh1,wo1]=calnnpls (calx,caly,testx,varargin)

    //function ypred=calnnpls (calx,caly,testx,lv,nh)
    
    x=div(calx);
    y=div(caly);
    xtest=div(testx);
    
    if size(x.d,2)>1000 then
        warning('the calibration matrix is too large: the process may take a very long time');
        warning('are you sure to continue?')
       pause
    end
 
    // default values:
    
    if argn(2)>=4 then
      lv=varargin(1);
    else
      lv=10;
    end
    
    if argn(2)>=5 then
      nh=varargin(2);
    else
      nh=5;
    end
    
    
    // PLS
    [b0,xscores,ploads]=calikpls(x.d,y.d,lv);
    s=pinv(x.d'*x.d); 
    
    xtest_scores=xtest.d*s*ploads*inv(ploads'*s*ploads);
    
    // neural network: 
    ypred.d=[];
    ypred.i=[];
    ypred.v=[];
    for i=1:lv;
        [wh,wo]=nns_init(xscores(:,1:i),caly,nh);
        [res_nns]=nns_buildbayes(wh,wo,xscores(:,1:i),caly);
        wh1=res_nns.wh.d;
        wo1=res_nns.wo.d;
        ypred0=nns_simul(wh1,wo1,xtest_scores(:,1:i));
        ypred.d=[ypred.d ypred0.d];
        ypred.v=[ypred.v ypred0.v];
    end
    
    ypred.i=ypred0.i;
    ypred=div(ypred);
    
    
endfunction
