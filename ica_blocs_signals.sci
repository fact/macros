function [res]=ica_blocs_signals(X0, nF, split, varargin);
                                        //Blocks
 

  // INPUT :
  // X = data mlatrix to be analysed
  // nF = maximum number of ICs to be calculated
  // Blocs = number of blocs of samples to be compared
  // Options = methods of creating blocks, plotting results and accelerating calculations

  // Get JADE Option values
  // Options.Method = 'Tall', 'Wide', 'Kernel', 'ComDim', 'Normal'
  // Options.Partitions = number of partition if Options.Method = 'Tall' or 'Wide'

  // OUTPUT :
  // Signal = nF sets of ICA Sources
  // Scores = nF sets of ICA Proportions
  // Sort_Signal_Corr = Correlations between Sources
  // Sort_Score_Corr = Correlations between 'Scores'
  // Criterion = Lowest correlation between blocks of samples 

  // Reference to cite when using the method : 
  // "Two novel methods for the determination of the number of components in
  //   independent components analysis models"
  // Chemometrics and Intelligent Laboratory Systems 112 (2012) 24ñ32
  // D. Jouan-Rimbaud Bouveresse, A. Moya-Gonz·lez, F. Ammari, D.N. Rutledge 
  
  
  // get Scilab version 
  sci_version=part(getversion(),[8,9,10]);  // a string
  sci_version=strtod(sci_version);          // a number 
  if sci_version < 6 then
      accolade1='(';
      accolade2=')';
  else 
      accolade1='{';
      accolade2='}';
  end
  
  // gestion de ibbplot: 
    // 0: none
    // 1: lowest correlations
    // 2: all 
  if argn(2)<=3 then
      Plotting=0;
  elseif argn(2)==4 then 
      options=varargin(1);
      if ~isfield(options,'ibbplot') then
        Plotting=0
      else
        Plotting=options.ibbplot;
      end
  end   

  // Div format (JCB)
  X1=div(X0);
  X=X1.d;
  
  [nR,nC]=size(X);
  
  // option 'Blockmethod' 
    // 1: 'Venetian blind'
    // 2: 'Successive blocks' = 'Jack knife'
    // 3: 'Random'
    // vecteur conjonctif:    'Groups'   
    
  if type(split)==10  then  // if split is a string

        if part(split,1:3)=="jck"  then  // jackniffe
            Blocs = strtod(part(split,4:length(split)));
            Blockmethod=2;
            
        elseif part(split,1:3)=="vnb"  then  // venitian blind
            Blocs = strtod(part(split,4:length(split)));
            Blockmethod=1;
                
        else
                error('Incorrect split argument') 
       
        end;

    else // if split is not a string

        if prod(size(split))==1  then  // simple random subsets 
            Blocs=split;
            Blockmethod=3;
        elseif prod(size(split))==nR then  // vecteur conjonctif 
            Blocs=max(split);
            Blockmethod=4;
            
        else
            error('Incorrect split argument') 
        end;

    end

  // Do M-times ICA on data

  BlockSize=floor(nR/Blocs);
  if nF>BlockSize then 
    nF=BlockSize;
  end
  

  AllSamples=[1:nR]';

  // Initiale 'rand' & create a random vector
  Sample_nums(:,1)=rand(nR,1);

  // Create a vector of numbers for blocks
  temp=[];
  for m=1:Blocs
    temp=[temp,m*ones(1,round(nR/Blocs))];   // JCB 6avril 19 remplace int8 par round
  end;
  // ajustement de la dimension              // JCB 6avril19
  if size(temp,2)>nR then 
      temp=temp(1:nR);
  elseif size(temp,2)<nR then 
      diff_temp=temp($)*ones(1,nR-size(temp,2));
      temp=[temp diff_temp];
  end
  

  // Verify it's the right length           // JCB 6avril19
  //Lentemp=length(temp);
  //Lensample=nR;
  //if Lentemp>nR then
  //   Sample_nums(:,2)=temp(1:nR)';
  //else
  //  Sample_nums(:,2)=[temp,m*ones(1,Lensample-Lentemp)]';
  //end;
  Sample_nums(:,2)=temp';


  // Randomize block numbers based on 'rand'
  //Sample_nums=gsort(Sample_nums,'lr','i');   // remplace sortrows  // JCB 6avril19
  [nul,index]=gsort(Sample_nums(:,1),'g','i');
  Sample_nums=Sample_nums(index,:);

  // création de "cell arrays" pour Scilab
  Signal=cell(Blocs,nF);
  Scores=cell(Blocs,nF);

  for m=1:Blocs

    select Blockmethod
        case 1                        //'vnb'
            samples=[m:Blocs:nR];

        case 2                        //'jck'
            samples=[(m-1)*BlockSize+1:min(nR,m*BlockSize)];

        case 3                        // random
            samples=AllSamples(Sample_nums(:,2)==m);

        case 4                        // vect. conjonctif
            samples=AllSamples(split==m);   
    end
          

    for Fac=1:nF
        if  argn(2)==4 then
            [Signal0,Scores0] = icascores(X1(samples,:),Fac, varargin(:));
            [Signal0,Scores0] = icasigns(Signal0,Scores0, varargin(:));
        elseif argn(2)==3 then 
            [Signal0,Scores0] = icascores(X1(samples,:),Fac);
            [Signal0,Scores0] = icasigns(Signal0,Scores0);
        end

        execstr(msprintf('Signal%s m,Fac%s.entries=Signal0;',accolade1,accolade2))
        execstr(msprintf('Scores%s m,Fac %s.entries=Scores0;',accolade1,accolade2))
    end;
  end;

  // Sort ICs based on successive correlations
  [Signal,Scores] =  ica_sort_ics(Signal,Scores);  // Signal et Scores sont des cell
  
  // Predict scores for all samples
  for m=1:Blocs
    for Fac=1:nF
        execstr(msprintf('signal_m_Fac=Signal%s m,Fac %s.entries;',accolade1,accolade2))
        all_scores.d=X1.d*signal_m_Fac.d*inv(signal_m_Fac.d'*signal_m_Fac.d);
        all_scores.i=X1.i;
        all_scores.v='ic'+string([1:1:Fac]');
        all_scores=div(all_scores);
        All_Scores(m,Fac).entries=[];
        All_Scores(m,Fac).entries=all_scores;
    end;
  end;

  // Get all Scores and signals (JCB)
  for Fac=1:nF
     execstr(msprintf('All_Scores_Cube%s 1,Fac %s.entries=[];',accolade1,accolade2))
     execstr(msprintf('All_Scores_Cube%s 1,Fac %s.entries=All_Scores(1,Fac).entries;',accolade1,accolade2)) 
     execstr(msprintf('All_Signals_Cube%s 1,Fac %s.entries=[];',accolade1,accolade2))       // indispensable d'initialiser la structure div
     execstr(msprintf('All_Signals_Cube%s 1,Fac %s.entries=Signal%s 1,Fac %s.entries;',accolade1,accolade2,accolade1,accolade2))
    for m=2:Blocs
         execstr(msprintf('All_Scores_Cube%s 1,Fac %s.entries=[All_Scores_Cube%s 1,Fac %s.entries, All_Scores(m,Fac).entries];',accolade1,accolade2,accolade1,accolade2))
         execstr(msprintf('s_m_fac=Signal%s m,Fac %s.entries;',accolade1,accolade2))
         execstr(msprintf('All_Signals_Cube%s 1,Fac %s.entries=[All_Signals_Cube%s 1,Fac %s.entries, s_m_fac];',accolade1,accolade2,accolade1,accolade2))
    end;
  end;


  // Keep all correlations between block scores
  for Fac=1:nF
    execstr(msprintf('correl_all_sc_cube=corrmat(All_Scores_Cube%s 1,Fac %s.entries,All_Scores_Cube%s 1,Fac %s.entries);',accolade1,accolade2,accolade1,accolade2))
    execstr(msprintf('Score_Corr%s 1,Fac %s.entries=abs(correl_all_sc_cube.d);',accolade1,accolade2)) 
    execstr(msprintf('temp=Score_Corr%s 1,Fac %s.entries(:);',accolade1,accolade2))                      //met sur 1 seule colonne
    temp=gsort(temp,'r','d');                               // trie chaque colonne par ordre décroissant
    execstr(msprintf('Sort_Score_Corr%s 1,Fac %s.entries=temp;',accolade1,accolade2))
  end;

  // Calculate divisor
  Divisor=(Blocs*Blocs-Blocs)/2;
  

  // Keep all correlations between block Scores and Signals
  for Fac=1:nF
    execstr(msprintf('correl_allsignalcube=corrmat(All_Signals_Cube%s 1,Fac %s.entries, All_Signals_Cube%s 1,Fac %s.entries);',accolade1,accolade2,accolade1,accolade2))
    Signal_Corr(1,Fac).entries=abs(correl_allsignalcube.d);
    // 'corrcoef' in main Matlab Toolbox
    
    temp=Signal_Corr(1,Fac).entries(:);         // (:) met dans la même colonne
    temp=gsort(temp,'r','d');   // trie chaque colonne ('r') par ordre décroissant ('d')
    Sort_Signal_Corr(1,Fac).entries=temp //(1:2:$);  // enlève 1 valeur/2 (doubles)
  end;

  // Signals
  if Plotting>=1 then

    figure;
    h1=gcf();
    h1.background=-2;
    h1.children.x_label.text='maximum possible number of significant ICs';
    h1.children.y_label.text='|R|';
    title(msprintf('ICA-by-bloc correlations signals, significant data'));     
 
    color_code=['b';'k';'r']
    
    for Fac=1:nF
        Longueur=size(Sort_Signal_Corr(1,Fac).entries(Blocs*Fac+1:Blocs*Fac*Blocs,:),1);
        Longueur=Longueur/2;
        
        color_index=1+modulo(Fac-1,3);

        execstr(msprintf('plot([1:1:Longueur]''/Divisor, Sort_Signal_Corr(1,Fac).entries(Blocs*Fac+1:2:Blocs*Fac*Blocs,:),''%s'');', color_code(color_index)));  

    end;

  end


  for Fac=1:nF
        Longueur=Blocs^2 * Fac - Blocs*Fac;  
        Longueur=Longueur/2;

        //     Significant
        Criterion(Fac,1)=Longueur;
        Criterion(Fac,2)=double(Sort_Signal_Corr(1,Fac).entries(Blocs^2*Fac,:)); 
                                                        
  end;

  if Plotting==2 then
    figure;
    h2=gcf();
    h2.background=-2;
    h2.children.x_label.text='maximum possible number of significant ICs';
    h2.children.y_label.text='|R|';
    title(msprintf('ICA-by-bloc correlations signals, all data'));    
    
    for Fac=1:nF
        LongueurAll=size(Sort_Signal_Corr(1,Fac).entries(Blocs*Fac+1:$,:),1);
        LongueurAll=LongueurAll/2;
        labels_all=[1:1:LongueurAll]/(Divisor);

        Longueur=size(Sort_Signal_Corr(1,Fac).entries(Blocs*Fac+1:Blocs*Fac*Blocs,:),1);
        Longueur=Longueur/2;
        labels_signif=[1:1:Longueur]/(Divisor);

        //     All
        plot(labels_all', Sort_Signal_Corr(1,Fac).entries(Blocs*Fac+1:2:$,:),'b');
       
        //     Significant
        plot(labels_signif', Sort_Signal_Corr(1,Fac).entries(Blocs*Fac+1:2:Blocs*Fac*Blocs,:),'r')

    end;

  end

  // Lowest Correlations
  if Plotting>0 then 
    figure;
    h2=gcf();
    h2.background=-2;
    plot(Criterion(:,2), '-o'); //, axis tight;
    xlabel('ICs');
    ylabel('|R|');
    title('Lowest correlations')
  end

  res.signal=Signal;
  res.scores=Scores;
  res.signal_corr=Sort_Signal_Corr;
  res.scores_corr=Sort_Score_Corr;
  res.rmin.d=Criterion;
  res.rmin.i='ic'+string([1:1:nF]');
  res.rmin.v=['length';'lowest correlations'];


endfunction

