function [res_classpls2da] = classplsfda(x,y,lv)
    
    // distributed under the CeCILL-C license
    // copyright INRA-IRSTEA 2013    
    
    // x : une matrice 
    // y : une matrice disjonctive
    
    // lv: un vecteur de longueur 2  lv=[lv1;lv2]
    // lv1 = nbre de dimensions de la PLS2
    // lv2 = nbre de dimensions de la FDA
    
    lv1=lv(1);
    lv2=lv(2);
    
    // Initialisation:
    [n,q]=size(x);   

    y=conj2dis(y);                 
    ngroups=size(y,2);
    
    // vérification du nbre de groupes et de la dimension de la FDA
    if ngroups <= lv2 then
        lv2=ngroups-1;
    end
    
    [b1,t1,p1,dof1,r1]=calsimpls(x,y,lv1);   // vu sur un exemple: p1=r1
                                            //  t1= x*r1
                                            // les t1 sont orthogonaux , pas les p1 ni les r1 (mais les r1 sont normés)
    res_fda=classfda(t1,y,lv2);

    t2=res_fda.scores;                      // les t2 sont orthogonaux 
    r2=res_fda.rloadings;                   // t2=t1*r2 
                                            // donc: t2= x*r1*r2 
    // Sorties:
    res_classpls2da.scores=t2;
    res_classpls2da.rloadings=r1*r2;

endfunction
