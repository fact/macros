function h=scmap(x,col1,col2,startpos,endpos,varargin)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013 
 
  if argn(2)==5 then
      colored_map_options(x,col1,col2,startpos,endpos,'s')
  elseif argn(2)==6 then
      colored_map_options(x,col1,col2,startpos,endpos,'s',varargin(1),'')
  elseif argn(2)==7 then
      colored_map_options(x,col1,col2,startpos,endpos,'s',varargin(1), varargin(2)) 
  end
  
  h=gcf()

endfunction


