function c=%div_m_div(a,b)
    
       // multiplication de structures Div

       c.d=a.d*b.d;
       c.i=a.i;
       c.v=b.v;
       
       c=div(c);
       
       c.infos=[a.infos(1);b.infos(2)];
        

endfunction
            

