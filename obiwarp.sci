function x2_new_rt=obiwarp(x1_mast,x2_slav,score,local,nostdnrm,factor,gap,init_penalty,response)
    
    // fonction obiwarp 
    // cette fonction ajuste les temps de rétention (RT) obtenus sur un jeu esclave 
    // par rapport à ceux qui ont été obtenus sur un jeu maitre 
    // x2_new_rt est destiné à remplacer x2_slave.i 
    
    
    // arguments d'entrée:
    // x1_master: un div 
    // x2_slave: un autre div ; x1.d et x2.d ont même nombre de colonnes 
    
    // arguments de la fonction cpp fixés a priori:
    // format:    'lmata' (fixe)
    // outfile:   'obi_tempfile'(fixe)
    // images:    non utilisé 
    // timefile:  non utilisé 
    
    // arguments variables:
    // score:       score function: (*)cor (Pearson's R), cov (covariance),
    //                  prd (product), euc (Euclidean distance) 
    // local:       local rather than (*)global alignment
    // nostdnrm :   no standard normal (By default the score matrix values are
    //               mean centered and divided by the standard deviation
    //               [unless all values are equal])
    // [factor_diag,fact_gap]:   local weighting applied to diagonal/gap moves in alignment.
    //               These are given in a comma separated string
    //               (*)Default: (diagonal,gap) = '2,1'
    //               '2,1' gives an unbiased alignment.
    // [gap_init,gap_extend]     gap penalty given in comma separated string: initiate,extend
    //               (*)Defaults: (gap_init,gap_extend) [by score type]:
    //                   'cor' = '0.3,2.4'
    //                   'cov' = '0,11.7'
    //                   'prd' = '0,7.8'
    //                   'euc' = '0.9,1.8'
    // init_penalty:  Penalty for initiating alignment (for local alignment only)
    //               (*)Default: 0
    // response:        Responsiveness of warping.  0 will give a linear warp based
    //                  on start and end points.  100 will use all bijective anchors
    
    // arguments de sortie: 
    // x2_new_rt: les temps de rétention corrigés 
    
    // vérification du format div
    x1_master=div(x1_mast);
    x2_slave=div(x2_slav);
    
    // conversion div -> lmata
    div2lmata(x1_master,'x1tempfile');
    div2lmata(x2_slave,'x2tempfile');
    
    // 7 arguments par défaut 
    if argn(2)<9 then
        response=[];
    end
    if argn(2)<8 then
        init_penalty=[];
    end
    if argn(2)<7 then
        gap=[];
    end
    if argn(2)<6 then
        factor=[];
    end
    if argn(2)<5 then
        nostdnrm=[];
    end
    if argn(2)<4 then
        local=[];
    end
    if argn(2)<3 then
        score=[];
    end
    
    images=[];
    timefile=[];
    
    // exécution de la fonction obiwarp_lmata
    obiwarp_lmata('x1tempfile','x2tempfile','lmata','obitempfile',images,timefile,score,local,nostdnrm,factor,gap,init_penalty,response);
    
   // pause
    
    // récupération des RT:
    file_id=mopen('obitempfile');
    line1=mgetl(file_id,1);          // lit la ligne 1 (ne sert pas)
    line2=mgetl(file_id,1);          // lit la ligne 2
    mclose(file_id);
    line2=stripblanks(line2,%f,1);
    line2=strsplit(line2,' ');
    
    x2_new_rt=strtod(line2);
    
    // nettoyage des fichiers 
    deletefile("x1tempfile")     
    deletefile("x2tempfile") 
    deletefile("obitempfile") 
    
endfunction
