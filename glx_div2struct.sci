function  xoutput=glx_div2struct(xinput)

   // un Div est transformé en structure
  
   if  typeof(xinput)=='div' then
      xoutput.typeof='div';
      xoutput.d=xinput.d;
      xoutput.i=justify(xinput.i,'l');
      xoutput.v=justify(xinput.v,'l');
      xoutput.infos=justify(xinput.infos,'l');

   else   
      error('a Div expected');
      
   end 
      
   
endfunction
