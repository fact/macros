function [classif,conf,nn,scale] = classtestknnda(calx,caly,testx,testy,nn,scal)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
  //initialisation
  if isconj(testy)=='T' then 
      testy=conj2dis(testy);  
  end

  if ~isdef('scal') then
      scal=[];
  end

  // calcul du modèle 
  classif=classknnda(calx,caly,testx,nn,scal);

  // matrice de confusion
  conf = classif'*testy;
  
  scale=scal;
 
 
endfunction
