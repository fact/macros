function [res]=snk(x0,groups)
  
  // ANOVAS à 1 facteur, pour chacune des q variables
  x1=div(x0);

  // suppression des variables de variance nulle:
  variance_x1=stdev(x1.d,'r');
  q1=size(x1.d,2);
  var_numbers=[1:1:q1]';
  var_to_del=var_numbers(variance_x1<100* %eps);
  //pause
  x=cdel(x1,var_to_del);
  
  [n,q]=size(x.d);

  nbr_removed=string(q1-q);  
  if nbr_removed~='0' then
    warning(msprintf('warning: %s variable(s) removed due to null variances',nbr_removed)); 
  end

  // taille max des noms de variables 
  lenghtmax=max(length(x.d));
  length_sel=min(lenghtmax,13);

  // initialisation des groupes
  [groups2,name_groups2,size_groups2]=str2conj(groups)


  groups_list=groupcreate(x,groups2);
  
  [ngroups]=size(groups_list);

  sizegroups=round(n/ngroups);
  
  [xcentered,xmean]=centering(x.d);             // moyenne générale
  
  // calcul des SCE:
  SCET=diag(xcentered.d'*xcentered.d);
  [n_scet,p_scet]=size(SCET);
  SCEA=zeros(n_scet,p_scet);
  SCER=zeros(n_scet,p_scet);

  xgroupcentered=[];
  xmeangroup_tous=[];
  nobs=[];
  
  for i=1:ngroups;   
      [xgroupcentered2,xmeangroup]=centering(groups_list(i));
      if xmeangroup_tous ==[] then
          xmeangroup_tous=xmeangroup.d';
      else
        xmeangroup_tous=[xmeangroup_tous;xmeangroup.d'];
      end
      if xgroupcentered==[] then
          xgroupcentered=xgroupcentered2.d;
      else 
          xgroupcentered=[xgroupcentered;xgroupcentered2.d];
      end
      nobs=[nobs;size(xgroupcentered2.d,1)];
      SCEA= SCEA+ size(xgroupcentered2.d,1)*(xmeangroup.d-xmean.d).*(xmeangroup.d-xmean.d);
  end

  //SCEA=SCEA;
  SCER=diag(xgroupcentered'*xgroupcentered);
 
  ddl_a=(ngroups-1)*ones(q,1);
  ddl_r=(n-ngroups)*ones(q,1);

  // calcul de fischer_vect
  CMA=SCEA/(ngroups-1);
  CMR=SCER/(n-ngroups);
  
  // cas de variances résiduelles intra-groupe nulles 
  n=max(size(CMR));            // pour éviter de diviser par 0; 30 juin 16
  fischer_vect=zeros(n,1);
  for i=1:n;
      if CMR(i)~=0 then 
          fischer_vect(i)=CMA(i)/CMR(i);
      else
          fischer_vect(i)=1000;    // valeur arbitraire, très grande => infini 
      end
  end 


  [prsupf,qrsupf]=cdff('PQ',fischer_vect,ddl_a,ddl_r);
  

  
  for i=1:size(qrsupf,1);     
      // la troncature ne va pas pour les nombres exprimés en puissance de 10 comme: 2.16567E-12
      if qrsupf(i)<0.001  then qrsupf(i)=0;
      end
  end
  
  
  // impression des résultats
  blanc0=[];  
  for i=1:q+1;
      blanc0=[blanc0;'  '];
  end

  col0=['Variable';part(string(x.v),1:lenghtmax)];
  col0=justify(col0,'l'); 
  col1=['SCEA';part(string(SCEA),1:5)];
  col1=justify(col1,'l');
  col2=['SCER';part(string(SCER),1:5)];
  col2=justify(col2,'l');

  titre3=msprintf('F(%s,%s)',string(ddl_a(1)),string(ddl_r(1)));
  col3=[titre3;part(string(fischer_vect),1:4)];
  col3=justify(col3,'l');
  col4=['Pr>F';part(string(qrsupf),1:5)];
  col4=justify(col4,'l');
  
  res_anova=col0+blanc0+col1+blanc0+col2 + blanc0 + col3 + blanc0 + col4;
  titre0=msprintf('ANOVA 1 factor, %s classes of %s repetitions on average ',string(ngroups),string(sizegroups));
  res_anova=[titre0; res_anova];
  res_anova=justify(res_anova,'l');

 
  // Reconstruction des F-values à partir de fischer-vect et var_to_del
  // pour respecter le nombre et l'ordre des variables de départ
  if var_to_del ==[] then
     f_values=fischer_vect;
  else
      index=0;
      for i=1:q1;
          if find(var_to_del==i)~=[] then
              f_values(i)=0  // valeur nulle car pas de différence 
              index=index+1;
          else
              f_values(i)=fischer_vect(i-index);  
          end
      end
  end
   
  fvalues.d=f_values;
  fvalues.i=x1.v;
  fvalues.v=titre3;
  fvalues=div(fvalues);
 
 
  //-----------------------------------------------------
  

 

  
  
  // SNK - Student-Newman-Keuls -------------------------

  res_snk=list();

  for i=1:q;   
  
    xi=x.d(:,i);
    sceri=SCER(i);
    
    // calcul du vecteur des ppas
    ppas=zeros((ngroups-1),1);
    for j=1:ngroups-1;
      q_ppas=cdfsnk(ngroups-j+1,(sizegroups-1)*ngroups); // valeur extraite de la table
      ppas(j)=q_ppas *sqrt(sceri/(sizegroups*((sizegroups-1)*ngroups-1))); 
    end
    
  
    
    // classement des moyennes 
    xmeangroup_q=xmeangroup_tous(:,i);
    [xmeangroup_q_tri,ordre_tri]=gsort(xmeangroup_q,'r','d'); 
        // 'r'=tri des lignes; 'd'=ordre=direct  (+grand -> +petit) 
    

    nobs2=nobs(ordre_tri);
       
    codegroup_i=['classes';name_groups2(ordre_tri)];   
    codegroup_i=justify(codegroup_i,'l');
      
    blanc=[];                                                
    xxs1=[];
    xxs2=[]
    for j=1:ngroups;
      if blanc ==[] then
          blanc=' ';
      else
          blanc=[blanc;' '];
      end
      if xxs1==[] then
          xxs1='X';
      else
          xxs1=[xxs1;'X'];
      end
      if xxs2==[] then
          xxs2='ABCDEFGHIJKLMNOP';
      else
          xxs2=[xxs2;'ABCDEFGHIJKLMNOP'];
      end
    end


    // graphique des moyennes classées 
    col_nobs= justify(['nobs  ';string(nobs2)],'l');    //9juil14
    nbre_chiffres=max(length(string(xmeangroup_q_tri)));
    graphic=codegroup_i + [' ';blanc] + col_nobs + [' ';blanc]+['mean  ';part(string(xmeangroup_q_tri),1:nbre_chiffres)] + [' ';blanc] + [' ';blanc];
    clear code2

    
    for j=1:ngroups-1;   // j=nombre de déplacements de la fenetre (entre 1 et ngroups-1) 
        nbre_moyennes=ngroups-j+1;
        for k=1:j        // k=position du haut de la fenetre 
          fenetre=xmeangroup_q_tri(k:k+nbre_moyennes-1);
          diff_max_min=max(fenetre)-min(fenetre);
          if diff_max_min<ppas(j) then 
              code2_k=[k k+nbre_moyennes-1];
              if ~isdef('code2') then
                  code2=code2_k;
              else
                  code2=[code2;code2_k];
              end   
          end
        end
    end  
  
    if ~isdef('code2') then
        code2=[1:1:ngroups]'*[1 1];
    else
        code2=[code2;[1:1:ngroups]'*[1 1]];
    end

    // 2° tri de code2 (pour enlever les regroupements convergents)
    scd2=size(code2,1);
    code3=ones(scd2,1);
    clear code4;
    flag=0;
    if (code2(1,1)==1 & code2(1,2)==ngroups) then
        clear code2
        code2(1,1)=1;
        code2(1,2)=ngroups;
        code4=1;
        flag=1;       // pour tout arreter (et gagner du temps) si pas de différences 
    elseif SCER(i)==0 then    // on suppose que toutes les autres moyennes sont à 0 
        code2=[1 1;2 ngroups];
        code4=[1;2];
    else
      for j=1:scd2                      // calcul de code3 
        for k=1:scd2;        
          if flag ==0 & j~=k & code2(j,1)>=code2(k,1) & code2(j,2)<=code2(k,2) & code2(j,1)~=0 then
            code3(j)=0; 
          end
        end
      end  
      for k=1:scd2;                     // calcul de code4
        if flag ==0 & code3(k)~=0 then
          if ~isdef('code4') then
              code4=k;
          else
              code4=[code4;k];
          end
        end
      end 
    end
    code5=code2(code4,:)                // calcul de code5
    code5=gsort(code5,'r','i');

    // note: code5 contient les codes des lettres A,B,C,,,
    // chaque ligne de code5 correspond à une lettre: ex: la 2° ligne est la lettre B 
    // code5 a 2 colonnes: la première colonne donne le début de la lettre, la deuxième la fin
    // ex: code5=[1 1;2 4] donnera  
    // A
    //   B
    //   B
    //   B
    // code5=[1 1; 2 2; 3 4] donnera
    // A
    //   B
    //     C
    //     C


    // visualisation graphique des regroupements de moyennes
    // graphic_code = lettres A, B, C... qui regroupent les moyennes
    clear graphic_code; // 27juillet16 
      
    for j=1:size(code5,1);
      if     ngroups >16  then   xxs=xxs1;
      elseif ngroups <=16 then   xxs=part(xxs2,j:j);
      end 
      length_code2_j=code5(j,2)-code5(j,1)+1;
      if code5(j,1)~=0 & code5(j,2)~=0 then
        if code5(j,1)==1 & code5(j,2)~=ngroups  then 
          graphcode=[xxs(1:code5(j,2)); blanc(code5(j,2)+1:ngroups)];
        elseif code5(j,1)~=1 & code5(j,2)==ngroups then 
          graphcode=[blanc(1:code5(j,1)-1);xxs(code5(j,1):ngroups)];
        elseif code5(j,1)==1 & code5(j,2)==ngroups then 
          graphcode= xxs(1:ngroups); 
        else   
          graphcode=[blanc(1:code5(j,1)-1); xxs(code5(j,1):code5(j,2));blanc(code5(j,2)+1:ngroups)];  
        end
      end 
      if ~isdef('graphic_code') then             // 27 juillet 16 car [] + x = [] quel que soit x
          graphic_code= blanc + graphcode;
      else   
          graphic_code= graphic_code + blanc + graphcode ;   
      end
    end

    sortie_i= graphic + [justify(x.v(i),'l'); graphic_code];

    res_snk(i)=['STUDENT_NEWMAN_KEULS 5%:' ;sortie_i];
  
  end

  res.anova=res_anova;
  res.snk=res_snk;
  res.fvalues=[];
  res.fvalues=fvalues;



endfunction

