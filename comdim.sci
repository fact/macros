function[res]=comdim(col,ndim,threshold)

  //comdim			- Finding common dimensions in multitable data (saisir format)
  // function[res]=comdim(collection,(ndim),(threshold))
  //
  //Input arguments:
  //---------------
  //collection:vector of saisir files (the numbers "nrow" of rows in each table must be equal) .
  //ndim:number of common dimensions
  //threshold (optional): if the "difference of fit"<threshold then break the
  //iterative loop
  //
  //Output arguments:
  //-----------------
  //res with fields:
  //Q : observations scores (nrow x ndim)
  //explained : 1 x ndim, percentage explanation given by each dimension
  //saliences : weight of the original tables in each
  // dimensions (ntable x ndim).
  //
  //Method published by  E.M. Qannari, I. Wakeling, P. Courcoux and H. J. H. MacFie
  //in Food quality and Preference 11 (2000) 151-154
  //
  //typical example (suppose 3 SAISIR matrices
  //"spectra1","spectra2","spectra3")
  //collection(1)=spectra1; collection(2)=spectra2; collection(3)=spectra3
  //myresult=comdim(collection);
  //map(myresult.Q,1,2);//// looking at the compromise scores
  //figure;
  //map(myresult.saliences,1,2);//// looking at the weights

 // get Scilab version 
  sci_version=part(getversion(),[8,9,10]);  // a string
  sci_version=strtod(sci_version);          // a number 
  if sci_version < 6 then
      accolade1='(';
      accolade2=')';
  else 
      accolade1='{';
      accolade2='}';
  end

  ntable=size(col); 
  for i=1:ntable;
      xs=div(col(i));
      col(i)=xs;
  end

  [nargout,nargin] = argn(0) // added by matlab2sci
  s=cell();
  
  if(nargin<3) then threshold=1E-10;  end;
  if(nargin<2) then ndim=ntable;      end;

  for i=1:ntable;
    execstr(msprintf('s%s i %s.entries=col(i).d;',accolade1,accolade2))
  end

  [Q.d, saliences.d,explained.d]=comdim_x(s,threshold,ndim);
  
  for i=1:ndim
    chaine=('D' + string(i) + '        ');
    bid(i)=part(chaine,(1:6));
  end

  for i=1:ntable
    chaine1=('t' + string(i) + '        ');
    bid1(i)=part(chaine1,(1:6));
  end
  
  Q.v=bid;
  Q.i=col(1).i;
  saliences.v=Q.v;// dimension
  saliences.i=bid1;// table
  explained.i=Q.v;
  explained.v='// explained';
  
  //Div
  Q=div(Q);
  saliences=div(saliences);
  explained=div(explained);
  
  res.scores=[];
  res.scores=Q; 
  res.weights=[];
  res.weights=saliences; 
  res.scores_tables=[];
  res.scores_tables=explained;
  
endfunction



// subfunction: comdim_x  ------------------------------------------------------------

function[Q, saliences, explained]=comdim_x(table,threshold,ndim)

  //comdim_x				- Finding common dimensions in multitable data
  // No direct use. Normally called with function "comdim"
  // function[Q, saliences, explained]=comdim_x(col,threshold,ndim)
  // Finding common dimensions in multitable according to method 'level3'
  // proposed by E.M. Qannari, I. Wakeling, P. Courcoux and H. J. H. MacFie
  // in Food quality and Preference 11 (2000) 151-154
  // table is an array of matrices with the same number of row
  // threshold (optional): if the difference of fit<threshold then break the iterative loop
  // ndim : number of common dimensions
  // default: threshold=1E-10; ndim=number of tables
  // returns Q: nrow x ndim the observations loadings
  //table=cell();

  [nargout,nargin] = argn(0) // added by matlab2sci
  average=cell();
  if(nargin<2) then 
      threshold=1E-10; 
  end;
  ntable=size(table,1);
  if(nargin<3) then 
      ndim=ntable; 
  end;
  execstr(msprintf('nrow=size(table%s 1 %s.entries,1);',accolade1,accolade2))
  W=zeros(ntable+1,nrow,nrow);// average assoc. matrix in last element
  prescale=zeros(ntable);
  LAMBDA=zeros(ntable,ndim);
  Q=zeros(nrow,ndim);
  // centring and rescaling the tables ***************************
  for(i=1:ntable);
    //disp([i ntable]);
    execstr(msprintf('X=table%s i%s.entries;',accolade1,accolade2))
    execstr(msprintf('average%s i %s.entries=mean(X,1);',accolade1,accolade2))
    execstr(msprintf('table%s i %s.entries =X-ones(nrow,1)*average%s i %s.entries;',accolade1,accolade2,accolade1,accolade2))
    execstr(msprintf('aux=table%s i %s.entries;',accolade1,accolade2))
    xnorm=sqrt(sum(aux.*aux));
    initial_norm(i)= xnorm;
    execstr(msprintf('table%s i %s.entries=table%s i %s.entries/xnorm;',accolade1,accolade2,accolade1,accolade2))
    execstr(msprintf('temp_table=table%s i %s.entries;',accolade1,accolade2));
    W(i,:,:)=temp_table*temp_table';
    W(ntable+1,:,:)=W(ntable+1,:,:)+W(i,:,:);
  end;
  // end centring and rescaling
  // main loop
  unexplained=ntable; // Warning!: true only because the tables were set to norm=1

  for dim=1:ndim
    //disp(dim);
    previousfit=10000;
    //step a
    lambda=ones(ntable,1);
    //step b
    deltafit=1000000;
    while(deltafit>threshold)
      W(ntable+1,:,:)=zeros(1,nrow,nrow);
      bid=0;
      for ta=1:ntable
        W(ntable+1,:,:)=W(ntable+1,:,:)+lambda(ta)*W(ta,:,:);
      end
      [UW, SW, VW]=svd(matrix(W(ntable+1,:,:),nrow,nrow));
      q=UW(:,1);
      fit=0;
      for ta=1:ntable
      // estimating residuals
      lambda(ta)=q'*matrix(W(ta,:,:),nrow,nrow)*q;
      aux=matrix(W(ta,:,:),nrow,nrow)-lambda(ta)*q*q';
      fit=fit+sum(aux.*aux);
      end
      deltafit=previousfit-fit;
      previousfit=fit;
    end //deltafit>threshold
    //xdisp('fit =', fit);
    explained(dim)=(unexplained-fit)/ntable*100;
    unexplained=fit;
    LAMBDA(:,dim)=lambda;
    Q(:,dim)=q;
    // updating residuals
    aux=eye(nrow,nrow)-q*q';

    for ta=1:ntable
      execstr(msprintf('table%s ta %s.entries=aux*table%s ta %s.entries;',accolade1,accolade2,accolade1,accolade2))
      execstr(msprintf('temp_file_1=table%s ta %s.entries;',accolade1,accolade2))
      //execstr(msprintf('temp_file_2=table%s ta %s.entries*table%s ta %s.entries;',accolade1,accolade2,accolade1,accolade2))
      W(ta,:,:)=temp_file_1*temp_file_1';
    end
  end
  aux1=0;
  aux2=0;
  saliences=LAMBDA;

endfunction







