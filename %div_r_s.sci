function c=%div_r_s(a,b)

       // division par un scalaire
       
       c.d=a.d/b;
       c.i=a.i;
       c.v=a.v;
       
       c=div(c);
       
       c.infos=a.infos;

endfunction
            

