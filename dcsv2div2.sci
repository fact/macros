function [data] = dcsv2div2(filename)                                    
  
 // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
  // reads a .csv file and converts it to .div
  // by default: separator = ';' decimal = ',' 
  // the first line and the first column of the .csv file must be labels                                                
  // the cell in position (1,1) is omitted 
  // updated: 2015, december, 18 by JMR
  
  data=dcsv2div(filename,',','.');

endfunction                                                                              
                                        

