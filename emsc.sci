function [saisir] = emsc(saisir0,reference,polynom_dim,plage)
 
  //msc        - Multiplicative scatter correction on spectra
  // function [X1] = msc(X,(reference))
  //
  //Input arguments:
  //================
  //X:SAISIR matrix of data (n x p)
  //reference (optional): SAISIR vector of a reference spectrum (default:
  //average of X)
  //
  //Output:
  //======
  //X1: data corrected by the Multiplicative scatter correction method.
  //
  // fonction ecrite par D.Bertrand
  // modifiée par JC Boulet le 19fev18
  

  saisir1=div(saisir0);
  [n,p]=size(saisir1.d);  

  if argn(2)==1 | (argn(2)>1 & reference=='m') then
     model0=mean(saisir1.d,'r');
  else
     reference=div(reference);
     model0=reference.d'; 
  end;   
  
  if argn(2)<3 then 
      polynom_dim=2;
  end
  
  if argn(2)<4 then
      plage=[1 p];
  end
  
  [nmodel,pmodel]=size(model0);
  
  if nmodel==1 then
      model0=model0';
  end
  
  // verification des dimensions
  [nmodel,pmodel]=size(model0);

  
  if nmodel ~= p then 
     error('the raw spectra and the reference spectrum should have the same number of variables' )
  end
  
  
  // initialisation
  saisir.d=zeros(n,p);
  
  // correction du polynome 
  saisir1=detrending(saisir1,polynom_dim);
  model1=detrending(model0',polynom_dim);
  
  model=model1.d';
   
  // construction de la MSC:
  y=[ones(p,1) model];

  for i=1:n;
     bcoeffs=calmlr(y(plage(1):plage(2),:),saisir1.d(i,plage(1):plage(2))');
     saisir.d(i,:)=(saisir1.d(i,:)-bcoeffs(1)*ones(1,p))/bcoeffs(2);
  end   

  saisir.v=saisir1.v;
  saisir.i=saisir1.i;
  
  saisir=div(saisir);

  endfunction


