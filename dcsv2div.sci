function [data] = dcsv2div(filename,separator,decimal)                                    
  
 // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
  // reads a .csv file and converts it to .div
  // by default: separator = ';' decimal = ',' 
  // the first line and the first column of the .csv file must be labels                                                
  // the cell in position (1,1) is omited 
  // updated: 2015, december, 18 by JMR
  
  if argn(2)<3 then
      decimal=',';
  end
  
  if argn(2)<2 then
      separator=';';
  end
   
  m=csvRead(filename,separator,decimal,'string');
  m=strtod(strsubst(m,'D','E'));  // notation scientifique 

  data=div(m);

endfunction                                                                              
                                        

