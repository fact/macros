function xout=normalize(xin)
    
    // cette fonction divise chaque vecteur-ligne (spectre) par sa norme 
    xin2=div(xin);
    
    sce=diag(xin2.d*xin2.d');
    q=size(xin2.d,2);
    norms=sqrt(sce/q);
    
    xout=xin2;
    xout.d=xout.d ./(norms*ones(1,q));
    
    
endfunction
