function [x,flt_out]=simufilters( sig00,bandpass,width );



  // gestion des données en entrée
  if typeof(sig00)~='div' then
      error('the signal should be a div structure')
  end

  // gestion des filtres de sortie
  if typeof(bandpass)~='div' then
      error('the filters should be a div structure')
  end

  sig=sig00.d;
  [n,nsig]=size(sig);

  // gestion des longueurs d'onde d'entrée
  lam=strtod(sig00.v);
  if find(isnan(lam))~=[] then
      error('please verify that the labels of the signal variable are wavelengths, represented by numbers, without strings')
  end
  // extension gauche / droite du domaine

  // gestion des filtres de sortie
  lout=strtod(bandpass.v);
  nf=length(lout);
  if find(isnan(lout))~=[] then
      error('please verify that the labels of the filter variable are wavelengths, represented by numbers, without strings')
  end


  // génération des filtres
  flt=zeros(nf,nsig);
  for i=1:nf;
     // flt(i,:)=exp( -4*log(2)*( (lam-lout(i)) ./ bandpass.d(i) ).^2 )';
     flt(i,:)=exp( -4*log(2)*( (lam-lout(i)) ./ width ).^2 )';
     s = sum(flt(i,:));
     if s > 0 then
        flt(i,:)=flt(i,:)./s;
     end;
  end;

  //calcul du signal
  x=div(sig*flt')
  x.i=sig00.i;
  x.v=string(lout);

//x=flt;

  //mise en forme du filtre
  flt_out=div(flt);
  flt_out.i=string(lout);
  flt_out.v=sig00.v;




endfunction
