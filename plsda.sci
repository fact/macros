function [model] = plsda(xi,yi,split,lv,varargin)

  //function [conf_matrix,err_cv,err_cal] = pls2da(x,y,split,lv,metric,scale,seuil,class)
  
  // cette fonction appelle cvclass avec la fonction 'pls2da'
  // et met au format Saisir
 

  [model]= cvclass(xi,yi,split,lv,'plsda',varargin(:))

endfunction
