function [saisir] = corrmat(x1,x2)
    
  //corrmap 			- Correlation between two tables
  //function [cor] = corrmat(X1,X2)
  //
  //Input arguments
  //--------------
  //X1 and X2: SAISIR matrix dimensioned n x p1 and n x p2 respectively
  //
  //Output argument:
  //---------------
  //cor: matrix of correlation dimensioned p1 x p2)
  //the tables must have the same number of rows
  //An element cor.d(i,j) is the correlation coefficient between the column i of X1 and j of X2

  // pour avoir l'ordre dvi:
  saisir.d=[];
  saisir.v=[];
  saisir.i=[];

  saisir1=div(x1);
  saisir2=div(x2);
  clear test;
  
  saisir.i=saisir1.v; 
  saisir.v=saisir2.v;

  [nrow1 ncol1]=size(saisir1.d);
  [nrow2 ncol2]=size(saisir2.d);
  if(nrow1~=nrow2) then 
      error('The number of rows must be equal');
  end
  m1=mean(saisir1.d,1);
  m2=mean(saisir2.d,1);
  s1=stdev(saisir1.d,1);
  s2=stdev(saisir2.d,1);
  
  // for variables with no variance
  flag=0;
  for i=1:ncol1;
      if s1(i)< 10*%eps then s1(i)=10**10;  //very high value for next calculation to give corr=0
         flag=flag+1;   
      end
  end
  
  for i=1:ncol2;
      if s2(i)< 10*%eps then s2(i)=10**10;  //very high value for next calculation to give corr=0
         flag=flag+1;
      end
  end
 
  if flag>0 then  disp('function corrmat set one or more correlations to 0 due to a variable without any variance');
  end
 
  
  saisir1.d=saisir1.d -ones(nrow1,1)*m1;
  saisir2.d=saisir2.d -ones(nrow1,1)*m2;

  saisir1.d=saisir1.d./(ones(nrow1,1)*s1);
  saisir2.d=saisir2.d./(ones(nrow1,1)*s2);

  saisir.d=(1/(nrow1-1))*(saisir1.d'*saisir2.d);
  
  //Div
  
  saisir=div(saisir);


endfunction

