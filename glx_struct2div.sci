function  xoutput=glx_struct2div(xinput)

   if xinput.typeof=='div' then
       xoutput.d=xinput.d;
       xoutput.i=xinput.i;
       xoutput.v=xinput.v;
       xoutput.infos=xinput.infos;
       xoutput=div(xoutput);
   else
       error('the input should be a structure with a field typeof equal to div')
   end
   
endfunction
