function [b,tscores,loads,dof] = calvodka(x,y,k,r)

    // distributed under the CeCILL-C license
    // copyright INRA-IRSTEA 2013

    [n,p]=size(x);

    if sum(length(r))==1 then 
       select r
         case 0  then       rparam = x'*sqrt(abs(y));
         case 1  then       rparam = x' * y;
         case 2  then       rparam = x'*(y.*y);
         else               error('invalid value for r');
       end;
    elseif size(r)==size(x'*y) then 
                            rparam=r;
    end;
    
    //normalisation de rparam
    rparam=rparam/sqrt(rparam'*rparam);
    
    // calcul des loadings et b 
    loads = zeros(p,k);
    b = zeros(p,k);
    tscores=zeros(n,k);
    Xx = x'*x;
    tol = norm(size(Xx),'inf')*norm(Xx)*%eps;   //default tolerance value
    tol = tol*10;                          //customized tolerance value
    s = pinv(Xx,tol);s=(s+s')/2;           //so S is perfectly symmetrical!

    for i1=1:k;
       if i1==1 
           POP=eye(p,p); 
       else
           POP=eye(p,p)- s*loads(:,1:i1-1)*inv(loads(:,1:i1-1)'*s*loads(:,1:i1-1))*loads(:,1:i1-1)';
       end;
       loads(:,i1)=POP'*Xx*POP'*rparam;
       loads(:,i1)=loads(:,i1)/sqrt(loads(:,i1)'*s*loads(:,i1));
       
       b(:,i1)=(s*loads(:,1:i1)) * (loads(:,1:i1)'*s) * (x'*y);
       
    end

    // calcul des scores
    tscores=x*s*loads;

    dof=[1:1:k]'+ones(k,1);    // rajouté 1 pour le paramètre r 
        

    
endfunction
