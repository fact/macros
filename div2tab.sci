function x_out = div2tab(x_in, filename, decimal, separator)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

 // x_out = glx_div2tab(x_in, filename, decimal, separator)

  // write a div structure (x_in) on the csv file (filename)
  // decimal and separator are optional parameters
  // updated: 2015, august, 06. by JMR

   x_out=[];

// partie commentée par JMR, car rien n'oblige :
// - un fichier csv a se nommer .csv
// - un nom de fichier à etre en minuscules

//  filename=convstr(filename,'u');
//  aux=strindex(filename,'.CSV');
//  if(isempty(aux))
//     filename=[filename + '.CSV'];
//  end

    x=div(x_in);


    if argn(2) < 4
        separator = ascii(9);
    end
    if argn(2) < 3 then
        decimal = '.';
    end

// pour remplacer les d en e dans les notations scientifiques (provisoire)
    xtmp=strsubst(string(x.d),'D','E')
    m = [["",x.v'];[x.i,xtmp]]

    if decimal~='.' then
        m=strsubst(m,'.',decimal);
    end


    csvWrite(m, filename, separator);

    // sortie div
    x_out = div(x);  // ne sert que pour valider le script de test

endfunction
