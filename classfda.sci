function [res_classdfa,vvectp,vv] = classfda(x,y,lv)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  // vérification que y est bien disjonctif
  [testdisj]=isdisj(y);
  if testdisj=='F' then
      y=conj2dis(y);
  end

  [n,q] = size(x);
  n2=size(y,1);
  nclasses=size(y,2);
  
  if n~= n2 then 
      error('classfda: not the same number of observations in x and y');
  end;
   
  lv=min(lv,nclasses); // pas plus de variables latentes que de classes

  // vérification de l'inversibilité des matrices:
  if rank(y'*y)<max(size(y'*y)) then
      error('classfda: (yprime)y is not invertible, FDA cannot apply')
  end
  
  if rank(x'*x)< max(size(x'*x)) then
      error('classfda: (xprime)x is not invertible, FDA cannot apply')
  end


  // determination de l'inertie interclasse
  b = x'*y*inv(y'*y)*y'*x;

  // determination de l'inertie totale
  t = x'*x;
  m = inv(t);

  // calcul des vecteurs propres et valeurs propres
  vmatSTSB = m*b;

  [vecp,valp]=spec(vmatSTSB);
   vecp=real(vecp);

  vv = diag(valp);
  
  [nul,order]=gsort(vv,'g','d');
  
  vv=vv(order);
  vecp=vecp(:,order);
  
  for i=1:size(vecp,2); 
	  vvectp(:,i)=vecp(:,i)./norm(vecp(:,i)); 
  end;

  lv2=min(lv,size(vvectp,2));

  res_classdfa.scores=x*vvectp(:,1:lv2);
  res_classdfa.rloadings=vvectp(:,1:lv2);

   
endfunction
