function [x_saisir] = cdel(x,index)   
    
 // mis à jour le 14 avril 17 cause Scilab 6.0                               

 //col_delete     - deletes columns of saisir files                            
 //function [X1] = deletecol(X,index)                                         
 //                                                                           
 //input arguments                                                            
 //===============                                                            
 //X:Saisir matrix (n x p)                                                    
 //index:vector indicating the columns to be deleted                          
 //                                                                           
 //Output argument                                                            
 //==============                                                             
 //X1:saisir matrix (n x q) with q <=p                                        
 //                                                                           
 //The deleted columns are indicated by the vector index (numbers of booleans)
 //                                                                           
 // Typical Examples  
 // ================                                                      
 //reduced=col_delete(data,[1 3 5]);//// deletes 3 columns                     
 //reduced1=col_delete(data,sum(data.d)==0); // deletes all the columns with   
 //the sum equal to 0                                                         
                                 
   
  saisir=div(x);
  
  if index==[] then 
    x_saisir=saisir;
  
  else   
    [ni,pi]=size(index);
    if ni==1 then
      index=index';
    elseif ni~=1 & pi~=1 then
      error('index should be a vector, not a matrix')
    end
    
    saisir_d=saisir.d;    // 14 avril 17 pour Scilab 6.0
    saisir_d(:,index)=[];
    x_saisir.d=saisir_d;
      
    saisir_v=saisir.v;
    saisir_v(index,:)=[];
    x_saisir.v=saisir_v; 
      
    x_saisir.i=saisir.i;
   
  end                                                        
   
  x_saisir=div(x_saisir);                                                       

endfunction                                                                  
                           
