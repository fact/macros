function [conf_pcent,err_classif,err_classif_byclass,not_class,not_class_bycl] = conf_obs2pcent(conf_obs,varargin)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  nbcl=size(conf_obs,1);

  // somme des colonnes de conf_obs
  sum_conf_obs=sum(conf_obs,'r')';

  if argn(2)==2 then
     classes_total=varargin(1);
     if sum(classes_total)<sum(conf_obs) then
         error('wrong number of observations in conf_obs2pcent')
     end
  else
     classes_total=sum_conf_obs;
  end
  
  if size(classes_total,1)==1 then
       classes_total=classes_total';   // transposition automatique
  end
 
  
  conf_pcent=conf_obs;
  for j=1:nbcl;
      if classes_total(j)~=0 then
          conf_pcent(:,j)=100*conf_pcent(:,j)/classes_total(j);
      //elseif classes_total(j)==0 & sum_conf_obs(j) ~=0 then 
      //    conf_pcent(:,j)=100*conf_pcent(:,j)/sum_conf_obs(j);
      end 
  end
  // la somme des colonnes de conf_pcent fait 100 si tous les échantillons ont été classés
   
   
  // erreur moyenne de classification, en p.cent (les non-classés sont mal classés) 
  obs_total=sum(classes_total);     // somme de toutes les valeurs de conf 
  err_classif=100*(obs_total-sum(diag(conf_obs)))/obs_total;
  
  
  // erreur de classification par classe, en p.cent (les non classés comptent dans les mal classés)
  err_classif_byclass=zeros(nbcl,1); 
  diag_conf_pcent=diag(conf_pcent);
  
  for j=1:nbcl;
    if classes_total(j)~=0 then
       err_classif_byclass(j)=100-diag_conf_pcent(j);
    end
  end

  if  argn(2)==2 then
  
    // pourcentage de non classés par classe 
    not_class_bycl=zeros(nbcl,1);
   
    for j=1:nbcl;
       if classes_total(j)~=0 then                                   
          not_class_bycl(j)=100*((classes_total(j)-sum_conf_obs(j))/classes_total(j));
       end
    end
    
    // pourcentage de non classés
    not_class=100* (sum(classes_total)-sum(conf_obs))/ sum(classes_total);
  
  else
    not_class=[];
    not_class_bycl=[];
    
  end
 
  
 
endfunction
