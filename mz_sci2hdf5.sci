function res_out=mz_sci2hdf5(res,h5name)
    
    // entrees: -------------------------
    // res: une structure avec les champs .time et .mzdata 

    // sorties:
    // newname2: le nom du fichier de sortie, au format .h5
    // avec les champs:
    // 		ChromatogramIndex
    //  	ChomatogramTime
    //  	SpectrumIntensity
    //		SpectrumMZ
    // 		SpectrumIndex
    
    // preparer les donnes .dat au format .h5
    // load(filedat_in)                        // donne res.time et res.bary
    
    n=length(res.time); 
    
    ChromatogramIndex=n; 
    
    ChomatogramTime=res.time;
    
    SpectrumIntensity=[];
    SpectrumMZ=[];
    SpectrumIndex=[]; 
    
    for i=1:n;
        mz_i=res.mzdata(i);                 // une matrice de 2 colonnes 
        diff_i=mz_i(2:$,1)-mz_i(1:$-1,1);
        if i==1 then 
            SpectrumIntensity=mz_i(:,2);
            SpectrumMZ=[mz_i(1,1); diff_i];
            SpectrumIndex=size(mz_i,1);
        else
            SpectrumIntensity=[SpectrumIntensity; mz_i(:,2)];
            SpectrumMZ=[SpectrumMZ; mz_i(1,1); diff_i];
            SpectrumIndex=[SpectrumIndex;SpectrumIndex($)+size(mz_i,1)];
        end
    end
    
    // on a reconstitue les champs au format h5 
    
    b=h5open(h5name,"w"); 
    h5write(b,"ChromatogramIndex",ChromatogramIndex);
    h5write(b,"ChomatogramTime",ChomatogramTime');
    h5write(b,"SpectrumIntensity",SpectrumIntensity');
    h5write(b,"SpectrumMZ",SpectrumMZ');
    h5write(b,"SpectrumIndex",SpectrumIndex');
    h5close(b)
    
    res_out=1;
    
endfunction
