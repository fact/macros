function [model,rmsec,rmsecv,b,ypredcv] = vodka(xi,yi,split,lv,r,cent)


  // [model] = vodka(x,y,split,lv, r,(centering));
  //
  // Building a regression model using cross validation and the function 'pls'  
  //       -> before this function: pretreatments should have been performed
  //                               (e.g. SNV, EPO, Detrend, Savitsky-Galay)
  //       -> after this function:  the obtained model can be applied to a
  //                                new dataset               
  // Input:
  // ======
  //  xi:                  Saisir structure (x.d,x.v,x.i)
  //                       or descriptors (n rows, p columns)
  //  yi:                  Saisir structure (y.d,y.v,y.i)
  //                       or responses (n rows, 1 column)  
  //  split:               how x is splitted during cross-validation
  //                         -> if scalar, as many blocks are randomly generated
  //                         -> if vector (n rows), it must contain the number 
  //                            of the block for each individual
  //  lv:                  number of latent variables to calculate 
  //  r:                   parameter of vodka, integer or vector (p x 1)
  //  centering (optional) :    if 1, data are centered (default value)
  //                            if 0, data are not centered  
  //
  // Output:
  // =======
  // A Saisir structure containing at least the following data:
  //  rmsec.d:           vector of calibration error (lv rows)
  //  rmsecv.d:          vector of cross validation error (lv rows)
  //  ypredcv.d:         matrix of predictions after cross-validation
  //                                         (n rows by lv columns)
  //  b.d:               b-coefficients of the PLS (p rows by lv columns) 
  // 
  //
  // Miscellaneous:
  // ==============
  // JC Boulet (INRA), JM Roger (IRSTEA), France
  // Last update: 2012, April, 27
  // -------------------------------------------- 


  x=div(xi);
  y=div(yi);
  clear test;
  if x.i~= y.i then warning('labels in x and y do not match')
  end
  
  if argn(2)==5 then  // par défaut: centrage
      cent=1;
  end;
  
  [model] = cvreg(xi,yi,split,'vodka',cent,lv,r);
    
  rmsec=model.err.d(:,1);
  rmsecv=model.err.d(:,2);
  b=model.b.d;
  ypredcv=model.ypredcv.d;
  

endfunction


