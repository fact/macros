function [b,t,p,dof] = calmlr(x,y)

  if  argn(2)~=2 then
    error('wrong number of input arguments in calmlr')
  end

  [n,q]=size(x);
  b=pinv(x'*x)*x'*y;

  t=[];
  p=[];

  dof=0;

endfunction
