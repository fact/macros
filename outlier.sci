function[t2,res,Levcal]=outlier(x0,scores0,loadings0,lv0)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  x=div(x0);
  scores=div(scores0);
  loadings=div(loadings0);
  
  
  lv = min(lv0,size(scores.d,2));

  [n,p]=size(scores.d(:,1:lv));
  res.d=scores.d(:,1:lv);

  //Calcul des variances residuelles (Q)
  for i=1:lv;
     resmat=x.d-scores.d(:,1:i)*loadings.d(:,1:i)';
     res.d(:,i) = (sum(resmat.^2,2));
  end

  //Calcul des T2 de Hotelling
  xc=centering(scores);
  t2.d=xc.d(:,1:lv).^2;
  var=(variance(xc.d(:,1:lv),1));
  op=ones(n,1)*var;
  t2.d=cumsum(t2.d./op,2);


  //Calcul des leviers
  Levcal.d=(1/n)+t2.d/(n-1);

  t2.i= scores.i;
  t2.v='T2-' + scores.v(1:lv);
  t2=div(t2);

  res.i=scores.i;
  res.v='Q-' + scores.v(1:lv);
  res=div(res);

  Levcal.i=scores.i;
  Levcal.v='lev-' + scores.v(1:lv);
  Levcal=div(Levcal);


endfunction
