function [model] = plsfda(xi,yi,split,lv,varargin)

  //function [model] = plsda(x,y,split,lv,metric,scale,seuil,class)
  
  // cette fonction appelle cvclass avec la fonction 'plsda'
  // et met au format Saisir
  
  // attention, lv=[lv1,lv2] avec :
  // lv1=dimension de la PLS2
  // lv2=dimension de la FDA
 

  [model]= cvclass(xi,yi,split,lv,'plsfda',varargin(:))

endfunction
