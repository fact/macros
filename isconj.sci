function testconj=isconj(xi)

  // Verifies if the input data is conjuctive 

  x2=div(xi)
  testconj='T';
  
  x=x2.d;
 
  [n,p]=size(x);
  
  if testconj=='T' then

    if p==1 then
      classes=x(1);
      for i=2:n;
        flag=0;
        for j=1:size(classes,1);
          if x(i)==classes(j) then flag=1;  
          end
        end
        if flag==0 then
          classes=[classes;x(i)];
        end
      end
      [nc,pc]=size(classes);
      if sum(classes)~= nc*(nc+1)/2 then
        testconj='F';
      end
    elseif p~=1 then
      testconj='F'; 
    end
  end
 
 
endfunction
