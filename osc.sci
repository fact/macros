function [p,w,t,lbd] = osc(x,y,f)
    
  // orthogonal signal correction
  // input X   n x q matrix of predictors
  //       Y   n x r matrix of dependent variables
  //       f   scalar, number of factors to return
  //
  // returns T       n x f matrix of scores 
  //         P       q x f matrix of loadings 
  //         W       q x f matrix of weights
  //         lambda  f x 1 vector of eigenvalues as a proportion of
  //                 the sum of the eigenvalues of MX'X
  //
  // The naming of variables corresponds to to Chemolab paper
  // In particular, t_i, p_i, and w_i are the i'th columns of T, P and W

  
  // initialization
  x=div(x);
  y=div(y);
  
  X=x.d;
  Y=y.d;

  [n,q]=size(X);
  M=eye(q,q)-X'*Y*inv(Y'*X*X'*Y)*Y'*X;
  Z=X*M;

  // calculation of C and S according to T.Fearn 
  if q >= n then
    [U,S,V]=svd(Z',0);   
    C=V(:,1:f);   
  else
    [U,S,V]=svd(Z,0);
    C=U(:,1:f);    
  end // ---------------------------------------
  
  // calculation of C and S modified by JC Boulet, 2014, march
  //Z2=Z*Z';
  //[U,S,V]=svd(Z2);
  //S=sqrt(S);  // to get the same S as Fearn
  //C=U(:,1:f); // ---------------------------------------------
  

  rtlam=diag(S(1:f,1:f));
  W=(M*X'*C)./(ones(q,1)*rtlam');
  T=C.*(ones(n,1)*rtlam');
  lam=rtlam.^2;
  P=X'*(T./(ones(n,1)*lam'));
  lambda=lam/sum(diag(S).^2);


  // outputs at the div format
  t.d=T;
  t.i=x.i;
  t.v='osc'+string([1:1:f]');
  t=div(t);
  
  p.d=P;
  p.i=x.v;
  p.v=t.v;
  p=div(p);
  
  w.d=W;
  w.i=x.v;
  w.v=t.v; 
  w=div(w);
  
  lbd.d=lambda;
  lbd.i=t.v;
  lbd.v='ev_pcent';
  

endfunction




