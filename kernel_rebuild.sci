function [x,c]=kernel_rebuild(x0, y0, y, eps );

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  n0=size(x0,1);
  
  if argn(2) < 4 then 
    eps = 1.0/n0;
  end;

  n = size(y,1);

  v = variance(y0);

  a =  y*ones(1, n0) -  ones(n,1)*y0';   // matrice (n x n0)
  
  c=exp(-a.^2/(2*(eps^2)*v)) / (eps*sqrt(2*%pi*v));   // (n x n0)

  
  // rajouté par FG car souvent problème: c contient des valeurs nulles	
  sumlw=(sum(c,'c')*ones(1,n0));
  sumlw(sumlw==0)=1;
  c = c ./ sumlw; 

  //--------------------------------
  
  c = c + (1/n0/mean(y0)) * (y-c*y0)*ones(1,n0);   

  x = c * x0;

endfunction
