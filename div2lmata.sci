function div2lmata(x_div,file_lmata)
    
        // exporte un div dans un fichier au format lmata 
 
        // x_div: un div
        
        // file_lmata: un fichier texte au format lmata
        // 1° ligne = nbre de lignes 
        // 2° ligne = labels des lignes 
        // 3° ligne = nbre de colonnes
        // 4° ligne = labels des colonnes 
        // lignes suivantes: les données séparées par un espace 
        
        [n,q]=size(x_div.d);
        
        line1=string(n);
        
        line2=x_div.i(1);
        for i=2:n;
            line2=line2 + ' ' + x_div.i(i);
        end
        
        line3=string(q);
        
        line4=x_div.v(1);
        for i=2:q;
            line4=line4 + ' ' + x_div.v(i);
        end
        
        
        // ecriture dans le fichier   
        file_id=mopen(file_lmata,'wt');
        mputl(line1,file_id);
        mputl(line2,file_id);
        mputl(line3,file_id);
        mputl(line4,file_id);
        for i=1:n;
            data_line=stripblanks(string(x_div.d(i,1)));
            for j=2:q;
                data_line=data_line + ' ' + stripblanks(string(x_div.d(i,j)));
            end
            data_line=data_line;
            mputl(data_line,file_id);
        end
        mclose(file_id);
        
        // verification 
        
        
        
    
    
endfunction
