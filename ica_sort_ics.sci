function [SigTemp,ScoTemp] =  ica_sort_ics(SigTemp,ScoTemp);

// Sort ICs base on successive correlations

 // get Scilab version 
  sci_version=part(getversion(),[8,9,10]);  // a string
  sci_version=strtod(sci_version);          // a number 
  if sci_version < 6 then
      accolade1='(';
      accolade2=')';
  else 
      accolade1='{';
      accolade2='}';
  end

  [Blocks,MaxICs]=size(SigTemp);

  for bloc=1:Blocks
    for ICs=2:MaxICs
        execstr(msprintf('max_corrs=corrmat(SigTemp%s bloc,ICs-1 %s.entries.d,SigTemp%s bloc,ICs %s .entries.d);',accolade1,accolade2,accolade1,accolade2)) //max_corrs=div
        MaxAbsCorrs=max(abs(max_corrs.d),'r');                                          //corrmat = Corr_DNR
                            
        execstr(msprintf('temp1=[MaxAbsCorrs;SigTemp%s bloc,ICs %s.entries.d];',accolade1,accolade2))
        execstr(msprintf('temp2=[MaxAbsCorrs;ScoTemp%s bloc,ICs %s.entries.d];',accolade1,accolade2))
        temp1 = gsort(temp1','lr','i');
        temp2 = gsort(temp2','lr','i');

        temp1bis=temp1(:,2:$)';
        temp2bis=temp2(:,2:$)';

        execstr(msprintf('SigTemp%s bloc,ICs %s.entries.d=temp1bis;',accolade1,accolade2))
        execstr(msprintf('ScoTemp%s bloc,ICs %s.entries.d=temp2bis;',accolade1,accolade2))
    end;
  end;

endfunction
