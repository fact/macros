function lot = cvgroups(mx,split)
 
  // mx:                nombre de valeurs pour lesquelles un regroupement est nécessaire
  // split (ancien):    la manière dont le regroupement est obtenu
  //                    ex: 'jck5'  5 groupes par Jack Knife (= 5 groupes contigus)
  //                        'vnb5'  5 groupes par Stores Vénitiens
  //                            5   5 groupes tirés au hasard
  // split (nouveau):   split.method
  //                    split.nblocks
  //                    split.nbrepeat
  //                    split.sameobs

  // lot:       vecteur de longueur mx codant les groupes en conjonctif pour les méthodes 'random,','jack' et 'venitian'
  //                                                   et en disjonctif pour 'prefixed'
  //            pour chaque observation


  // --------------------------------------------------ancienne version (ci-dessous)
  if typeof(split)~='st' then
    lot=cvgroups_2(mx,split);
  end

  // -------------------------------------------------nouvelle version, a/c de nov2018
  if typeof(split)=='st' then 

    flag=0;   // correction de bug, 5 avril 16: pour avoir tous les groupes représentés

    while flag==0;

        sameobs=split.sameobs;                         // longueur mx
        sameobs_unique=unique(sameobs);
        mx_u=max(size(sameobs_unique));
        sameobs_number=zeros(mx_u,1);
        for i=1:mx_u;
            //pause
            temp=find(sameobs==sameobs_unique(i));
            sameobs_number(temp)=i;                    // sameobs_number contient les n° de groupes de 1 à mx_u
        end

        // ex: 2 2 1 2 1 1 3 4 3 3 4 5 5 6 6 7 7 7 8 8
        // les n° identiques correpondent à des répétitions
        // il ne faut surtout pas les dissocier dans la CV
                                                       
        if split.method=='jack' then
            nsplit = split.nblocks;                     // nbre de classes
            bysplit = round(mx_u/nsplit);               // taille d'une classe

            lot0=ceil(sameobs_number/bysplit);

        elseif split.method=='venitian' then
            nsplit = split.nblocks;
            bysplit = round(mx_u/nsplit);               // taille d'une classe

            venitian_list=repmat([1:nsplit]',mx_u+1,1);    // +1 pour en avoir plus que besoin

            lot0=zeros(mx,1);
            for i=1:mx_u;
                list_temp=find(sameobs_number==sameobs_unique(i));
                lot0(list_temp)=venitian_list(i);

            end
//pause
        elseif split.method=='random' then
            if split.nblocks >= mx/2  then
                split.nblocks=2;
            end;
            x_date=getdate("s");                    // pour avoir chaque fois un tirage totalement aléatoire
            rand("seed",x_date);

            lot0=zeros(mx,split.nbrepeat);
            for i=1:split.nbrepeat;
                lot0(:,i)=ceil(rand(mx,1)*split.nblocks);
            end

        elseif split.method=='prefixed' then        // on transfère la variable sans rien faire
            lot0=split.nblocks;


    end         // fin de la boucle 'random' 'jack' 'venitian' 'prefixed'
    

        // vérification que tous les groupes sont représentés (les colonnes de lot0 = conjonctives ou disjonctives)
        // erreur possible avec prefixes ou hasard 

        for i=1:size(lot0,2);
            if isconj(lot0(:,i))=='T' | isdisj(lot0(:,i))=='T' then
                flag=1;
            end;
        end
        
        // tout mis en disjonctif
        if isconj(lot0(:,1))=='T' then
            lot=zeros(size(lot0,1),max(lot0(:,1)),size(lot0,2));      // max(lot(:,1)) = nombre de classes
            for i=1:size(lot0,2);
                lot(:,:,i)=conj2dis(lot0(:,i));
            end;
        else 
            lot=lot0;
        end
        
     end                                                         // fin du flag

   end

endfunction


// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


function lot = cvgroups_2(mx,split)

// ancienne fonction cvgroups (jusqu'au 5 nov 2018)
// mx:        nombre de valeurs pour lesquelles un regroupement est nécessaire
// split:     manière dont le regroupement est obtenu
//            ex: 'jck5'  5 groupes par Jack Knife
//                 'blk5' 5 gtoupes contigus
//                 'vnb5' 5 groupes par Stores Vénitiens
//                  5     5 groupes tirés au hasard
// lot:       vecteur de longueur mx codant les split groupes (1,2,...split)
//            pour chaque observation

flag=0;   // correction de bug, 5 avril 16: pour avoir tous les groupes représentés

while flag==0

if type(split)==10  then  // if split is a string


if     part(split,1:3)=="vnb"  then  // venitian blind
nsplit = strtod(part(split,4:length(split)));
bysplit = round(mx/nsplit);

lot=[];
for i=1:bysplit+1;
lot=[lot; [1:1:nsplit]'];
end

lot = lot(1:mx);

elseif part(split,1:3)=="blk" | part(split,1:3)=="jck"  then  // blocks =  jacknife
nsplit = strtod(part(split,4:length(split)));
bysplit = round(mx/nsplit);
lot = ones(mx,1).*nsplit;
for i=1:nsplit
lot((i-1)*bysplit+1:i*bysplit)=i;
end
lot = lot(1:mx);

else
error('Incorrect split argument')
end;

else // if split is not a string

if prod(size(split))==1  then  // simple random subsets

if split >= mx      // leave one out
lot = (1:mx)';
else                // random subset
lot=ceil(rand(mx,1)*split);
end;

else                      // simple fixed subsets
if prod(size(split))==2  then  // double random subsets
lot=ceil(rand(mx,split(2))*split(1));
else
if size(split,1)==mx  then // double fixed subsets
for k=1:size(split,2)
lot(:,k)=str2conj(split(:,k));
end;
else
error('Incorrect split argument')
end;
flag=1;

end;
end;
end



// vérification que tous les groupes sont représentés (les colonnes de lot = conjonctives)

if (type(split)==10 | (type(split)~=10 & prod(size(split))==1)) & isconj(lot)=='T' then
flag=1;
elseif type(split)~=10 & prod(size(split))==2 then
[n,p]=size(lot);
ktot=0;
for i=1:p;
if isconj(lot(:,i))=='T' then  // on compte les "bonnes" colonnes
ktot=ktot+1;               // modifs 14juin16
end
end
if ktot==p then
flag=1;
end
end


end   // end du flag


endfunction












