function xoutput=div2mat(xinput,matfilename,varnameout)
    
    // xinput: Div ou liste ou structure comprenant comme éléments des structures ou des fichiers classiques
    // matfilename: nom du fichier .mat de sortie
    // varnameout: nom de la variable sauvegardée dans matfilename; elle contient xinput  
    // JCB 1/08/16
    
    
    // nom du fichier de sortie par défaut 
    if argn(2)< 3 then
        varnameout=strsplit(matfilename,'.');
        varnameout=varnameout(1);
    end
  
    if typeof(xinput)=='div' then   // xdiv est bien une structure Div
            xoutput=glx_div2struct(xinput);

    elseif typeof(xinput)=='st' then
            xoutput=div2mat_div2struct(xinput);
    
     elseif typeof(xinput)=='list' then 
            xoutput=glx_list2struct(xinput);                        
     
     else     
          error('the input should be a div, a structure or a list')
     end
  
    if  ~isfield(xoutput,'typeof') then
        xoutput.typeof='st';
    end
  
    execstr(msprintf('%s=xoutput;',varnameout))

 //pause
 
    execstr(msprintf('savematfile %s %s -v6;',matfilename,varnameout));
    
endfunction





