function [z0] = gapremove(sp0,sites,w) 

// Removes the vertical gaps in spectra, by means of
// linear regression computed on the left side of the gaps
//
// z = gapremove( x, sites, w );
//
// x : matrix (n x p) of n spectra by p wavelengths
// sites : vector of index corresponding to the right part of the gaps to
//   correct for. For example, if gaps are observed between indexes 245-246
//   and 520-521, sites should contain [246 521].
// w : number of points used for estimating the slope
//
// JM Roger, Cemagref ITAP


sp1=div(sp0);
sp=sp1.d;

[n,p] = size(sp);
z = sp;

sites = gsort(sites,'g','i');

for i = 1:max(size(sites));
  // index of the gap to process
  k = max( min( sites(i), p ), w+1 );
  // build a predictive matrix
  x = [(k-w:k-1)' ones(w,1)];
  // ... and the associated responses
  y = z(:,k-w:k-1)';
  // computes n linear models
  b = x\y;
  // apply them to the absciss of the gap
  pr = [k,1]*b;
  // correct the bias
  z(:,k:$)=z(:,k:$)+repmat(pr'-z(:,k),1,p-k+1);
end;

z0.d=z;
z0.i=sp0.i;
z0.v=sp0.v;
z0=div(z0);


endfunction
