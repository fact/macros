function [model] = copda(xi,yi,split,lv,varargin)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
   //-----------------------------------------------------------------
  // Adjustment of the input data to the Saisir format (if necessary) 
  //-----------------------------------------------------------------
 
  //x=div(xi);
  //y=div(yi);

  //if  size(x.d,1)<2 then error ('input data must contain at least 2 observations')
  //end

  // calcul du modèle plsda 
  [model]= cvclass(xi,yi,split,lv,'copda',varargin(:))    // 15sept14

endfunction
