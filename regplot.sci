function regplot(ym,yp,pointstyle,trend,unit,fig_title) 

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  global newfig

  // définition des options par défaut
  if argn(2)<=5                         then   fig_title='';           end;
  if argn(2)<=4                         then   unit='';                end;
  if argn(2)<=3                         then   trend ='' ;             end;

  // définition de couleur et marqueur: ils deviennent permutables!
  color_symbols=['r';'g';'b';'c';'m';'y';'k'];
  marker_symbols=['+';'o';'*';'.';'x';'s';'^';'v';'<';'>';'pentagram'];
                                      // obtenus d'après help LineSpec
  if ~isdef('pointstyle')| pointstyle==[]  then   pointstyle='ko';
  else  
    for i=1:length(pointstyle); 
      if find(color_symbols==part(pointstyle,i:i))~=[] then
         trend_color=part(pointstyle,i:i);
      elseif find(marker_symbols==part(pointstyle,i:i))~=[] then
         marker=part(pointstyle,i:i); 
      end
    end
    if ~isdef('trend_color') then trend_color='k'; end;  
    if ~isdef('marker') then marker='o';end  
    pointstyle=trend_color+marker; 
  end
  

  // mise en forme des données   
  ym2=div(ym);
  yp2=div(yp);
  clear test1 test2;

  ymes=ym2.d;
  if size(ymes,1)==1 then ymes=ymes'; end
  ypred=yp2.d;
  if size(ypred,1)==1 then ypred=ypred'; end


  // nouvelle figure
  if  newfig=='T' then
     figure();
  end


  // représentation graphique
  plot(ymes,ypred,pointstyle);
  h = gcf();
  h.background=8;
    
  // définition des bornes max et min
  a1x=floor(min(ymes));
  a1y=floor(min(ypred));
  a2x=ceil(max(ymes));
  a2y=ceil(max(ypred));

  a1=min(a1x,a1y);
  a2=max(a2x,a2y);

  
  // impression de y=x
  plot([a1,a2],[a1,a2],'k');
  
  
  // legendes des axes"
  if ~isempty(unit) then
    xtitle("", sprintf("Reference values (%s)",unit), sprintf("Predicted values (%s)",unit) );
  else
    xtitle("","Reference values","Predicted values");
  end;


  // biais, pente, droite de régression
  bs = mean( ypred - ymes );
  se = stdev( ypred - ymes );
  r2 = (((ypred-mean(ypred))'*(ymes-mean(ymes)))/stdev(ypred)/stdev(ymes)/(length(ymes)-1))^2;
  rmse=sqrt(bs**2 + se**2);

  if trend=='t' then
    b = [ymes,ones(size(ymes,1),1)]\ypred;
    plot([a1;a2],[[a1;a2],[1;1]]*b,sprintf('%s:',trend_color));
  end
 
 
  // positionnement des légendes (R2, RMSEP et cie) dans la figure 
  c2=a2-0.25*(a2-a1);
  c1=a1+0.02*(a2-a1);

  if trend~='t' then
    xstring(c1,c2,sprintf(" R² = %.2f ;  RMSE = %.2f %s \n bias = %.2f %s; std = %.2f %s \n  \n ",r2,rmse,unit,bs,unit,se,unit));
  elseif trend=='t' then
    xstring(c1,c2,sprintf(" R² = %.2f ;  RMSE = %.2f %s \n bias = %.2f %s ; std = %.2f %s \n trend line (dashed): \n offset = %.2f %s \n slope = %.2f  ",r2,rmse,unit,bs,unit,se,unit, b(2),unit, b(1)));
 end;

  title(fig_title)


endfunction




