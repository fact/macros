function [model] = cvreg(xi,yi,split,func,cent,varargin)


  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013


  //------------------------------
  // linear vs. non-linear models
  //------------------------------
  if part(func,[1 2])=='nn' then
      flag='NL';
  else
      flag='L';
  end


  //-----------------------------------------------------------------
  // Adjustment of the input data to the Saisir format (if necessary) 
  //-----------------------------------------------------------------
  [x]=div(xi);
  [y]=div(yi);
  if x.i~= y.i then warning('labels in x and y do not match')
  end
  
  
  // --------------
  // Initialization
  // --------------      
  [n,p]=size(x.d);   
  x_mean=mean(x.d,1);
  y_mean=mean(y.d,1);    
  
  

  if flag=='L' then
        [rmsecv,ypredcv] = cvxx( x.d, y.d,split,func,cent,varargin(:));
        [rmsec,predU,bsU,last_argout] = caltestxx(x.d,y.d,x.d,y.d,func,cent,varargin(:));
        dof=last_argout.dof;
  elseif flag=='NL' then
        [rmsecv,ypredcv] = cvxx( x.d, y.d,split,func,cent,varargin(:));
        [rmsec,predU,bsU,ploads,wh,wo] = caltestxx(x.d,y.d,x.d,y.d,func,cent,varargin(:));
        dof=[];
  end
  
  // -------------------
  // degrees of freedom
  // -------------------
  lv=size(rmsec,1);

  rmsec=sqrt(rmsec.^2 .* (n*ones(lv,1)) ./  ((n-cent)*ones(lv,1)-dof));    


  // ------
  // Output
  // ------

/////// à faire par la fonction calpls, calpcr, calmlr, etc.

  namelv=string([1:size(rmsec,1)]');
  namelv='LV'+namelv;
  
  model.err.d=[rmsec rmsecv];
  model.err.i=namelv;
  model.err.v=['rmsec';'rmsecv'];
   
  model.ypredcv.d=ypredcv;
  model.ypredcv.i=x.i;
  model.ypredcv.v=namelv;

//  if isdef('bcoeffs') then
//      if bcoeffs~=[] then    // cas de calnnpls
//          model.b.d=bcoeffs;
//          model.b.i=x.v;
//          model.b.v=namelv; 
//      end
//  end

  if isfield(last_argout, 'bcoeffs') then
      if last_argout.bcoeffs~=[] then    // cas de calnnpls
          model.b.d=last_argout.bcoeffs;
          model.b.i=x.v;
          model.b.v=namelv; 
      end
  end

 
 
  // pour gérer MLR et RIDGE
//  if flag=='L' & tscores~=[] then  // modèle linéaire avec scores
//    model.scores.d=tscores;
//    //model.scores.i='i'+string([1:1:size(x.i,1)]');    // modifié pour compatibilité avec curves
//    model.scores.i=x.i;                                 // remodifié 21 juin 2016
//    model.scores.v=namelv;    
//  end
//  
//  if  ploads ~=[] then
//    model.loadings.d=ploads;
//    model.loadings.i=x.v;
//    model.loadings.v=namelv;
//  end
  
  if flag=='L' & isfield(last_argout,'tscores') & last_argout.tscores~=[] then  // modèle linéaire avec scores
    model.scores.d=last_argout.tscores;
    //model.scores.i='i'+string([1:1:size(x.i,1)]');    // modifié pour compatibilité avec curves
    model.scores.i=x.i;                                 // remodifié 21 juin 2016
    model.scores.v=namelv;    
  end
  
  if isfield(last_argout,'ploads') & last_argout.ploads ~=[] then
    model.loadings.d=last_argout.ploads;
    model.loadings.i=x.v;
    model.loadings.v=namelv;
  end
  
  
  if flag=='NL' then
    model.wh=wh;
    model.wo=wo;
  end
 
  model.y_ref=[];
  model.y_ref=y;
 
  model.x_mean.d=x_mean;
  model.x_mean.i='x_mean';
  model.x_mean.v=x.v;

  
  model.y_mean.d=y_mean;
  model.y_mean.i='y_mean';
  model.y_mean.v='y_mean';
  model.y_mean=div(model.y_mean);
  
  model.center=cent;
  
  model.method=func;

  model.err=div(model.err);  
  model.err(5)=['number of latent variables';'RMSE'];
  
  model.ypredcv=div(model.ypredcv);
  model.ypredcv(5)=['observations';'y predicted'];

  if flag=='L' & last_argout.bcoeffs ~=[] then
     model.b=div(model.b);
     model.b(5)=['initial variables';'b-regression coefficients'];
  end 
   
  model.x_mean=div(model.x_mean);
  model.x_mean(5)=['mean vector';'initial variables']
  model.x_mean=model.x_mean';
    
  if flag=='L' & last_argout.tscores~=[] then
    model.scores=div(model.scores);
    model.scores(5)=['observations';'scores of the observations onto the loadings ']
  end
  
  if flag=='L' & last_argout.ploads~=[] then  
    model.loadings=div(model.loadings);
    model.loadings(5)=['initial variables';'scores of the loadings '];
  end

 

endfunction
