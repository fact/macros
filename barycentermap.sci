function barycentermap(s,col1,col2,classid,varargin)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013 

  global newfig;
  
  [nargout,nargin] = argn(0); 
  s2=div(s);
  classid2=div(classid);
  [n,p]=size(s2.d);

  // calculation of the barycenters
  maxgr=max(classid2.d,'r');  
  barycenter=zeros(maxgr,p);

  for i=1:maxgr
      barycenter(i,:)=mean(s2.d(classid2.d==i,:),1);
  end  

  // determination of the colormap
  cmap2=[0,0,0;1,0,0;0,0,1;0,0.7,0;0.5,0.5,0;0.25,0,0.25;0,0.5,0.5;0.25,0.25,0.25;0.5,0,0;0,0.5,0;0,0.5,0;0.1,0.2,0.3;0.3,0.2,0.1;0.5,0.5,0.8;0.1,0.8,0.1];
  
  if newfig=='T' then
    figure();
  end
  
  f=gcf(); f.color_map=cmap2; 
  f.background=-2;           //blanc!

  // determination of the frame of the graph
  margin=0.05;
  minx=min(s2.d(:,col1));maxx=max(s2.d(:,col1));
  miny=min(s2.d(:,col2));maxy=max(s2.d(:,col2));
  minx=minx-(maxx-minx)*margin;
  miny=miny-(maxy-miny)*margin;
  maxx=maxx+(maxx-minx)*margin;
  maxy=maxy+(maxy-miny)*margin;
  a=get("current_axes");
  a.data_bounds=[minx,miny;maxx,maxy];
  a.axes_visible = 'on' ;
  a.box="on";

  // plot of the labels associated to the different points 
  for k=1:n
      pos=s2.d(k,[col1 col2]);
      g=classid2.d(k);
      xbar=barycenter(g,[col1 col2]);
      xpoly([pos(1) xbar(1)],[pos(2) xbar(2)],"lines");
      p=gce(); 
      p.thickness=2;                                   // taille des traits
      tag=pmodulo(g,15)+1;
      p.foreground=tag;
      p.background=tag;
      graphid=string(s2.i);
 
      xstring(s2.d(k,col1),s2.d(k,col2),graphid(k));  
 
      a = get("hdl")  
      a.font_foreground = tag;  
     

      if argn(2)>=5 then
        b = get("current_axes");
        b.x_label.text=varargin(1);
        if argn(2)>=6 then
          b.y_label.text=varargin(2); 
        end
        if argn(2)==7 then
             a.font_size=varargin(3);  
        else 
             a.font_size=1;     
        end
      end
        
  end
  

endfunction

