function xo = normc(m0)

  //   norm(m) normalizes the columns of m to a length of 1.
  //   Example: m = [1 2; 3 4]; n = normc(m)
  //   Last update: 2014, sept., 05

  [nargout,nargin] = argn(0) 
  if nargin < 1,error('Not enough input arguments.'); end

  xo=div(m0);
  
  m=xo.d;
  
  [mr,mc] = size(m);
  if (mr == 1) then
       xn=ones(1,mc);
  else xnormsquare=diag(m'*m);
       xnorm=sqrt(xnormsquare);
      
       xn=m;
       for i=1:mc;
           if xnorm(i)~=0 then
               xn(:,i)=m(:,i)./(ones(mr,1)*xnorm(i));
           end
       end
  end
 
  xo.d=xn;
  
endfunction

