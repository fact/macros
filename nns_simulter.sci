function [sse,gra,hes]=nns_simulter(wh,wo,x,y)
    
    // mise de la fonction c_nnsimulter au format Div
    
    wh=div(wh);
    wo=div(wo);
    x=div(x);
    y=div(y);
    
    [sse.d,gra.d,hes0]=c_nnsimulter(wh.d,wo.d,x.d,y.d);

    sse.i='sse';
    sse.v=y.v;    
    sse=div(sse);
    
    [nwh,qwh]=size(wh.d);
    [nwo,qwo]=size(wo.d);
    wh_i=[];
    for i=1:qwh;
        wh_i=[wh_i;wh.i+wh.v(i)];
    end
    wo_i=[];
    for i=1:qwo;
        wo_i=[wo_i;wo.i+wo.v(i)];
    end
    
    gra.i=[wh_i;wo_i];
    gra.v=y.v;
    
    gra=div(gra);

    if size(y.d,2)==1 then
	hes.d=hes0;
	hes.i=gra.i;
	hes.v=gra.i;
	hes=div(hes);
    else
	hes=hes0;
    end

    
endfunction
