function res2=statis(col0);

    // distributed under the CeCILL-C license
    // copyright INRA-IRSTEA 2013


    [test0,col]=islist(col0);

    WK=list();
    ntable=size(col);
    for i=1:ntable;
        chaine=('t'+ string(i)+ '             ');
        table_name(i)=part(chaine,1:10);
    end

    obs_name=col(1).i;// 1° table => id noms observations 
    // centring the tables and assessement of the Wk matrices
    for i=1:ntable;
        x=col(i).d;
        [nrow ncol]=size(x);
        ave=mean(x,1);

        X=(x-ones(nrow,1)*ave);

        Wk=X*X';
        Wk=Wk/norm(Wk,'fro');//// normed objects !!!

        //=============

        aux(i,:)=matrix(Wk,1,nrow*nrow);

        WK(i).d=Wk;     
        WK(i).i=col(i).i; 
        
        aux1=(string(i)+ '     ');
        aux1=part(aux1,(1:3));
        aux1=char(ones(nrow,1)*ascii(aux1));

        WK(i).i= aux1 + col(i).i;   
        WK(i)=div(WK(i));
         
    end;

    RV=aux*aux';
    res.rv.d=RV;
    res.rv.i=table_name;
    res.rv.v=table_name;
    res.rv=div(res.rv);

    [V,D]=svd(RV); 
    D=D(1,1);
    V=V(:,1);
    
    res.rv_eigval1.d=D;
    res.rv_eigval1.i='First eigenvalue of RV';
    res.rv_eigval1.v='value';
    res.rv_eigval1=div(res.rv_eigval1);

    res.rv_eigvec1.d=V;
    res.rv_eigvec1.i=table_name;
    res.rv_eigvec1.v='First eigenvector of RV';
    res.rv_eigvec1=div(res.rv_eigvec1);

    res.wk=WK;

    aux=WK(1).d; 

    W=V(1)*aux;
    
    for i=2:ntable
        aux=WK(i).d;
        W=W+V(i)*aux;
    end
    W=W/ntable;
    
    res.wcompro.d=W;
    res.wcompro.i=obs_name;
    res.wcompro.v=obs_name;
    res.wcompro=div(res.wcompro);

    // scores of the tables
    r=rank(W);
    [V1,D1,V2,r]=svd(W);
    V1=V1(:,1:r);
    D1=D1(1:r,1:r);
    
    res.wcompro_eigval.d=diag(D1);
    res.wcompro_eigval.v='eigenvalues of the W compromise';
    res.wcompro_eigval.i=string((1:r)');

    res.wcompro_eigval=div(res.wcompro_eigval);

    res.wcompro_eigvec.d=V1;
    res.wcompro_eigvec.i=obs_name;
    res.wcompro_eigvec.v=res.wcompro_eigval.i;
    res.wcompro_eigvec=div(res.wcompro_eigvec);

    res.wcompro_scores.d=W*V1/sqrt(D1);
    res.wcompro_scores.i=obs_name;
    res.wcompro_scores.v=res.wcompro_eigval.i;
    res.wcompro_scores=div(res.wcompro_scores);

    //assessment of the trajectory of the observation according to the tables
    t.d=[];
    t.i=[];
    g.d=[];
    
    for i=1:ntable
        aux=WK(i).d;  // 1aout19
        aux1=V(i)*aux;r
        t.d=[t.d;aux1];
        t.i=[t.i;WK(i).i];
        g.d=[g.d; (1:nrow)'];
    end
    t.d=t.d*V1/sqrt(D1);
    t.v=res.wcompro_eigval.i;

    res.trajectory=t;
    res.trajectory=div(res.trajectory);

    g.i=t.i;
    g.v='Rank of the observation';

    res.xgroup=g;
    res.xgroup=div(res.xgroup);

    // assessing the factorial map of tables
    [V,D]=spec(RV);
    [D,index]=gsort(diag(D)','g','d');

    V=V(:,index);
    res.rv_tables_scores.d=V*sqrt(diag(D));
    res.rv_tables_scores.i=table_name;
    aux1=D/sum(D)*100;

    clear aux;   
    aux=[];
    aux=xdisp('score # ',1 ,' ',round(aux1(1)*10)/10,'%');
    for i=2:ntable
        aux=[aux; xdisp('score # ',i ,' ',round(aux1(i)*10)/10,'%')];
    end
    res.rv_tables_scores.v=aux;
    res.rv_tables_scores=div(res.rv_tables_scores);

    res.rv_tables_eigval.d=D';
    res.rv_tables_eigval.v='eigenvalues of RV';
    res.rv_tables_eigval.i=res.rv_tables_scores.v;
    res.rv_tables_eigval=div(res.rv_tables_eigval);

    // classification des sorties et tri
    res2.rv=res.rv;
    res2.rv_eigval1=res.rv_eigval1;
    res2.rv_loading1=res.rv_eigvec1;    
    res2.rv_tables_scores=res.rv_tables_scores;
    res2.rv_tables_loadings=res.rv_tables_eigval;
    res2.wcompro=res.wcompro;
    res2.wcompro_eigval=res.wcompro_eigval;
    res2.wcompro_scores=res.wcompro_scores;
    res2.wcompro_loadings=res.wcompro_eigvec;
    res2.wk=res.wk;
    res2.trajectory=res.trajectory; 
    

endfunction


