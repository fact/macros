function c=patch_pls_c_yloadings(respls)
    
      // fonction de calcul du vecteur c = loadings de y après régression pls
      // note: c n'est pas au format div
      
      // initialisation de yi:
      y=respls.y_ref;
      
      // gestion automatique du centrage:
      yi=y.d-respls.center*respls.y_mean.d;
      
      // dimension de la pls:
      a=size(respls.err.d,1);
      
      // scores de la pls (les mêmes pour x et y):
      t=respls.scores;
      
      // nombre d'observations:
      n=size(t.d,1);
      
      // initialisation de c:
      c=zeros(a,1);
      
      for i=1:a;
          c(i)=yi'*t.d(:,i)/ (t.d(:,i)'*t.d(:,i));
          yi=yi-c(i)*t.d(:,i);
      end
      
    
    
endfunction
