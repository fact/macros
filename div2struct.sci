function xoutput=div2mat(xinput,matfilename,varnameout)
    
    // xinput: Div ou liste ou structure comprenenant comme éléments des structures ou des fichiers classiques
    // matfilename: nom du fichier .mat de sortie
    // varnameout: nom de la variable sauvegardée dans matfilename; elle contient xinput  
    // JCB 1/08/16
    
    
    // nom du fichier de sortie par défaut 
    if argn(2)< 3 then
        varnameout=strsplit(matfilename,'.');
        varnameout=varnameout(1);
    end
    
    
  
    if typeof(xinput)=='div' then   // xdiv est bien une structure Div
            xoutput=glx_div2struct(xinput);

    elseif typeof(xinput)=='st' then
            fields_names = fieldnames(xinput);  // liste des champs en entrée 
            nfields=size(fields_names,1);       // nombre de champs en entrée 
            xoutput=[];
            for i=1:nfields;
                execstr(msprintf('field_level1=xinput.%s;',fields_names(i)));
                if typeof(field_level1)=='div' then
                    execstr(msprintf('xoutput.%s=glx_div2struct(field_level1);',fields_names(i)));
                elseif typeof(field_level1)=='constant' | typeof(field_level1)=='hypermat' then  
                    execstr(msprintf('xoutput.%s=field_level1;',fields_names(i)));  
                elseif typeof(field_level1)=='string' then
                    field_level1=justify(field_level1,'l');
                    execstr(msprintf('xoutput.%s=field_level1;',fields_names(i))); 
                elseif typeof(field_level1)=='list' then
                    execstr(msprintf('xoutput.%s=glx_list2struct(field_level1);',fields_names(i)));
                end    
             end  
     elseif typeof(xinput)=='list' then 
            xoutput=list2struct(xinput);                        
     
     else     
          error('the input should be a div, a struture or a list')
     end
  
    execstr(msprintf('%s=xoutput;',varnameout))

    savematfile(msprintf('%s',matfilename),msprintf('%s',varnameout),'-v6' ) 
    
    
endfunction
