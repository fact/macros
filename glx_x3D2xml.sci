function glx_x3D2xml(x,xmlfile)

    // x est une matrice 3D, de dimensions (n x p x q)
    doc = xmlDocument(xmlfile);
    root=xmlElement(doc,"root");
    doc.root=root;
   
    
    // Initialisation
    [n,p,q]=size(x);
    

    // construction de la structure
    for i=1:q;
        name='plan'+string(i)
        plani=x(:,:,i);
        
        // mise au format csv: separation par ','
        // note: doit pouvoir bien s'améliorer!
        xd=[];
        for j=1:n;
            xdj=[];
            for k=1:p-1;
                xdj=xdj + string(plani(j,k)) +',';
            end
            xdj=xdj+string(plani(j,p)); 
            xd=[xd;xdj];
        end
        
        //pause
        execstr(msprintf("%s=xmlElement(doc,name)",name))
        //execstr("xmlAppend(doc,name);");
        execstr(msprintf('doc.root.children(i)=%s;',name));
   
        doc.root.children(i).content=string(xd);
       
    end
    
    xmlWrite(doc,xmlfile,%t)

    xmlDelete('all')

endfunction



