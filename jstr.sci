function str=jstr(number,ndigit);
    
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  [n,p]=size(number);
  if((p~=1)&(n~=1)); //// not a vector
       error('The first argument must be a row or a column vector');
       str=[];
       return;
  end

  if(n<p)
       number=number';
       disp('The vector has been transposed');
       [n,p]=size(number);
  end

  if  argn(2)==1 | argn(2)==3 & ndigit<max(length(number)) then  
    ndigit=max(length(number));  
  end

  // création d'un vecteur de blancs de dimension (n x 1)
  blanc=' ';

  for i=1:n-1;
     blanc=blanc+' ';
  end
  
  // ajout du vecteur de blancs:
  n2=ndigit-min(length(number))+1;
  for i=1:n2;
    number=number+blanc;
  end

  // sélection des ndigit+1 premiers caractères:
  str=part(number,1:ndigit+1);


endfunction

