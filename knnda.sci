function [model_kppv] = knnda(x,yclasses,split,nn,varargin)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
  model_kppv=cvclass2(x,yclasses,split,nn,'knnda',varargin(:));
 
 
endfunction
