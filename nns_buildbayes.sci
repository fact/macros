function [res]=nns_buildbayes(Wh0,Wo0,Inp0,Out0,Opt)
 
// function [Wh,Wo,StdRes,CovW,Opt]=nns_buildbayes(Wh,Wo,Inp,Out,Opt)
    
// Neural Network for Static models -- Training with Bayesian regularization
//
// Simplified usage:
//     [Wh,Wo] = NNSTrainBayes(Wh,Wo,Inp,Out)
//
// Advanced usage:
//     [Wh,Wo,StdRes,CovW,Opt] = NNSTrainBayes(Wh,Wo,Inp,Out,Opt)
//
// Var   Size         Significance
// ----  ----------   -----------------------------------------
// Ni   (1 x 1)       Number of network inputs (without bias)
// Nh   (1 x 1)       Number of neurons in the hidden layer (without bias)
// No   (1 x 1)       Number of network outputs
// Nd   (1 x 1)       Number of experimental data points (observations)
// Nw   (1 x 1)       Total number of weights = (Ni+1)*Nh + (Nh+1)*No
//
// Wh   (Ni+1 x Nh)   Weight matrix for the hidden layer (tanh transfer function)
// Wo   (Nh+1 x No)   Weight matrix for the output layer (identity transfer function)
// Inp  (Nd x Ni)     Input data matrix. One row per observation.
// Out  (Nd x No)     Output data matrix. One row per observation.
//
// StdRes (1 x No)    Estimated standard deviation of the residual ''noise''
// CovW (Nw x Nw)     Estimated weight covariance matrix
// Opt  (struct)      Options for the training process.
//                    Missing values are replaced by defaults.
//                    Additional information is returned
//                    at the end of the training process.
//
// Available display/history options:
// ----------------------------------
// Opt.MaxTime        Maximum training time in seconds.
//        Default = 100
// Opt.MaxIter        Maximum number of iterations of the training algorithm.
//        Default = 1000
// Opt.DisplayFreq    Display frequency during learning
//        Default = 10
//        0 = last values only
//        n = every n iterations 
//
// Available accuracy/algorithm options:
//--------------------------------------
// Opt.PrecResid      Required precision for the residuals.
//        Scalar or vector of length No.
//        Default = 1e-6*std(Out,1,1)
// Opt.PrecParam      Required precision for the effective number of parameters.
//        Scalar or vector of length Nc.
//        Default = 1e-4
// Opt.StdResMin      Minimum standard deviation of the residuals.
//        Scalar or vector of length No.
//        Default = 1e-6*std(Out,1,1)
//        Controls the amount of output variability that can be attributed
//        to measurement noise, versus the variability that has to be
//        explained by the input variables.
// Opt.StdResMax      Maximum standard deviation of the residuals.
//        Scalar or vector of length No.
//        Default = 0.1*std(Out,1,1)
//        Controls the amount of output variability that can be attributed
//        to measurement noise, versus the variability that has to be
//        explained by the input variables.
// Opt.RegClass       Classes for weight regularization.
//        Default = 3
//        0 = no regularization
//        1 = one class for all weights
//        2 = two classes: one for inputs, one for outputs
//        3 = one class for each input, one for the hidden layer biases
//                    and one for each output
//        4 = one class for each weight
//        vector of length Ni+1+No or Nw = custom classes. Each element 
//                    of the vector represents a class number.
// Opt.PreProc        Data pre-processing before learning.  
//        Default = 2
//        0 = no pre-processing
//        1 = normalisation, to make the sum of squares for each input
//                    and each output equal to Nd.
//        2 = standardisation, to make the mean for each input and each output
//                    equal to zero and the sum of squares equal to Nd.
//        After learning, the weights are restored to original units.
// Opt.MomentParam    Momentum for adjusting the effective number
//        of parameters in each weight class. Scalar between 0 and 1.
//        Default = 0.8
//
// Additional information returned in Opt:
// ----------------------------------------
// Opt.Stop          Condition that provoked termination (string).
// Opt.R2            Determination coefficient for each output. 
//                   Fraction of the output variance explained by the model.
//                   1 - sum((Out-SimOut).^2) / sum((Out-mean(Out)).^2)
// Opt.WhEff         Ratio (effective / total) number of parameters
//        for each weight in the hidden layer.
//        The weights in the same class have the same ratio.
//        Has the same size as Wh, that is (Ni+1 x Nh).
// Opt.WoEff         Ratio (effective / total) number of parameters
//        for each weight in the optput layer.
//        The weights in the same class have the same ratio.
//        Has the same size as Wo, that is (Nh+1 x No).
// Opt.HistIters     History of iteration numbers.
//        Depends on Opt.DisplayFreq.
// Opt.HistResid     History of the residual network error for each output.
//        One column per output, and as many rows as Opt.HistIters.
// Opt.HistParam     History of the effective number of parameters for each class.
//        One column per class, and as many rows as Opt.HistIters.
// Opt.TotalParam    Total number of parameters (weights) in each weight class.
//        One column per class, and 1 row.
// Opt.ClassNumbers  Class number of each weight, sorted as [Wh(:); Wo(:)].
// 

// (c) 1999 Ioan Cristian TRELEA



   // Out fields ordered
   res.wh=[];
   res.wo=[];
   res.stdres=[];
   res.covw=[];
   res.options=[];


   // default option:
   if  argn(2)==4 then
       Opt=[];
   end


   // div:
   Inp1=div(Inp0);
   Out1=div(Out0);
   Wh1=div(Wh0);
   Wo1=div(Wo0);
   
   Inp=Inp1.d;
   Out=Out1.d;
   Wh=Wh1.d;
   Wo=Wo1.d;


   // ---------- Check input arguments ----------

   // Wh
   if isempty(Wh) | type(Wh)~= 1 then
      error(' ''Wh'' must be an array of real numbers'); 
   end
   [N,Nh]=size(Wh);
   Ni=N-1;
   if Nh==0 then
      error('There are no hidden neurons in ''Wh'' ');
   end
   if Ni==0 then
      error('There are no inputs in ''Wh'' '); 
   end

   // Wo
   if isempty(Wo) | type(Wo)~= 1 then
      error(' ''Wo'' must be an array of real numbers'); 
   end
   [N,No]=size(Wo);
   if N~=Nh+1 then
      error(sprintf(' ''Wh'' indicates  %d hidden neurons, but ''Wo'' has %d rows istead of %d',Nh,N,Nh+1)); 
   end
   if No==0 then
      error('There are no outputs in ''Wo'' '); 
   end

   // Inp
   if isempty(Inp) | type(Inp)~= 1 then
      error(' ''Inp'' must be an array of real numbers'); 
   end
   [Nd,N]=size(Inp);
   if N~=Ni then
      error(sprintf(' ''Wh'' indicates %d inputs, but ''Inp'' has %d columns instead of %d',Ni,N,Ni)); 
   end

   // Out
   if isempty(Out) | type(Out)~= 1 then
      error(' ''Out'' must be an array of real numbers'); 
   end
   [M,N]=size(Out);
   if M~=Nd then
      error(sprintf(' ''Inp'' indicates %d data points, while ''Out'' has %d rows instead of %d',Nd,M,Nd)); 
   end
   if N~=No then
      error(sprintf(' ''Wo'' indicates %d outputs, while ''Out'' has %d columns instead of %d',No,N,No)); 
   end

   // Early initialization
   Nwh=(Ni+1)*Nh;
   Nwo=(Nh+1)*No;
   Nw=Nwh+Nwo;

   


   // Check options and insert defaults
   if argn(2)<5 then
      Opt=struct();
   end
   
   Opt=nns_ValidateOptions(Opt,stdev(Out,'r'));
   


   // Pre processing  [NewInp 1] = [OldInp 1] * Ainp
   if Opt.PreProc==0  then  // No pre processing
      // Inp
      AInp=eye(Ni+1,Ni+1);
      // Out
      AOut=eye(No+1,No+1);
   elseif Opt.PreProc==1    // Make sums of squares = Nd
      // Inp
      SsqInp=sum(Inp.^2,1);
      if any(SsqInp==0) then
         error('Zero column not allowed in ''Inp'' '); 
      end
      AInp=diag([sqrt(Nd./SsqInp) 1]);  // 
      // Out
      SsqOut=sum(Out.^2,1);
      if any(SsqOut==0) then
         error('Zero column not allowed in ''Out'' '); 
      end
      AOut=diag([sqrt(Nd./SsqOut) 1]); 
   else // Opt.PreProc==2    // Make means = 0 and sums of squares = Nd
      // Inp
      MeaInp=mean(Inp,1);
      SsqInp=sum((Inp-repmat(MeaInp,Nd,1)).^2,1);
      if min(SsqInp)==0 then
         error('Constant column not allowed in ''Inp'' '); 
      end
      AInp=diag([sqrt(Nd./SsqInp) 1]);
      AInp($,1:Ni)=-MeaInp.*sqrt(Nd./SsqInp);   //JCB: remplace end par $ 
      // Out
      MeaOut=mean(Out,1);
      SsqOut=sum((Out-repmat(MeaOut,Nd,1)).^2,1);
      if min(SsqOut)==0 then
         error('Constant column not allowed in ''Out'' '); 
      end
      AOut=diag([sqrt(Nd./SsqOut) 1]);  // Processed out=Out*AOut + bOut
      AOut($,1:No)=-MeaOut.*sqrt(Nd./SsqOut);
   end


   Opt.Nw=Nw;
 
   // JCB (pour figures)
   Opt.StdOut=stdev(Out,'r', mean(Out,'r'));


   // Process all input variables
   aWh=inv(AInp);                         // NewWh = aWh * OldWh
   AWh=kron(eye(Nh,Nh),aWh);              // NewWh(:) = AWh * OldWh(:), for CovW
   Wh=aWh*Wh;                             // Hidden layer weights

   aWo=AOut;                              // [NewWo [00 1]] = [OldWo [00 1]] * aWo
   AWo=kron(aWo(1:No,1:No), eye(Nh+1,Nh+1));// NewWo(:) = AWo * OldWo(:) + const, for CovW
   Wo=[Wo [zeros(Nh,1); 1]]*aWo(:,1:No);  // Output layer biases

   Inp=[Inp ones(Nd,1)]*AInp(:,1:Ni);     // Processed inputs
   Out=[Out ones(Nd,1)]*AOut(:,1:No);     // Processed outputs

   Opt.PrecResid=Opt.PrecResid(1)*AOut(1:No,1:No);  //JCB: rajouté: (1) sur les 3 lignes
   Opt.StdResMin=Opt.StdResMin(1)*AOut(1:No,1:No);
   Opt.StdResMax=Opt.StdResMax(1)*AOut(1:No,1:No);

   // ------------------ In normalized units from now on -------------------------
   StdInp=stdev(Inp,'r', mean(Inp,'r'));
   StdOut=stdev(Out,'r', mean(Out,'r'));
 

   // Set up matrices related to weight classes
   [ICW,Nc,Npar,Sc,So]=nns_SetWeightClasses(Opt.RegClass,Nw,Nwh,Nwo,Ni,Nh,No);
   Opt.Npar=Npar;

   // Check related to Nc
   if size(Opt.PrecParam(1),1)~=1 & size(Opt.PrecParam(1),1)~=Nc then  //JCB: rajoute (1) 
      error(sprintf('Opt.PrecParam(1) must be a scalar or a vector of length Nc=%d.',Nc));
   end

   

   // ---------- Initialization ----------

   I=eye(Nw,Nw);
   Lamb=1e-3;
   Alph=1 ./(Sc*nns_VarW(StdInp,StdOut,Nh)); // 1 ./Typical variance for each weight class
   if Opt.RegClass==0 then
      AlphMax=zeros(size(Alph));
      AlphMin=zeros(size(Alph));
   else
      AlphMax=1e8*Alph;
      AlphMin=1e-4*Alph;
   end
  
   
   BetaMin=(1 ./ diag(Opt.StdResMax.^2));  // JCB 15dec
   BetaMax=(1 ./ diag(Opt.StdResMin.^2));
   Gama=Npar;

   W=[Wh(:); Wo(:)];
   wh=Wh;
   wo=Wo;
// bug Scilab 6.0 sur clock
   warning('off')    
   StartTime=clock();    // bug avec Scilab 6
   Stop=[];
   warning('on')

   // Initialize history arrays
   if Opt.DisplayFreq(1)~=0 then
       NHist=ceil(Opt.MaxIter(1)/Opt.DisplayFreq(1))+1;
   else
       NHist=1;
   end

   Nw=sum(Npar);
   Nc=length(Gama);

   Iter=0;
   HistIters=zeros(NHist,1);
   HistResid=zeros(NHist,No);
   HistParam=zeros(NHist,Nc);
   IHist=0;
 


   // ---------- Main loop ----------

   while isempty(Stop)
   
      while isempty(Stop)  // Normal exit in the middle of the loop
         // Uptate Alph
         Ssw=Sc*W.^2;      // Sum-squared weights 
         i=find(Ssw==0);  // Avoid divide by zero message
         Alph(i)=AlphMax(i);
         i=find(Ssw~=0);  // Now for the others
         Alph(i)=nns_clip(Gama(i)./Ssw(i), AlphMin(i), AlphMax(i));     // 1/Variance for used classes
      
         // Update Beta
         Wh(:)=W(1:Nwh);
         Wo(:)=W(1+Nwh:Nw);
         [Sse,Gra,Hes]=c_nnsimulter(Wh,Wo,Inp,Out); // Usage 3
         Hes=matrix(Hes,[Nw Nw No]); // 14mars2017
         //Hes=hypermat([Nw Nw No],Hes);        
         Beta=nns_clip((Nd-So*Gama)'./Sse, BetaMin', BetaMax');   // 1/Variance of output noise
         // Update misfit function
         M=Alph'*Ssw + Sse*Beta';
         gM=2*Alph(ICW).*W + Gra*Beta';
         hM=2*diag(Alph(ICW));
         for io=1:No; 
            hM=hM+Hes(:,:,io)*Beta(io);
         end
         if Opt.RegClass==0 | rcond(hM)>%eps then
            break          // OK, continue
         end
         
         Stop=[Stop; "Dead lock"];
      end

      // Record history
      if Opt.DisplayFreq==0 then 
          remainder=1; 
      else 
          remainder=abs(Iter)- fix(abs(Iter)./Opt.DisplayFreq).* Opt.DisplayFreq;   
      end

      if Iter<0 | remainder==0 then  // Last iteration or display iteration
        IHist=IHist+1;
        Opt.HistIters(IHist)=abs(Iter);
        Opt.HistResid(IHist,:)=sqrt(Sse/Nd);
        Opt.HistParam(IHist,:)=Gama';
      end

      // Update weights by Levenberg-Marquardt
      while isempty(Stop)   // Everything OK in previous loop. Normal exit in the middle
         w=W-(hM+Lamb*eye(Nw,Nw))\gM;
         ssw=Sc*w.^2;      // Sum-squared weights 
         wh(:)=w(1:Nwh); 
         wo(:)=w(1+Nwh:Nw); 
         out=c_nnsimul(wh,wo,Inp); // Usage 1, the fastest
         sse=sum((out-Out).^2,1);
         m=Alph'*ssw + sse*Beta';
         if m<M then
            W=w;
            Lamb=0.5*Lamb;
            break          // OK, a better point found, continue
         end
         if m==M
            Stop=[Stop; "No progress"];
            break
         end
         Lamb=10*Lamb;     // The new point is worse. Try another Lamb.
      end

      // Update Gama
      if isempty(Stop)  // Everything was OK
         if Opt.RegClass==0 then
            gam=Npar;
         else
            gam=Npar - Sc*(2*Alph(ICW).*diag(pinv(hM)));    // JCB: hM was found onece not invertible 
         end
         gama=(1-Opt.MomentParam(1))*gam + Opt.MomentParam(1)*Gama; // Update with momentum   //JCB  
         
         diff_g_G=abs(gama-Gama);
         diff_s_S=abs(sse-Sse);
         // WARNING: signs < of the following original line CHANGED  to > !!!   JCB       
         if diff_g_G(diff_g_G>=Opt.PrecParam(1))==[] & diff_s_S(diff_s_S>=Opt.PrecResid(1))==[] then
            Stop=[Stop; "Residuals and Effective parameters converged"];
         end
         Gama=gama;
      end
// bug Scilab 6.0 sur clock:
      warning('off')
      if etime(clock(),StartTime)>Opt.MaxTime(1) then
         Stop=[Stop; "Time out"];
      end
      warning('on')
      if Iter>=Opt.MaxIter(1)-1 then
         Stop=[Stop; "Maximum number of iterations exceeded"];
      end

      Iter=Iter+1;

   end
   // ---------- End of main loop ----------



   // --------------- Transform back to physical units --------------
   bWh=inv(aWh);
   Wh=bWh*Wh;                             // Hidden layer weights
   bWo=inv(aWo);
   Wo=[Wo [zeros(Nh,1); 1]]*bWo(:,1:No);  // Output layer biases
   BOut=inv(AOut); 
   StdRes=1 ./sqrt(Beta)*BOut(1:No,1:No);
   BW=[inv(AWh)  zeros(Nwh,Nwo);  zeros(Nwo,Nwh) inv(AWo)];

   CovW=BW*pinv(hM)*BW';       // JCB 6avril 2017 pinv remplace inv 

   Opt.PrecResid=Opt.PrecResid*BOut(1:No,1:No);
   Opt.StdResMin=Opt.StdResMin*BOut(1:No,1:No);
   Opt.StdResMax=Opt.StdResMax*BOut(1:No,1:No);
   Opt.HistResid=Opt.HistResid*BOut(1:No,1:No);  // JCB
   Opt.Stop=Stop;
   Opt.R2=1-(Sse/Nd)./StdOut.^2;   
   WEff=Opt.HistParam($,ICW)'./Npar(ICW); // Ratio (Effective/Total) number of parameters
   Opt.WhEff=matrix(WEff(1:Nwh),size(Wh));    // reshape remplaced by matrix
   Opt.WoEff=matrix(WEff(1+Nwh:Nw),size(Wo));
   Opt.ClassNumbers=ICW;
   Opt.TotalParam=Npar';
  
 
   // Outputs:
   res.wh.d=Wh;
   if typeof(Wh0)=='div' then
       res.wh.i=Wh0.i;
       res.wh.v=Wh0.v;
   else
       res.wh.i=['hn' + string([1:1:Ni]');'hbiais'];
       res.wh.v='hidden neurons';
   end
   
   res.wo.d=Wo;
    if typeof(Wo0)=='div' then
       res.wo.i=Wo0.i;
       res.wo.v=Wo0.v;
   else
       res.wo.i=['on' + string([1:1:Nh]');'obiais'];
       res.wo.v='hidden layer';
   end  
   
   
   
   res.stdres=StdRes;
   res.covw=CovW;
   res.options=Opt;
 
   


endfunction

// ============================== End nnn_train_bayes ==========================








function Opt=nns_ValidateOptions(Opt,StdOut) //-------------------------------------
   // Validate existing options and insert defaults


   if ~isempty(Opt) & typeof(Opt) ~= 'st' then
      error('Argument ''Opt'' must be a structure or empty []');
   end
   No=length(StdOut);

   // Default display/history options & their properties
       if ~isfield(Opt,'MaxTime')      then   Opt.MaxTime= 300;           end;
       if ~isfield(Opt,'MaxIter')      then   Opt.MaxIter= 10000;         end;
       if ~isfield(Opt,'DisplayFreq')  then   Opt.DisplayFreq=10;         end;
       if ~isfield(Opt,'DisplayMode')  then   Opt.DisplayMode=[];         end;

   // Default accuracy/algorithm options & their properties
       if ~isfield(Opt,'PrecResid')    then   Opt.PrecResid= 1e-6*StdOut; end;
       if ~isfield(Opt,'PrecParam')    then   Opt.PrecParam= 1e-4;        end;
       if ~isfield(Opt,'StdResMin')    then   Opt.StdResMin= 1e-6*StdOut; end;
       if ~isfield(Opt,'StdResMax')    then   Opt.StdResMax= 0.1*StdOut;  end;
       if ~isfield(Opt,'RegClass')     then   Opt.RegClass= 3;            end;
       if ~isfield(Opt,'PreProc')      then   Opt.PreProc= 2;             end;
       if ~isfield(Opt,'MomentParam')  then   Opt.MomentParam= 0.8;       end;
      
       if ~isfield(Opt,'HistIters')    then   Opt.HistIters=[];           end;
       if ~isfield(Opt,'HistResid')    then   Opt.HistResid=[];           end;
       if ~isfield(Opt,'HistParam')    then   Opt.HistParam= [];          end;    

  // Transform scalars applicable to all outputs to row vectors
  if max(size(Opt.PrecResid))==1 then
       Opt.PrecResid=Opt.PrecResid(ones(1,No)); 
  end
  if length(Opt.StdResMin)==1 then
       Opt.StdResMin=Opt.StdResMin(ones(1,No)); 
  end
  if length(Opt.StdResMax)==1 then
       Opt.StdResMax=Opt.StdResMax(ones(1,No)); 
  end

endfunction
// ============================ End nns_ValidateOptions ============================







function [ICW,Nc,Npar,Sc,So]=nns_SetWeightClasses(RegClass,Nw,Nwh,Nwo,Ni,Nh,No) //--
   // Set up weight repartition into regularization classes
   // RegClass  Integer code for the number of classes
   //           or vector of class numbers
   // Nw        Total number of weights
   // Nwh       Number of weights in the hidden layer
   // Nwo       Number of weights in the output layer
   // Ni        Number of inputs
   // Nh        Number of hidden neurons
   // No        Number of outputs
   //
   // ICW (Nw x 1)  Class indexes for each weight
   // Nc           Total number of classes
   // Npar(Nc x 1) Number of weights in each class
   // Sc (Nc x Nw) Matrix for summing weight contribution for each class
   //              1(i,j) when weight j belongs to class i, 0 otherwise
   // So (No x Nc) Matrix for summing weight contribution for each output
   //              (i,j)=fraction of weights in class j contributing to output i

   // RegClass: indices of classes in vector ICW

   select length(RegClass)
      case 1   // Scalar
         select RegClass
            case 0   // No regularization :All weights in a single class
               ICW=ones(Nw,1);
            case 1   // All weights in a single class
               ICW=ones(Nw,1);
            case 2   // One class for Wh, another for Wo
               ICW=[ones(Nwh,1); 2*ones(Nwo,1)];
            case 3   // One for each input, one for each output
               ICW=repmat(Ni+2:Ni+1+No, Nh+1,1); // For Wo
               ICW=[repmat((1:Ni+1)', Nh,1);  ICW(:)];
            case 4   // One for each weight
               ICW=(1:Nw)';
         end
      case Ni+1+No   // Classes assigned per groups of inputs and outputs
         ICW=repmat(RegClass(Ni+2:Ni+1+No)',Nh+1,1); // For Wo
         ICW=[repmat(RegClass(1:Ni+1),Nh,1);  ICW(:)];
      case Nw   // Classes assigned for each weight individually
         ICW=RegClass
      else
         error(' ''Opt.RegClass'' must be 1,2,3,4, or a vector of length Ni+1+No or Nw');
   end
   if ICW<1 | ICW>Nw then
      error(sprintf(' ''Opt.RegClass'' must contain values between 1 and the total number of weights (%d)',Nw)); 
   end

   // Set up Npar
   Nc=max(ICW); // Number of classes
   Npar=zeros(Nc,1); // Number of weights in each class
   for ic=1:Nc;
      Npar(ic)=sum(ICW==ic);
   end
   imiss=find(Npar==0);
   if ~isempty(imiss) then
      error(sprintf('Class %d has no weights. ',imiss)); 
   end

   // Matrix for summing weight contributions per class
   Sc=zeros(Nc,Nw);
   for ic=1:Nc; // For all classes
      Sc(ic,find(ICW==ic))=1;  // Mark weights belonging to that class
   end
   // Matrix for summing class contributions per output
   So=zeros(No,Nc);
   for io=1:No // For all outputs
      for ic=1:Nc // For all classes
         // Find weights in class ic that contribute to output io
         i1=Nwh+1+(io-1)*(Nh+1); // Index, in ICW, of the first direct weight to output io
         i2=Nwh+io*(Nh+1); // Index, in ICW, of the last direct weight to output io
         So(io,ic)=sum(ICW([1:Nwh i1:i2])==ic)/Npar(ic); // Fraction of weights from class ic contributing to output io
      end
   end

endfunction

// ============================ End nns_SetWeightClasses ==========================







function y=nns_clip(x,lbound,hbound) // --------------------------------------------
   // Make sure is x between lbound and hbound
   y=min(hbound,max(lbound,x));

endfunction

// ============================ End nns_clip =======================================







function V=nns_VarW(StdInp,StdOut,Nh) // -------------------------------------------
// Estimated variance for each weight

   Ni=length(StdInp);
   No=length(StdOut);

   Vh=ones(Ni+1,Nh); // Variance of Wh
   for ii=1:Ni
      Vh(ii,:)=1 ./StdInp(ii).^2;
   end

   Vo=ones(Nh+1,No); // Variance of Wo
   for io=1:No
      Vo(:,io)=StdOut(io).^2;
   end

   V=[Vh(:); Vo(:)];

endfunction

// ============================ End nns_VarW ===============================
