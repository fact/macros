function  xoutput=glx_list2struct(xinput)

   // une liste est transformée en structure, avec les champs: 'typeof', 'list1','list2',...
   
   if  typeof(xinput)=='list' then
      nlist=length(xinput);

      for i=1:nlist;
          if typeof(xinput(i))=='div' then
              execstr(msprintf('xoutput.list%s=glx_div2struct(xinput(i));',string(i)));
          elseif typeof(xinput(i))=='constant' | typeof(xinput(i))=='hypermat' then
              execstr(msprintf('xoutput.list%s.typeof=typeof(xinput(i));', string(i)));
              execstr(msprintf('xoutput.list%s.list1=xinput(i);',string(i)));
          elseif typeof(xinput(i))=='string' then
              execstr(msprintf('xoutput.list%s.typeof=typeof(xinput(i));', string(i)));
              xinputi=justify(xinput(i),'l');
              execstr(msprintf('xoutput.list%s=xinputi;',string(i)));
          elseif typeof(xinput(i))=='st' then
              execstr(msprintf('xoutput.list%s=div2mat_div2struct(xinput(i));',string(i)));
          else
              pause
              error('div, st, constant, hypermat or string expected');
          end
      end      
      
   end 
   
   xoutput.typeof='list';
      
   
endfunction
