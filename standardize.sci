function [saisir, xstd] = standardize(x,xmode)
    
  //standardizes		- divides each column by the corresponding standard deviation
  //function [X, xstd] = standardize(X1,(option))
  //
  //Input arguments
  //==============
  //X1:SAISIR matrix (n x p)
  //option : either 0: divides by n-1, or 1: divides by n (default : 1)
  //
  //Output argument
  //===============
  //X:SAISIR matrix (n x p)
  //xstd: standard deviation of the columns of X1 (1 x p)
  //OBSOLETE    Use preferably standardize

  xmode=0;  
  [nargout,nargin] = argn(0) 
  saisir1=div(x);
  //This function divides each column by the corresponding standard deviation
  [n,p]=size(saisir1.d);
  if(nargin==2) then
    xmode=1;
  end
  xstd.d=stdev(saisir1.d,1);
  if(xmode==1)
    xstd.d=xstd.d*sqrt((n-1)/(n))
  end
  aux=ones(1,p)./xstd.d;
  //size(aux);
  saisir.d=saisir1.d.*(ones(size(saisir1.d,1),1)*aux);
  saisir.v=saisir1.v;
  saisir.i=saisir1.i;
  xstd.i='Standard deviation';
  xstd.v=saisir1.v;

  // sorties mises au format div:   31-08-2015
  saisir=div(saisir);
  xstd=div(xstd);

endfunction

