function [res_appli]=pcaapply(model,xval,dim);

    // function [res_appli]=pcaapply(model,xval,(dim));
    // applique un modèle d'ACP sur un jeu de données xval

    // model: modèle obtenu avec la fonction pcana ou cspcana
    // xval: données sur lesquelles appliquer le modèle
    // res_appli= scores de xval selon les vecteurs-propres de model

    // vérification des arguments en entrée, du nombre (facultatif)de vecteurs-propres à utiliser
    if argn(2)<2 | argn(2)>3 then
        error('wrong number of input arguments in pcaaply, 2 or 3 expected');
    elseif argn(2)==2 then
        dim=size(model.eigenvec.d,2);
    end;

    // initialisation
    xv=div(xval);
    n=size(xv.d,1);

    // vérification que les dimensions de model et de xval correspondent
    if size(model.eigenvec.d,1)~=size(xv.d,2) then
        error('the model and the data do nat have the same dimensions')
    end

    // options centrage-réduction
    xv.d=xv.d - model.centred*ones(n,1)*model.x_mean.d';  // centrage
    if model.std~=0 then
        xv.d=xv.d ./ (ones(n,1)*model.x_stdev.d');       // réduction
    end
    // application du modèle:
    xv.d=xv.d*model.eigenvec.d(:,1:dim);

    // sorties
    xv.v = 'PC' + string([1:1:dim]');
    res_appli=xv;


endfunction
