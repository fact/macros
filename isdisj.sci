function testdisj=isdisj(xi)

  // Verifies if the input data is disjuctive 

  testdisj='T';

  x2=div(xi)
  x=x2.d;  
 
  [n,p]=size(x);
  mc=sum(x,'c');
  
    for i=1:n;
      for j=1:p;
          if x(i,j)~=0 & x(i,j)~=1 then 
          //disp('A disjuctive data must contain only booleans (0 or 1)'); 
          testdisj='F';
          end
      end
    end
    if  max(mc)-1> 10*%eps then 
      //error('a same observation has been attributed to more than one class');
    testdisj='F';
    end

 
 
endfunction
