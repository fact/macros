function [obtained_groups,model,effectif] = str2conj(si,startpos,endpos)

  // create_group1    - uses the identifiers to create groups
  // use the identifier for creating groups.
  // creates as many group means as different strings from startpos to endpos
  // function saisir=create_group1(s,startpos,endpos)
  // s: saisir file, startpos and enpos : position of discriminating characters


  [nargout,nargin] = argn(0)

  si=string(si); 
  if size(si,1)==1 then
      si=si';
  elseif size(si,1)~=1 & size(si,2)~=1 then
      error('the identifier is expected to be a vector, not a matrix')
  end
  [nrow,ncol] = size(si);

  if nrow==1 then
      si=si';
  end
  
  if nargin==3 then
    plage = [startpos:endpos]';
  elseif nargin==2 then 
    plage = [startpos:startpos];
  elseif nargin==1 then
    plage=[1:max(length(si))];
  else 
    error ('too many input arguments');  
  end;
  
  model(1) = part(si(1),plage);
  nmodel = 1;
  effectif(1) = 0;
  nom_group=plage(1);
  obtained_groups = zeros(nrow,1);
  for i = 1:nrow
    if pmodulo(i,10000)==0 then
      disp(i,'i=');
    end;
    trouve = 0;
    for j = 1:nmodel
      if ~strcmp(model(j),part(si(i),plage)) then
        indice = j;
        trouve = 1;
        effectif(indice,1) = effectif(indice)+1;
        obtained_groups(i,1)=indice; //[obtained_groups;part(si(i),plage)];
      end;
    end;
    if trouve==0 then
      nmodel = nmodel+1;
      indice = nmodel;
      model(nmodel) = part(si(i),plage);
      effectif(nmodel,1) = 1;
      obtained_groups(i,1) = indice;
    end;
    
  end;                                                   

endfunction
