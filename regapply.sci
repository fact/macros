function [res_appli]=regapply(model,xval,yval);
    
    // Attention: varagin enlevé le 24/04/13
    
    // function [ypred,rmsep]=reg_apply(model,xval,(yval));
    // 
    // This function applies any calibration model obtained with the function cvreg
    // to a new dataset xval (matrix or Saisir structure). 
    // If yval is provided, the function also computes the error of prediction (RMSEP)
    // and the square correlation coefficient
    //
    // Input:
    // ======
    // model:             a structure obtained after applying the function cvreg
    // 
    // xval:              Saisir structure or matrix n X p, 
    //                             the data to be used for prediction
    // yval (optional):   Saisir structure or vector n x p, the reference values of xval
    //
    // Output:
    // =======
    // ypred:             predicted values
    // rmsep (optional):  root mean square error of prediction
    // r2 (optional):     square correlation coefficient
    // 
    // Miscellaneous:
    // ==============
    // JC Boulet, D.Bertrand (INRA), France
    // Last update: 2012, April, 23
    // ----------------------------
 
    
    xv=div(xval);
    
    lv=size(model.b.d,2);
    
    ypred.d=[];
    ypred.i=[];
    ypred.v=[];

    r2.d=[];
    r2.i=model.b.v;
    r2.v='r2';
    
    res_appli=[];
    
    xval2=xv.d;
    n=size(xval2,1);
    
    x=xval2-model.center*ones(n,1)*model.x_mean.d';
    
    size_model=size(model.b.d);
    
    if max(size(size_model))==2 then        // modele=matrice 2D  
        flag_dim='2D'; 
        ypred.d=x*model.b.d + model.center*ones(n,lv)*model.y_mean.d;
        ypred.i=xv.i;
        ypred.v=model.b.v;
    elseif max(size(size_model))==3 then    // model=matrice 3D
        flag_dim='3D';
        ypred.d=zeros(n,size_model(2),size_model(3));
        for i=1:size_model(3);
            model_bi=model.b.d(:,:,i);
            ypred.d(:,:,i)=x*model_bi + model.center*ones(n,size_model(2))*model.y_mean.d(i);
            
        end
    end

    // gestion de yval
           
    flag=0;       
    if argn(2)>=3 then                                              // if yval is known
        if flag_dim=='2D' then 
            
             rmsep.d=[];
             rmsep.i=model.b.v;
             rmsep.v='rmsep';

             yv=div(yval);
  
             res_appli.ypred=ypred;
      
             res_appli.ypred=div(res_appli.ypred);
           
             yval2=yv.d;  
             if size(yval2,1)~= n then 
                       error ('not the same number of samples in xv and yv');
             elseif size(yval2,1)<= 10 then 
                       warning('RMSEP and r2 calculated with less than 10 values')
             end
             if max(size(yval2))== 1 then
                flag=1;         
                warning('only 1 value => no RMSEP or r2')
             end 

             if flag==1 then
                rmsep.d=[];
                r2.d=[];

             else 
                difference=ypred.d-yval2*ones(1,lv);
                rmsep.d=sqrt(diag(difference'*difference)/n);
                res_appli.rmsep=rmsep;
                res_appli.rmsep=div(res_appli.rmsep);
                res_appli.rmsep(5)=['number of regression dimensions';'RMSEP'];
           
                ypredc=ypred.d-ones(n,1)*mean(ypred.d,'r');             // centering
                yval2c=(yval2-mean(yval2,1)*ones(n,1))*ones(1,lv);
 
                r2num=(diag(ypredc'*yval2c)).^2;                         // r2 calculation
                r2denom=diag(ypredc'*ypredc).*diag(yval2c'*yval2c);
                r2.d=r2num./r2denom;
                res_appli.r2=r2;
                res_appli.r2=div(res_appli.r2);
                res_appli.r2(5)=['number of regression dimensions';'squared correlation coefficient R2'];
               
             end
           
             if argn(2)==4 then    // corrigé le 21/06/16 
                lvsel=find(model.err.d(:,2)==min(model.err.d(:,2)));
                titre1= sprintf("LV for min(RMSECV)= %2.0f",lvsel);
                regplot(yv.d,ypred.d(:,lvsel),'0','t','', titre1);
             end

        elseif flag_dim=='3D' then  // ------3D -------------------------------

             yv=div(yval);
             res_appli.ypred=ypred;
             res_appli.ypred=div(res_appli.ypred);
           
             yval2=yv.d;  
             if size(yval2,1)~= n then 
                       error ('not the same number of samples in xv and yv');
             elseif size(yval2,1)<= 10 then 
                       warning('RMSEP and r2 calculated with less than 10 values')
             end
             if max(size(yval2))== 1 then
                flag=1;         
                warning('only 1 value => no RMSEP or r2')
             end 

             if flag==1 then
                rmsep.d=[];
                r2.d=[];

             else 
                rmsep.d=zeros(size_model(2),size_model(3));
                rmsep.i=model.b.v.v1;
                rmsep.v=model.b.v.v2
                for i=1:size_model(3);      // nbre de colonnes dans y
                    difference_i=ypred.d(:,:,i)-yv.d(:,i)*ones(1,size_model(2));
                    rmsep_i=sqrt(diag(difference_i'*difference_i)/n);
                    rmsep.d(:,i)=rmsep_i;
                end
                rmsep=div(rmsep);
                
                res_appli.rmsep=rmsep;
                res_appli.rmsep=div(res_appli.rmsep);
                res_appli.rmsep(5)=['number of regression dimensions';'RMSEP'];
           //pause
                // calcul du R2
                //ypredc=zeros(n,size_model(2),size_model(3));
                //yval2c=zeros(n,size_model(2),size_model(3));
                yval2c_i=yval2(:,i)-ones(n,1)*mean(yval2(:,i),'r');
                yval2c_i=yval2c_i*ones(1,size_model(2));

                res_appli.r2.d=zeros(size_model(2),size_model(3));
                
                for i=1:size_model(3);    
                    ypredc_i=ypred.d(:,:,i)-ones(n,1)*mean(ypred.d(:,:,i),'r');             // centering
                    r2num_i=(diag(ypredc_i'*yval2c_i)).^2;                         // r2 calculation
                    r2denom_i=diag(ypredc_i'*ypredc_i).*diag(yval2c_i'*yval2c_i);
                    r2=r2num_i./r2denom_i;
                    res_appli.r2.d(:,i)=r2;
                end
               //pause 
                res_appli.r2.i=model.b.v.v1;
                res_appli.r2.v=model.b.v.v2;
                res_appli.r2=div(res_appli.r2);
                
             end
           
             //if argn(2)==4 then    // corrigé le 21/06/16 
             //   lvsel=find(model.err.d(:,2)==min(model.err.d(:,2)));
             //   titre1= sprintf("LV for min(RMSECV)= %2.0f",lvsel);
             //   regplot(yv.d,ypred.d(:,lvsel),'0','t','', titre1);
             //end
             


        end  // ---------------------------------------------------------------


    elseif argn(2)==2 then
    
           ypred=div(ypred);
           res_appli.ypred=[];
           res_appli.ypred=ypred;
           res_appli.ypred=div(res_appli.ypred);
           
    elseif argn(2)>4 then 
           error('too many input arguments');

    elseif argn(2)<2 then 
           error('not enough input arguments')

    end;
    
     
endfunction
