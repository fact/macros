function  x_div=lmata2div(file_lmata)

        // importe un fichier au format lmata 
        
        // file_lmata: un fichier texte au format lmata
        // 1° ligne = nbre de lignes 
        // 2° ligne = labels des lignes 
        // 3° ligne = nbre de colonnes
        // 4° ligne = labels des colonnes 
        // lignes suivantes: les données séparées par un espace 
        
        // x_div: un div 
        
        file_id=mopen(file_lmata); // ouvre le fichier 
        
        line1=mgetl(file_id,1);  // lit la ligne 1 
        line2=mgetl(file_id,1);  // lit la ligne 1+1
        line3=mgetl(file_id,1);  // lit la ligne 1+1+1
        line4=mgetl(file_id,1);  // lit la ligne 1+1+1+1
        data=mgetl(file_id);     // lit les lignes 5 et suivantes
        
        mclose(file_id)          // ferme le fichier 
        
        // extraction des données
        line1=stripblanks(line1,%f,1);              //  n'enlève que les blancs à la fin
        nbr_lines=strtod(line1);
        line2=stripblanks(line2,%f,1);
        labels_lines=strsplit(line2,' ');
        line3=stripblanks(line3,%f,1);
        nbr_cols=strtod(line3);
        line4=stripblanks(line4,%f,1);
        labels_cols=strsplit(line4,' ');

        data2=zeros(nbr_lines,nbr_cols);
        for i=1:nbr_lines; 
            data_line=stripblanks(data(i),%f,1);   // n'enlève que les blancs à la fin
            data_line=strsplit(data_line,' ');
            data_line=strtod(data_line');
            data2(i,:)=data_line;
        end
        
        // construction du div:
        x_div.d=data2;
        x_div.i=labels_lines;
        x_div.v=labels_cols;
        x_div=div(x_div);
 
    
    
endfunction
