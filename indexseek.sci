function index=indexseek(str,value);

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  if type(str)==10 then
     aux=strtod(str);
  elseif type(str)==1 then
     aux=str   
  else error('wrong type of input argument in find_index')  
  end
  
  if(isnan(sum(aux))) then
     disp('First argument impossible to convert into numerical value ');
  end
  
  [min_value index]=min(abs(aux-value));


endfunction

