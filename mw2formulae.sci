function [res_form]=mw2formulae(x0,threshold,limit,base_calc)
    
    // propose des formules brutes CHO à partir d'une masse 
    // x:           masse brute observée (corrigée du H+)
    // threshold:   écart-maximum de x à la valeur de référence
 
    // ex de test: [a,b]=mw2formulae(56,1.1);
    
    // la fonction compare x à une valeur tirée d'une base, qui contient les masses théoriques calculées dans une 
    // certaine fourchette de C, H et O.
  
    // pour calculer la base: mettre flag=1
   
    if argn(2)<=2 then
        limit=100;
    end
   
    if argn(2)<=3 then
        flag=0;
    else
        flag=base_calc;
    end
    

    // augmentation de la mémoire si 5.5.2
    v=getversion();
    v=part(v,8);
    v=strtod(v);
    if v<6 then 
        stacksize('max');
    end


    
    // construction d'une base: max. 200 C, 10000 H, 300 O~ 1 N [compter quelques minutes]
    // valeur exacte pour C12
    //    "     "     "   H1  (www.chemcalc.org)
    //    "     "     "   O16 (www.chemcalc.org)
    //    "     "     "   N14 (www.chemcalc.org)
    
    if flag~=0 then
        n_c=200;
        n_h=1000;
        n_o=300;
        n_n=1;
        m_c=12;                 
        m_h=1.0078250321;       
        m_o=15.9949146196;        
        m_n=14.0030740049;      
        base=zeros(n_c,n_h,n_o,n_n+1);
        for i=1:n_c;
            for j=1:n_h;
                for k=1:n_o;
                    for l=1:(n_n)+1;
                        base(i,j,k,l)=i*m_c + j*m_h + k*m_o+ (l-1)*m_n;
                    end
                end
            end
        end
        save('x_mw2formulae_base.dat','base')
    else
        load('x_mw2formulae_base.dat')
    end 
    
    // obtention des solutions 
    
    n_masses=max(size(x0));
    
    for k=1:n_masses;
        
        x=x0(k);
        
        [a,b,c,d]=find(abs(base -x)<threshold);
//pause
        if c~= 0 then
            res_sol=[a' b' c' (d'-1)];    // car premier tableau = zéro N 
        else
            error('No solution was found. Try to increase the threshold.')
        end
    
        // calcul des différences entre x et la base
        n=size(res_sol,1);
        diffs=zeros(n,1);
        for i=1:n;
            base_i=base(res_sol(i,1),res_sol(i,2),res_sol(i,3),res_sol(i,4)+1);
            diffs(i)=x-base_i;
        end
    
        // tri selon les valeurs décroissantes
        diffs2=abs(diffs);
        [values,indices]=gsort(diffs2,'r','i');
    
        // mise en forme des données
  
        res_d=[res_sol(indices,:) diffs(indices,:) [1:1:n]'];
        res_d=res_d(1:min([n limit]),:);
        
        res_i=string(x*ones(n,1));
        res_i=res_i(1:min([n limit]),:);

        if k==1 then
           res_form.d= res_d; 
           res_form.i= res_i; 
        else
           res_form.d=[res_form.d; res_d] ;   
           res_form.i=[res_form.i;res_i];
        end 
        res_form.v=['mass';'C';'H';'O';'N';'rank'];
    
    end
    
    
    
    res_form=div(res_form);    
    
endfunction
