function [classif] = membership(x,y,metric,class) 

  // (JCB) pour chaque observation, calcule la probabilité d'appartenir à chacune des classes
  // y est une matrice DISJONCTIVE!
  // si x est de dim.(n,p) et y de dim.(n,nclass) alots classif est de dim. (n x ndim)
  // et somme de chaque ligne de classif=1 
  
  // x = scores! 

  // cas général: class=0 (pas encore utilisé avec class1)


  [n,p] = size(x);
  [m,c] = size(y);

  if n~=m then error('no concordance');
  end;

  d = zeros(n,c);

  for i = 1:c
    ind = find(y(:,i));  // extrait les indices de la colonne i
    ind=ind';
    z = x - ones(n,1)* mean(x(ind,:),'r');
    
    if max(size(ind))>1 then
           select metric
           case 0 then
                zz = x(ind,:)- ones(size(ind,1),1)* mean(x(ind,:),'r');
                s=pinv(zz'*zz);
           case 1 then
                s = eye(p,p);
           else error('valid values for metric are 0 or 1') 
           end;
           
     else s=eye(p,p);
     end;
     d(:,i) = diag(z*s*z');
  end;
  notzero=0.0000000001;
  d(d<notzero)=0.0000000001;  //douteux comme opération...

  a=ones(n,c)./sqrt(d); 
  s = sum(a,2);
  ind = find(bool2s(s==0))';   // vecteur colonne donnant les positions des valeurs nulles de s
  a(ind,:) = ones(max(size(ind)),1) .*. ones(1,c);
  s(s==ind)=c;
  a = a ./(ones(1,c) .*. sum(a,2));


  if class==1 then
     [mn,ind] = max(a,'c');
     a2=zeros(n,c);
     for i=1:n;
         a2(i,ind(i))=1;
     end
     a=a2;
  end;

  classif=a;
  

endfunction
