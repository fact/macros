function xoutput=glx_mat2div(xinput, varname)
    
    // nom du fichier qui sera ouvert 
    if argn(2)<2 then
       varname=strsplit(xinput,'.');
       varname=varname(1);
    end

    // chargement du fichier matlab
    execstr(msprintf('loadmatfile %s ;',xinput))
    

    // exécution de la fonction d'analyse de varname     
    xoutput=glx_mat2div_struct2div(varname);
    
    
    
endfunction 
    
