function [res] = iirp(xg,xtest,ytest,split,lv,varargin)
    
  // IIR 

   xg=div(xg);
   n=size(xg.d,1);
   classes_obs=ones(n,1);
    
  if argn(2)==5 then
   centrage=1;
  elseif argn(2)==6 then
   centrage=varargin(1);
  end
   
  [dmatrix] = pop_dextract('iirp',xg);
  res=pop_dtune(dmatrix,20,xtest,ytest,split,'pls',centrage,lv)   
           
endfunction

 

