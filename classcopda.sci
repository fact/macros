function [res_classcopda] = classcopda(x,y,lv)
  
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
  
  // initialisation
    [n,q]=size(x);
    [ny,qy]=size(y);
    if ny~=n then error ('x and y do not have the same number of observations')
    end

    x_intra=x-y*inv(y'*y)*y'*x;    
    ld = optim_svd(x_intra);
    
    lv2=min(lv,q);
    ld=ld(:,1:lv2);

    res_classcopda.scores=x*ld;
    res_classcopda.rloadings=ld;

endfunction


function v = optim_svd(x)
  [n,p]=size(x);
  if n < p        
    [u,s,v]=svd( x*x' );
    v = x'*v;
  else
    [u,s,v]=svd( x'*x );
  end;
endfunction
