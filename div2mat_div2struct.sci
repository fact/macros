function   xoutput=div2mat_div2struct(xinput)
    
            // xinput est une structure qui contient des champs de nature différente, dont des div
            // div2mat_div2struct remplace les div et les listes par des structures 
            
            if typeof(xinput)~='st' then
                error('a structure expected in div2mat_div2struct')
            end
            
            fields_names = fieldnames(xinput);  // liste des champs en entrée 
            nfields=size(fields_names,1);       // nombre de champs en entrée 

            for i=1:nfields;
                execstr(msprintf('field_level1=xinput.%s;',fields_names(i)));
                if typeof(field_level1)=='div' then
                    execstr(msprintf('xoutput.%s=glx_div2struct(field_level1);',fields_names(i)));
                elseif typeof(field_level1)=='constant' | typeof(field_level1)=='hypermat' | typeof(field_level1)=='string' then  
                    execstr(msprintf('xoutput.%s=field_level1;',fields_names(i)));  
                elseif typeof(field_level1)=='string' then
                    field_level1=justify(field_level1,'l');
                    execstr(msprintf('xoutput.%s=field_level1;',fields_names(i))); 
                elseif typeof(field_level1)=='list' then
                    execstr(msprintf('xoutput.%s=glx_list2struct(field_level1);',fields_names(i)));
                else
                    error('div,constant, string or list expected in div2mat_div2struct')
                end    
             end  
    
             xoutput.typeof='st';
    
    
endfunction
