function [T] = ica_pca_tall_pct_dnr(X, PCs, partitions)

    

    W = icarowpartition(X, partitions);
    q = size(X,2);
    I = eye(q,q);

    D=W(1)'*W(1);

    for par = 2:partitions
        D = D + W(par)'*W(par);
    end         

    [Vx Sx]=svd(D);
    // Vx=flipud(Vx);  //utile de retourner Vx? pas sûr 
    // Vx contient les vecteurs singuliers (propres?) disposés en colonne 
    // et classes par ordre decroissant de valeur propre 

    T = [];
    for par = 1:partitions
        t = W(par)*Vx;   
        T = [T ; t(:,1:PCs)];
    end


endfunction
