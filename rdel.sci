function [x_saisir] = rdel(x,index)                              

 //row-delete           - delete rows                                       
 //function X1 = row-delete(X,index)                                        
 //input arguments                                                         
 //===============                                                         
 //X:Saisir matrix (n x p)                                                 
 //index:vector indicating the rows to be deleted                          
 //                                                                        
 //Output argument                                                         
 //==============                                                          
 //X1:saisir matrix (n1 x p) with n1 <=n                                   
 //                                                                        
 //The deleted rows are indicated by the vector index (numbers of booleans)
 //                                                                        
 //Typical Examples 
 //================                                                    
 //reduced=row_delete(data,[1 3 5]);//// deletes 3 rows                    
 //reduced1=row-delete(data,sum(data.d,2)==0); // deletes all the rows with 
 //the sum equal to 0                                                      
                            
                                                                          
  saisir=div(x);                                                                     
  if index==[] then
    x_saisir=saisir;
    
  else
    [ni,pi]=size(index);
    if ni==1 then
      index=index';
    elseif ni~=1 & pi~=1 then
      error('index should be a vector, not a matrix')
    end

    saisir_d=saisir.d;    // 18 avril 17 pour Scilab 6.0
    saisir_d(index,:)=[];
    x_saisir.d=saisir_d;

    saisir_i=saisir.i;
    saisir_i(index,:)=[];
    x_saisir.i=saisir_i; 
 
    x_saisir.v=saisir.v;                                                      
                                                     
  end
  
  x_saisir=div(x_saisir);
                                                      
endfunction                                                               
                       
