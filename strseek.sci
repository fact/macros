function index = strseek(x,str1)
  //seekstring		- returns a vector giving the indices of string in matrix of char x in which 'str' is present
  //function index = seekstring(identifiers,xstr)
  //
  //Input arguments
  //===============
  //identifiers: matrix of characters (n x p)
  //xstr: string (1 x k), with k smaller th=an p
  //
  //Output argument
  //==============
  //ndex: vector of integers giving the indices of the rows of "identifiers" in
  //which the string "xstr" has been found
  //
  ////Typical example:
  ////===============
  //index=seekstring(DATA.i,'thisname');
  //Gives the indices in DATA.i in which the string "thisname" is present.


  j=0;
  str1=stripblanks(str1);
  [n,p]=size(str1);
  index=[];
  for k=1:n
    str=str1(k);
      for i=1:size(x,1);
        if(~isempty((strindex(x(i),str)))) then
            j=j+1;
            index(j)=i;
        end;
      end;
  end
 


endfunction

