function res=statislda(X0,Y0,RV)
     
   // Xtab : liste comportant q matrices
   // Y    : vecteur des groupes

   // mise au format de listes / structures Div
   [test,Xtab]=islist(X0);
   
   // gestion de Ytab
   if typeof(Y0)=='div' then                        // div ou pas div
       Y1=Y0;
   else 
       Y1=div(Y0);
   end
   
   Ytab=Y1.d; 
   
   if size(Ytab,1)==1 then                          // mise en colonne
            Ytab=Ytab';                            
   end
       
   if size(Ytab,1)~=size(Xtab(1).d,1) then    // vérif. nbre d'observations
            error ('X and Y do not have the same number of observations');
   end
        
   Ytab=string(Ytab);
  
   Ytab=str2conj(Ytab,1,max(length(Ytab)));
   Y=conj2dis(Ytab);   
   nclasses=size(Y,2);
 

   
	
    // calcul des paramètres courants 
    if argn(2)==2 then
        RV='F';
    end

   // initialisation
   q=length(Xtab);   // q=nbre de tables
   n=size(Xtab(1).d,'r');
   labels_tables='table'+string([1:1:q]');
	
   pind=zeros(q);              
   for i=1:q;
       pind(i)=size(Xtab(i).d,'c');
   end
   p=sum(pind);

   tmp=[];
   tmp_labels_var=[];
   for i=1:q;
       if size(Xtab(i).d,'r')~=n then error('all data should have the same number of lines');
       else 
           tmp=[tmp Xtab(i).d];
           tmp_labels_var=[tmp_labels_var;Xtab(i).v];
       end
   end
   tmp_labels_obs=Xtab(1).i;

   moyC=mean(tmp,'r');
   Xc=tmp-ones(n,1)*moyC;    // Xc est la matrice concaténée-centrée des Xi
	

   // calcul des variances pour les deux tableaux
   UI=conj2dis(Y);
   DI=UI'*UI/n
   Xi=list();
   Gi=list();
   Bi=list();
   Vi=list();
   Viinv=list();

   for i=1:q;
      Xi(i)= Xtab(i).d- ones(n,1)*mean(Xtab(i).d,'r');
      Gi(i)=pinv(DI)*UI'*Xi(i)/n;
      Bi(i)=Gi(i)'*DI*Gi(i);
      Vi(i)=Xi(i)'*Xi(i)/n;     //Vi=var-covariance des Xi
      Viinv(i)=pinv(Vi(i));
   end
	
   //### Calcul de la matrice C
   C=zeros(q,q);
   normw=zeros(q,1);
   for k=1:q;
      C(k,k)=sum(diag(Bi(k)*Viinv(k)*Bi(k)*Viinv(k)));
      normw(k)=sqrt(C(k,k));
      for l=1:k; 
         C(k,l)=sum(diag(Gi(k)*Viinv(k)*Gi(k)'*DI*Gi(l)*Viinv(l)*Gi(l)'*DI));
         C(l,k)=C(k,l);
      end
      if RV=='T' then
         //for k=1:q;
         for l=1:k;
            C(k,l)=C(k,l)/(normw(k)*normw(l));
            C(l,k)=C(k,l);
         end
         //end
         //for k=1:q;
         normw(k)=1;
           //end
       end
   end
    
   // Calcul de ltot 
   [CC,SC,egC]=svd(C);    
   coeff2=sum(normw)/sqrt(sum(diag(SC)));
   if RV=='T' then 
       coeff2=1;
   end
   ltot=abs(egC(:,1))*coeff2;

   // Calcul de Gtot
      Gtot=[];
      for i= 1:q; 
         Gtot= [Gtot Gi(i)];
      end

   // calcul de Btot
   Btot=Gtot'*DI*Gtot;       

   // Calcul de Vtot
   Vtot=zeros(p,p);
   deb=1
   for i=1:q;
      Vtot(deb:deb-1+pind(i),deb:deb-1+pind(i))=ltot(i)*Viinv(i);
      deb=deb+pind(i);
   end

   // decomposition de Choleski de Vtot   
   Vtothalf=chol(Vtot);
   [n_Vtothalf,p_Vtothalf]=size(Vtothalf);
   Vtothalf=[Vtothalf zeros(n_Vtothalf,n_Vtothalf-p_Vtothalf)];
   
   // diagonalisation
   diagtot=Vtothalf*Btot*Vtothalf';
   [Udiagtot,Sdiagtot,Vdiagtot]=svd(diagtot);
   egtot=Vdiagtot;
   valptot=diag(Sdiagtot);
   valpropr=valptot(valptot>0.000001);
   Atot=Vtothalf'*Vdiagtot(:,1:max(size(valpropr)));

   labels_axes='axe'+string([1:1:max(size(valpropr))]');
	
   // coordonnées des individus
   w = -Xc*Atot;

   // coordonnées des centres de gravité
   ws= - Gtot*Atot;
    
   // sorties:                 statut:            inchangé quand mult. x 10000
   
   res.p.d=Atot;               // valide               non
   res.p.i=tmp_labels_var;
   res.p.v=labels_axes;

   res.t.d=w;                  //                  oui 
   res.t.i=tmp_labels_obs;
   res.t.v=labels_axes;

   res.tg.d=ws;                // valide           oui
   res.tg.i='cl'+string([1:1:nclasses]');
   res.tg.v=labels_axes;

   res.normw.d=normw;          //                  oui
   res.normw.i=labels_tables; 
   res.normw.v='normw';

   res.cmatrix.d=C;            // valide           oui
   res.cmatrix.i=labels_tables;
   res.cmatrix.v=labels_tables;

   res.cmatrix_eigenvec.d=egC; //                  oui    
   res.cmatrix_eigenvec.i=labels_tables;
   res.cmatrix_eigenvec.v='eigenvec'+string([1:1:q]');

   res.cc.d=ltot;                //                  oui
   res.cc.i=labels_tables;
   res.cc.v='cc';

   res.vtot.d=Vtot;              // valide               non
   res.vtot.i=tmp_labels_var;
   res.vtot.v=tmp_labels_var;

   res.vtot_eigenvec.d=egtot;    // valide               non
   res.vtot_eigenvec.i=tmp_labels_var;
   res.vtot_eigenvec.v='ev'+string([1:1:size(egtot,2)]');

   res.vtot_eigenval.d=valptot;  // faux             oui
   res.vtot_eigenval.i=tmp_labels_var;
   res.vtot_eigenval.v='vtot_eigenval';

   res.xconc_mean.d=moyC';        // valide               non
   res.xconc_mean.i=tmp_labels_var;
   res.xconc_mean.v='xconc_mean';

   
 
    
                   

   
   
    

                                // autres=???  

    // normw:   (qx1)      norme du kieme opérateur associé à la LDA de Xk/y
    // C:       (q x q)    matrice des produits scalaires entre opérateurs  
    // ltot:->cc  (q x 1)    coeffs du compromis de Statis-lda obtenus après 
    		           // diagonalisation de la matrice C 
    // Vtot:-> vtot             (p x p)    matrice de variance-covariance du compromis
    // egtot:-> vtot_eigenvec 	(p x lv)   vecteurs-propres de Vtot 
    // valptot:-> vtot_eigenval (lv x 1)   valeurs-propres de Vtot   
		                           // (le 'X' du compromis de Statis-lda)


    // Atot:-> p  (p x lv)   matrice des axes de Statis-lda; vect. en colonne
    // w: -> t    (n x lv)   matrice des coordonnées des observations (w=X.Atot)
    // ws: -> tg             coordonnées des centres de gravité des q tables 
    // Xc:      (n x p)    matrice concaténée des Xi
 
    // Xc_mean: (p x 1)    moyenne des variables de Xc (concaténation des Xi) 
   
    // q = nbre de tables
    // p = nbre de variables dans Xconc_mean
    

endfunction
	


