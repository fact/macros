function c=%div_p(c)
    
    // edition d'une Tlist de type 'div'
  
    if length(size(c.d))==2 then    // matrice 2 dimensions 
      disp(msprintf('  d: [%sx%s %s]', string(size(c.d,1)), string(size(c.d,2)),typeof(c.d)) )
    elseif length(size(c.d))==3 then  // hypermatrice 3D
      disp(msprintf('  d: [%sx%sx%s %s]', string(size(c.d,1)), string(size(c.d,2)),string(size(c.d,3)),typeof(c.d)) ) 
    end
    
    disp(msprintf('  i: [%sx%s %s]', string(size(c.i,1)), string(size(c.i,2)),typeof(c.i)) )
    
    if length(size(c.d))==2 then    // matrice 2 dimensions 
      disp(msprintf('  v: [%sx%s %s]', string(size(c.v,1)), string(size(c.v,2)),typeof(c.v)) )
    elseif length(size(c.d))==3 then  // hypermatrice 3D
      disp(msprintf('  v.v1: [%sx%s %s]', string(size(c.v.v1,1)), string(size(c.v.v1,2)),typeof(c.v.v1)) )
      disp(msprintf('  v.v2: [%sx%s %s]', string(size(c.v.v2,1)), string(size(c.v.v2,2)),typeof(c.v.v2)) )
    end
    
    

endfunction
            

