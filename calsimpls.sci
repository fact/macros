function [B,T,P,dof,R]=calsimpls(x0,y0,nf)
    
    // initialisation
    [n1,p1]=size(x0);
    [n2,p2]=size(y0);
    
    if n1 ~= n2 then 
        error('x and y should have the same number of observations')
    end
    
    T=zeros(n1,nf);
    P=zeros(p1,nf);
    R=zeros(p1,nf);
    B=zeros(p1,nf,p2);
    
    s0=x0'*y0;
    
    for i=1:nf;
       if i==1 then
          [u,s_diag,v]=svd(s0);
       else 
          s=s0-P(:,1:i-1)*inv(P(:,1:i-1)'*P(:,1:i-1))*P(:,1:i-1)'*s0;
          [u,s_diag,v]=svd(s);
       end
       
       r=u(:,1);
       R(:,i)=r;
       
       t=x0*r;
       T(:,i)=t;
       
       p=x0'*t/(t'*t);
       P(:,i)=p;
       
       bi=R(:,1:i)*inv(T(:,1:i)'*T(:,1:i))*T(:,1:i)'*y0;
       
       B(:,i,:)= bi;   
    
       // note: T=X0*R 
       // et garder le terme: inv(T'*T) 
       // car la table 1 de la publi de De Jong (93) est fausse: T'T n'est pas l'identité
    
    end
    
    dof=[1:1:nf]';
    
endfunction
