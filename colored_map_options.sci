function colored_map_options(x,col1,col2,startpos,endpos,symbol,varargin)
  
  // function[h]=colored_map(s,col1,col2,startpos,endpos,symbol,(labcol1),(labcol2),(symbol_color))
  // 
  // Biplot of two columns as colored map
  // Samples are identified by their coordonates in s.d and their labels in s.i
  // The function selects a range within s.i, determined by startpos:endpos to
  // make groups, then plots the samples onto a graphic using this new 
  // group identifier
  //
  // Input:
  // ======
  // s:                     SAISIR matrix (structure with s.d,s.v,s.i)
  // col1:                  index of the first column to be represented 
  // col2:                  index of the second columns to be represented 
  // startpos:              first character for identification, within s.i
  // endpos:                last character for identification, within s.i
  // labcol1 (optional):    label of the variable forming the X-axis
  // labecol2 (optional):   label of the variable forming the Y-axis
  //
  // Output: 
  // =======
  // the graph...
  // 
  // Depending functions:
  // ==================
  // dat_creategroup1
  // dat_creategroup1>dat_selectrow
  // 
  // Miscellaneous
  // ============
  // contributors: D.Bertrand (INRA)
  // last update:  2012, april, 20
  // ----------------------------------------------------------
  
 
  global newfig; 
  
  [nargout,nargin] = argn(0)
  s=div(x);
  
  couleur = [0,0,0;1,0,0;0,0,1;0,0.7,0;0.5,0.5,0];
  couleur= [couleur;0.5,0,0.5;0,0.5,0.5;0.25,0.25,0.25;0.5,0,0;0,0.5,0];
  couleur=[couleur;0,0.5,0;0.1,0.2,0.3;0.3,0.2,0.1;0.5,0.5,0.8;0.1,0.8,0.1];
  cmap2 =couleur;      // 15 different colors
  
  symbols_list=['∆';'•';'*';'+';'x';'o';'Ø'];                   
  margin = 0.05;

  minx=min(s.d(:,col1));maxx=max(s.d(:,col1));
  miny=min(s.d(:,col2));maxy=max(s.d(:,col2));
  minx=minx-(maxx-minx)*margin;
  miny=miny-(maxy-miny)*margin;
  maxx=maxx+(maxx-minx)*margin;
  maxy=maxy+(maxy-miny)*margin;

  model = part(s.i,startpos:endpos);
  s.i=model;            // pour avoir le label à imprimer
  code_conj=str2conj(model);

  gr = groupcreate(s,code_conj);

  // rappel: gr = liste
  // on reconstruit s2 en concaténant les éléments de gr
  n_gr=size(gr);
  s2.d=[];
  s2.i=[];
  groups_s2=[];
  for j=1:n_gr;
    s2.d=[s2.d; gr(j).d];
    s2.i=[s2.i;gr(j).i];
    groups_s2=[groups_s2; j*ones(size(gr(j).d,1),1)];  // groupes ordonnés et concaténés 
  end
  s2.v=s.v;
  
  //indice = gr.d;

  if newfig=='T' then
  figure();
  end

  a=get("current_axes");
  a.data_bounds=[minx,miny;maxx,maxy];
  a.axes_visible = 'on';
  a.box = "on"; 
  
  f= gcf();
  f.background=-2;
  f.color_map=cmap2;
  f.children.font_size=2            // taille des échelles 
  f.children.x_label.font_size=3    // taille des légendes 
  f.children.y_label.font_size=3
  
  // définition des légendes en x et y
  if nargin==6 then
      f.children.x_label.text=s2.v(col1);
      f.children.y_label.text=s2.v(col2);
  end
  
  if nargin>=7 then  
      f.children.x_label.text=varargin(1);
  end;
  if nargin>=8 then  
      f.children.y_label.text=varargin(2); 
  end;
  
  
  [n,p] = size(s2.d);
  for j = 1:n;
      // définition du symbole et de sa taille
      if symbol=='diamond' then 
         symbol2='∆';
         csize=3;
      elseif symbol=='dot' then   
         symbol2='•';
         csize=5;
      elseif symbol=='star' then   
         symbol2='*';
         csize=5;        
      elseif  symbol=='labels' | symbol=='k' then
         symbol2=s2.i(j);
         csize=2;
 
      elseif (symbol=='ks'|symbol=='s') & max(code_conj)<=7 then   // symboles (peu de classes)
         symbol2=symbols_list(pmodulo(groups_s2(j)-1,7)+1);        // 7 symboles seulement 
         if symbol2=='•' | symbol2== '*' then
           csize=4;
         elseif symbol2=='∆' then 
           csize=2;
         else
           csize=3;
         end
      elseif (symbol=='ks'|symbol=='s') & max(code_conj)>7 then   // nombres (trop de classes)
         symbol2=string(code_conj(j));
         csize=3;
      end
   
    xstring(s2.d(j,col1),s2.d(j,col2),symbol2); 
                           //defines the character to be ploted
    t = get("hdl")         //gets the handle of the newly created object
    g = groups_s2(j);
    if symbol~='k'& symbol~='ks' then
      t.font_foreground = pmodulo(g-1,15)+1;  //only 15 colors defined...
    end
    t.font_size=csize;
  end;



endfunction


