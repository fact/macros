function var_out=glx_mat2div_struct2div(varname)

   // Un fichier .mat sera toujours une structure.
   // La fonction va exmplorer les différents champs selon leur nature: 'st', 'list', 'constant', 'string' ou 'div' 


    //execstr(msprintf('var_in=%s', varname));

   // identification et comptage des champs dans var_in 
    execstr(msprintf('fields_names = fieldnames(%s);',varname));
    nfields=size(fields_names,1);  
    
    // Etude de chaque champ
    for i=1:nfields;
        
       if fields_names(i)~='typeof' then

           execstr(msprintf('field_path=%s.%s;',varname,fields_names(i)));
          
           if typeof(field_path)=='st' then 
           
                if isfield(field_path,'typeof') & typeof(field_path)=='st'   then  
 
                    execstr(msprintf('field_type=%s.%s.typeof;',varname, fields_names(i))); 
  
                    if field_type=='div' then
                        execstr(msprintf('var_out.%s=[];',fields_names(i)));
                        execstr(msprintf('var_out.%s=glx_struct2div(field_path);',fields_names(i)));
                  
                    elseif field_type== 'list' then
                        execstr(msprintf('var_out.%s=glx_struct2list(field_path);',fields_names(i)));
                    elseif field_type== 'constant' |  field_type== 'hypermat'  |  field_type== 'string'      then
                        execstr(msprintf('var_out.%s=field_path.%s.list1;',fields_names(i),fields_names(i)));
                    elseif field_type=='st' then                                                               //29dec16
                        execstr(msprintf('var_out.%s=glx_struct2list(field_path);',fields_names(i))); 
                        //chemin pas testé
                    end
                else 
                    error('field_path wrong in glx_mat2div_struct2div')
                end
          
           elseif typeof(field_path)=='constant' | typeof(field_path)=='string' then 
                execstr(msprintf('var_out.%s=%s.%s;',fields_names(i),varname, fields_names(i)));
          
           end
            
        end
    end







endfunction 
