function cl = dis2conj(cld);

  //function cl = dis2conj(cld);
  // from disjunctive matrix to conjunctive vector 
  // 
  // Input:
  // ======
  // cld : matrix of Booleans (1->belongs to the class; 0-> not)
  // 
  // Output:
  // =======
  // cl :  vector of integers containing the class of each element 
  //
  // Miscellaneous:
  // ==============
  // JM Roger (IRSTEA) and JC Boulet (INRA)
  // Last update: 2012, March, 12
  // -------------------------------------

  n=size(cld,1);
  cl=zeros(n,1);
  
  if size( cld, 2) > 1  then 
        for i=1:n;
            [val,pos]=max(cld(i,:));
            cl(i)=pos;
        end
  else
    cl = cld;
  end;

  endfunction
