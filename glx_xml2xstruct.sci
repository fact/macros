function xstr=glx_xml2xstruct(xmlfile)
    
    // lecture d'un fichier .xml contenant le resultat d'une regression et ecriture sous forme d'une structure 

    doc=xmlRead(xmlfile)
    n_doc=max(size(doc.root.children)); 


    
    x=[];
    
    for i=1:n_doc;
        
        name='plan'+string(i);
        name2='x.'+name;
   
        xi=doc.root.children(i).content;
        ni=size(xi,1);
        
        // Strings converted to double
        x0=[]; 
        for j=1:ni;
            line= strsplit(xi(j,:),',');
            line=strtod(line);
            x0=[x0;line'];    
        end
    
        execstr(msprintf('%s=x0;',name2)); 
    end 
    
    xstr=x;
        

    
endfunction
