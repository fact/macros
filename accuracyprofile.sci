function [x_exact_incert_fct,x_calc_incert,x_loq]=accuracyprofile(x_in0,proba_beta,limit_accept,concentration0, slope_corr,biais_corr)
   
    // cette fonction est concue pour donner les mêmes résultats que la feuille Excel "Profil_Exactitude_v4_2.xlsx"
    // si une erreur est présente dans la feuille excel, elle est reportée ici!!

    // Entrées: ===========
    // x_in0: un div avec :
    // 		x_in0.i: les n° ou noms des jours d'acquisition 
    // 		x_in0.d: une matrice de 2 colonnes: valeur de référence et valeur prédite
    // les répétitions ne sont pas identifiées 

    // proba_beta: valeur entre 0 et 1 
    // limit_accept: limite d’acceptabilité; entre 0 et 1 <-> entre 0 et 100% d’erreur 
    // 		typiquement: 0.1 <-> 10% d’erreur 
    // concentration: une valeur pour laquelle on souhaite calculer l'incertitude de mesure
    // slope_corr: correction de pente; le facteur 1/a1 dans le cours de Feinberg 
    // biais_corr: correction de l'ordonnée à l'origine; n'est pas utilisé, mais pourrait l'être... 
    
    // sorties =============
    // x_exact_incert_fct: une structure div de dimensions (10 x n_niveaux)
    // 		=> sert à construire les profils d'exactitude, d'incertitude, et les 2 fonctions d'incertitude
    // x_calc_incert: une structure div de dimensions (6 x 1) 
    //		=> sert à calculer les fonctions d'incertitude
    // x_loq: une structure div de dimensions (4 x (n_levels-1))
    //		=> propose des valeurs de LOQ=limite de quantification 
    // ============================================================================    




    if argn(2)<2 then
        proba_beta=0.9;
    end

    if argn(2)<3 then
     	limit_accept=0.1;
    end
    
    if argn(2)<4 then
        concentration0=1;
    end

    if argn(2)<5 then
        slope_corr=1;
    end

    if argn(2)<6 then
        biais_corr=0;
    end

  

    // tri des données: d'abord les jours, puis les niveaux 
    [nul,tri]=gsort(x_in0.i,'g','i');
    x_in=x_in0(tri,:);
    [nul,tri]=gsort(x_in.d(:,1),'g','i');
    x_in=x_in(tri,:);
    
    // identification/comptage des niveaux, des jours, des répétitions 
    
    acquis_days=unique(x_in.i);
    n_days=size(acquis_days,1);
    
    levels=unique(x_in.d(:,1));
    n_levels=size(levels,1);
     
    n=size(x_in.d,1);
   
    n_repets= n/(n_days*n_levels);
    
    // vérification que les données sont bien complètes:
    
    if round(n_repets) ~= n_repets then
        disp(msprintf('found: %s observations, %s days, %s levels, %s repetitions',string(n),string(n_days), string(n_levels),string(n_repets)))
        disp('expected:  obs = days x levels x repetitions and repetitions is an integer')
        error('please check your data')
    end
 
 
    // application d'une correction biais_pente (1/a1)
    x_in.d(:,2)= biais_corr + x_in.d(:,2)*slope_corr;
    concentration=concentration0*slope_corr;   //19nov17

 
    // regroupement des calculs: 
    x_calc.d=zeros(25,n_levels);
    x_calc.i=['number of series(K)';'number of repetitions (I)'; 'SCE resid';'SCE tot';'SCE inter series';'s2L';'variance of repetability s2r';'variance inter series s2L';'variance of fidelity s2FI'; 'mean level';'std error or repetability sr';'std error inter series sL';'std error of fidelity sFI';'biais'; 'R';'B2';'coeff intermed';'degrees of freedom';'prob. beta';'Student t low';'Student t high';'Student t interpolé';'ktol';'low tolerance';'high tolerance'];
    
    x_calc.v= string(levels);
    x_calc=div(x_calc);
    
    // remplissage du nbre de séries (K) et du nbre de répétitions I)
    x_calc.d(1,:)=n_days*ones(1,n_levels);
    x_calc.d(2,:)=n_repets*ones(1,n_levels);

    // calcul de SCE residuelle et de moyenne totale
    for j=1:n_levels;
        tri=find(x_in.d(:,1)==levels(j));
        x_j=x_in(tri,:);
        sce_res=0;
        mean_level=mean(x_j.d(:,2));
        x_calc.d(10,j)=mean_level;                                      // moyenne du niveau
        for i=1:n_days;
            x_ji=x_j.d(1+(i-1)*n_repets: i*n_repets,2);
            mean_i=mean(x_ji);
            diff_i=x_ji-mean_i;
            sce_res=sce_res+diff_i'*diff_i;
        end 
        x_calc.d(3,j)=sce_res;                                          // SCE residuelle
        diff_temp=x_j.d(:,2)-mean(x_j.d(:,2));
        x_calc.d(4,j)=diff_temp'*diff_temp;                             // SCE totale
    end

    // calcul de SCE inter séries
    x_calc.d(5,:)=x_calc.d(4,:)-x_calc.d(3,:);                          // SCE inter séries 

    // calcul de s2r 
    x_calc.d(7,:)=x_calc.d(3,:) ./ (x_calc.d(1,:).*(x_calc.d(2,:)-1));   // s2r 
    
    // calcul intermédiaire de s2L
    x_calc.d(6,:)=((x_calc.d(5,:) ./ (x_calc.d(1,:)-1)) - x_calc.d(7,:)) ./ x_calc.d(2,:);
    
    // calcul de la variance inter-séries s2L
    x_calc.d(8,:)= max(0,x_calc.d(6,:));
    
    // calcul de la fidélité s2FI
    x_calc.d(9,:)=x_calc.d(7,:)+x_calc.d(8,:);
    
    //Fidélité ---------------------
    
    // Ecart-type de répétabilité sr 
    x_calc.d(11,:)=sqrt(x_calc.d(7,:));
    
    // Ecart-type inter-séries sL
    x_calc.d(12,:)=sqrt(x_calc.d(8,:));
    
    // Ecart-type de fidéflité sFI
    x_calc.d(13,:)=sqrt(x_calc.d(9,:));
    
    // Justesse ---------------------
 
    // Biais
    x_calc.d(14,:)=x_calc.d(10,:) ./ strtod(x_calc.v') -1;
    
    // Facteur de couverture --------
    
    // Rapport des variances R=s2L/s2r
    x_calc.d(15,:)=x_calc.d(8,:) ./ x_calc.d(7,:);
    
    // Coeffficient B2
    x_calc.d(16,:)=(x_calc.d(15,:)+1) ./ (x_calc.d(2,:).*x_calc.d(15,:)+1);
    
    //Coefficient intermédiaire:
    x_calc.d(17,:)=sqrt(1 ./(x_calc.d(1,:).*x_calc.d(2,:).*x_calc.d(16,:))+1);

    // Nombre de degrés de liberté 
    num1=(x_calc.d(15,:)+1).^2;
    denom1= (x_calc.d(15,:)+1 ./x_calc.d(2,:)).^2 ./ (x_calc.d(1,:)-1);
    denom2= (1- 1 ./x_calc.d(2,:)) ./ (x_calc.d(1,:).*x_calc.d(2,:));
    
    x_calc.d(18,:)=num1 ./ (denom1 + denom2);
    
    // Proba beta: donnée en entrée
    x_calc.d(19,:)=proba_beta*ones(1,n_levels);

    // t student bas 
    // attention: dans la fonction cdft de Scilab entrer p/2 pour P et 1-p/2 pour Q  
    // P=0,1 ddl=5 T=2,015 
    x_calc.d(20,:)=cdft("T",floor(x_calc.d(18,:)),1-(1-x_calc.d(19,:))/2, (1-x_calc.d(19,:))/2);
    
    // t student haut 
    x_calc.d(21,:)=cdft("T",ceil(x_calc.d(18,:)),1-(1-x_calc.d(19,:))/2, (1-x_calc.d(19,:))/2);
    
    // t student interpolé
    x_calc.d(22,:)=x_calc.d(20,:)- (x_calc.d(20,:)-x_calc.d(21,:)).*(x_calc.d(18,:)-floor(x_calc.d(18,:)));
    
    // facteur de tolérance ktol 
    x_calc.d(23,:)=x_calc.d(22,:).*x_calc.d(17,:); 
    
    // valeur basse tolérance 
    x_calc.d(24,:)=x_calc.d(10,:)-x_calc.d(23,:).*x_calc.d(13,:);
    
    // valeur haute tolérance
    x_calc.d(25,:)=x_calc.d(10,:)+x_calc.d(23,:).*x_calc.d(13,:);
   
 
    // remplissage des données de synthèse:  
    x_out.d=zeros(41,n_levels);
    x_out.v=x_calc.v;
    //x_out.i=['proba beta';'limit of acceptability';'level reference';'level mean';'std error or repetability sr';'std error inter series sL';'std error of fidelity sFI';'variances ratio R';'coefficient';'dof';'low tolerance';'high tolerance';'bias %';'recouvrement %';'low tolerance limit %';'high tolerance limit %';'low acceptability %';'high acceptability %'];
    
    x_out.i=['proba beta';'limit of acceptability';'reference values';'observed mean values';'std error or repetability (sr)';'std error of fidelity (sFI)';'degrees of freedom';'interpolated t-student (ktol)';'std error of IR (sIT)';'low limit of the tolerance range';'high limit of the tolerance range';'relative biais %';'recovery %';'low tolerance limit %';'high tolerance limit %';'low acceptability limit %';'high acceptability limit %'; 'expanded uncertainty';'relative uncertainty %'; 'relative uncertainty / low limit %';'relative uncertainty / high limit %';'uncertainty function coefficients / power';'uncertainty function coefficients / constant';'expanded relative uncertainty %';'Y';'X'; 'concentration (corrected)';'low limit of expanded uncertainty';'high limit of expanded uncertainty';'low acceptability limit';'high acceptability limit';'low LOQ-t1';'low LOQ-a1';'low LOQ-t0';'low LOQ-a0';'LOQ-low intersection';'high LOQ-t1';'high LOQ-a1';'high LOQ-t0';'high LOQ-a0';'LOQ-high intersection' ];
     
    // tolérance beta 
    x_out.d(1,:)= x_calc.d(19,:);

    // limite d'acceptabilité
    x_out.d(2,:)=limit_accept * ones(1,n_levels);
    
    // valeur cible (ref.)
    x_out.d(3,:)=strtod(x_calc.v)';

    // valeur observée (moyenne)
    x_out.d(4,:)=x_calc.d(10,:);
    
    //ecart-type de répétabilité sr  
    x_out.d(5,:)=x_calc.d(11,:);
    
    //ecart-type de fidélité sFI
    x_out.d(6,:)=x_calc.d(13,:);
    
    //nombre de degrés de liberté
    x_out.d(7,:)=x_calc.d(18,:);
    
    //facteur de couverture (ktol)=t student interpollé
    x_out.d(8,:)=x_calc.d(22,:);
    
    // ecart-type du IT =(sIT)=t student interpollé * Ne=coeff intermédiaire=incertitude type composée 
    x_out.d(9,:)=x_calc.d(13,:) .* x_calc.d(17,:);
    
    //  rapport des variances R
    //x_out.d(8,:)=x_calc.d(15,:);

    // coeff. intermédiaire 
    //x_out.d(9:10,:)=x_calc.d(17,:);
    
    // valeurs basse + haute tolérance
    x_out.d(10:11,:)=x_calc.d(24:25,:);
    
    // biais %
    x_out.d(12,:)=100*x_calc.d(14,:);


    // --------------------------
    // exactitude 
    // --------------------------
    
    // valeur de référence: 
    // =x_out.d(3,:)
    
    // taux de recouvrement (justesse)%
    x_out.d(13,:)=100*x_out.d(4,:)./x_out.d(3,:);

    // limites basse et haute tolérance %
    x_out.d(14:15,:)=100*x_out.d(10:11,:)./ (ones(2,1)*x_out.d(3,:));
    
    // limites d'acceptabilité basse et haute 
    x_out.d(16,:)=100*(1-x_out.d(2,:));
    x_out.d(17,:)=100*(1+x_out.d(2,:));

    //----------------------------
    // incertitude
    // ---------------------------

    // valeur de référence: 
    // =x_out.d(3,:)

    // incertitude type composée (=sIT)
    // = x_out.d(9,:);
    
    // incertitude étendue
    x_out.d(18,:)=2*x_out.d(9,:);
    
    // incertitude relative (%)
    x_out.d(19,:)=100*x_out.d(18,:)./x_out.d(3,:);

    // limite basse incertitude relative 
    x_out.d(20,:)=100-x_out.d(19,:);
    
    // limite haute intertitude relative 
    x_out.d(21,:)=100+x_out.d(19,:);


    // -----------------------------------------------------
    // coeffs de la fonction d'incertitude élargie relative
    // -----------------------------------------------------
    
    // Y
    x_out.d(25,:)=log(x_out.d(19,:)/100)/log(10);
    
    // X
    x_out.d(26,:)=log(x_out.d(3,:))/log(10);
    
    // calcul de la régression entre X et Y 
    X=x_out.d(25,:)';
    Y=x_out.d(26,:)';
//    X2=[ones(n_levels,1) X];
//    res=inv(X2'*X2)*X2'*Y;
    Y2=[ones(n_levels,1) Y];
    res=inv(Y2'*Y2)*Y2'*X;

    origine=res(1);
    pente=res(2);  
     //pause 
      
    // puissance
    x_out.d(22,:)= pente*ones(1,n_levels);
    
    // constante 
    x_out.d(23,:)=10**origine * ones(1,n_levels);
    
    // incertitude relative élargie  (%)
    x_out.d(24,:)=100*x_out.d(23,:)*concentration**x_out.d(22,1);
   
    // valeur demandée
    x_out.d(27,:)=concentration*ones(1,n_levels);
    
    //incertitude relative seuil min 
    x_out.d(28,:)=x_out.d(27,:).* (1-0.01*x_out.d(24,:)); 
    
    //incertitude relative seuil max 
    x_out.d(29,:)=x_out.d(27,:).* (1+0.01*x_out.d(24,:)); 
   


    // --------------------
    // LOQ 
    // --------------------
    
    // valeur de référence: 
    // =x_out.d(3,:)

    // valeur basse intervalle tolérance 
    // = x_out.d(10,:)
    
    // valeur haute intervalle tolérance 
    // =x_out.d(11,:)
   
    // limite d'acceptabilité basse 
    x_out.d(30,:) = 0.01*x_out.d(16,:) .* x_out.d(3,:);
    
    // limite d'acceptabilité haute 
    x_out.d(31,:) = 0.01*x_out.d(17,:) .* x_out.d(3,:);
   
    // LOQ basse - t1
    x_out.d(32,1:n_levels-1)=(x_out.d(10,2:n_levels)-x_out.d(10,1:n_levels-1)) ./ ((x_out.d(3,2:n_levels)-x_out.d(3,1:n_levels-1)));
    
    // LOQ basse - a1
    x_out.d(33,1:n_levels-1)=(x_out.d(30,2:n_levels)-x_out.d(30,1:n_levels-1)) ./ ((x_out.d(3,2:n_levels)-x_out.d(3,1:n_levels-1)));
   
    // LOQ basse - t0
    x_out.d(34,:)=x_out.d(10,:)-x_out.d(32,:).*x_out.d(3,:);
    x_out.d(34,n_levels)=0;
    
    // LOQ basse - a0
    x_out.d(35,:)=x_out.d(30,:)-x_out.d(33,:) .* x_out.d(3,:);
    x_out.d(35,n_levels)=0;
   
    // LOQ possibles - 1
    for i=1:n_levels-1;
        nomin=x_out.d(35,i)-x_out.d(34,i);
        denomin=x_out.d(32,i)-x_out.d(33,i);
        if denomin ~=0 then
            x_out.d(36,i)=nomin/denomin;
        end
    end
    
    // LOQ haute - t1
    x_out.d(37,1:n_levels-1)=(x_out.d(11,2:n_levels)-x_out.d(11,1:n_levels-1)) ./ ((x_out.d(3,2:n_levels)-x_out.d(3,1:n_levels-1)));
    
    // LOQ haute - a1
    x_out.d(38,1:n_levels-1)=(x_out.d(31,2:n_levels)-x_out.d(31,1:n_levels-1)) ./ ((x_out.d(3,2:n_levels)-x_out.d(3,1:n_levels-1)));
   
    // LOQ haute - t0
    x_out.d(39,:)=x_out.d(11,:)-x_out.d(37,:).*x_out.d(3,:);
    x_out.d(39,n_levels)=0;
    
    // LOQ haute - a0
    x_out.d(40,:)=x_out.d(31,:)-x_out.d(38,:) .* x_out.d(3,:);
    x_out.d(40,n_levels)=0;
   
    // LOQ -2
    for i=1:n_levels-1;
        nomin=x_out.d(40,i)-x_out.d(39,i);
        denomin=x_out.d(37,i)-x_out.d(38,i);
        if denomin ~=0 then
            x_out.d(41,i)=nomin/denomin;
        end
    end
    
    
   
    // gestion des arrondis 
 
    // variables à 2 chiffre après la virgule 
    x_out.d([12:17,19:21,24],:)=0.01*round(100*x_out.d([12:17,19:21,24],:));

    // variables à 3 chiffres après la virgule
    x_out.d([18,36,41,28,29],:)= 0.001*round(1000*x_out.d([18,36,41,28,29],:));
     

    x_out=div(x_out);


    
    // découpe du tableau de sortie en 3, car pas les mêmes dimensions 
    x_exact_incert_fct=x_out([3,13:17,20,21,18,19],:);
 
    x_calc_incert=x_out([22,23,27,24,28:29],1);
    x_calc_incert.v='values';

    x_loq.d=[x_out.d(3,1:n_levels-1);x_out.d(3,2:n_levels);x_out.d(36,1:n_levels-1);x_out.d(41,1:n_levels-1)];
    x_loq.v='range' + string([1:n_levels-1]);
    x_loq.i=['low limit';'high limit';x_out.i(36);x_out.i(41)];
    x_loq=div(x_loq);
    
    
endfunction
