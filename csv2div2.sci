function [data] = csv2div2(filename)                                    

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
  // reads a .csv file (field separation = ;/ decimal separator=. ) and converts it to .div
  // the first line and the first column of the .csv file must be labels                                                
  // the cell in position (1,1) is omited 
  // updated: 2015, december, 31 by JCB

  data=csv2div(filename,',','.');
  
endfunction
