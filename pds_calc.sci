function [model_pds]=pds_calc(x_master,x_slave,w);

// PDS function computes a matrix b and an offset o so that s=m*b+o
// w  : width of the sliding window (must be odd)

// JM Roger, IRTSEA, 2016

  // div
  m1=div(x_master);
  s1=div(x_slave);

  m=m1.d;
  s=s1.d;

  [nm,pm]=size(m);
  [ns,ps]=size(s);

  // vérifications d'usage
  if (nm~=ns) | (pm~=ps)
      error('dimensions do not match');
  end

  if (w<3) | (2*floor(w/2)==w)
     error('w must be odd and >=3');
  end;

  // centrage des matrices m et s
  mm=mean(m,'r');      // moyennes des colonnes, ce qui donne un vecteur ligne
  ms=mean(s,'r');

  xm=m-repmat(mm,nm,1);
  xs=s-repmat(ms,nm,1);

  // demi-fenêtre
  l=floor(w/2);

  // init b
  b = zeros(pm,pm);

  // calcul pour chaque longueur d'onde
  for i=1:ps
     // extraction des données à utiliser pour la régression
     y=xs(:,i);
     i1=max(1,i-l); i2=min(pm,i+l);
     x=xm(:,i1:i2);

     // extraction des dimensions principales
     [u,s,v]=svd(x);

     // detection de la courbure max sur la courbe des VP
     d=diag(s);
     dn=d./d(1);                             // normalisation / VP1
     z = [dn(2);dn;dn($-1)];               // on étend de chaque coté
     dd = z(1:$-2)-2*z(2:$-1)+z(3:$);  // calcul derivée seconde
     [dm,j]=max(dd);                         // max de la dérivée seconde

     // on garde tout ce qui est en dessous du max de courbure
     ind = 1:j-1;

     // rÈgression
     bb=u(:,ind)\y;
     bb=v(:,ind)*inv(s(ind,ind))*bb;

     // stockage dans b
     b(i1:i2,i)=bb;
  end;

  // calcul de l'offset
  offset = ms - mm*b;


  // sorties
  model_pds.b.d=b;
  model_pds.b.i=m1.v;
  model_pds.b.v=m1.v;
  model_pds.b=div(model_pds.b);

  model_pds.offset.d=offset';
  model_pds.offset.i=m1.v;
  model_pds.offset.v='offset';
  model_pds.offset=div(model_pds.offset);


  endfunction
