function [Psignal,Tscores] =  icascores(x,ICs,varargin);


  // INPUT variables :
  //
  //   X(nxT) = mixed signal dataset
  //   ICs=  Sources (pure signal components) =underlying signals =pure spectra
  //   where :
  //   n=  Sensors (mixed signal inputs) = rows =spectra
  //   T=  Signal data points = columns =variables
  //
  //   Options.Method = 'Tall', 'Wide', 'Kernel', 'ComDim', 'Normal' X
  //   Options.Partitions = number of partition if X tall or wide
  //
  //
  // OUTPUT variables :
  //
  //   Signal(nxT) or Signal(ICsxT)  = pure signal dataset
  //   Scores = "scores" of samples calculated from ICs
  //          = old_X*Signal*inv(Signal'*Signal);
  //
  //
  // CALLS :
  //
  // icajade
  // icacolumnpartition
  // icarowpartitionar
  // ica_pca_tall_pct_dnr
  //----------------------------------------------------------------------
  
  x=div(x);

  X=x.d;
  [nR,nC]	= size(X);
  MaxRank=min(nR,nC);

  // default values
  centering_option='data not centered, ';

  // arguments optionnels -> par défaut
  if argn(2)>2 then // if exists('Options')
     Options=varargin(1);
     if ~isfield(Options,'centred') then
        Options.centred=0;
     end
     if ~isfield(Options,'method') then
        Options.method='normal';
     end
  elseif argn(2)==2 then
     Options.centred=0;
     Options.method='normal';
  end
          
      
  // application des options 'centred' et 'method'
  select Options.centred
     case 1 // X matrix centred
          X=X-ones(nR,1)*mean(X,'r');
          centering_option='data centered, ';  
     else
          X=X;
  end


  select Options.method
     case 'tall'
        if isfield(Options,'partitions') then
           partitions=Options.partitions;
        else
           Options.partitions=1;
           partitions=Options.partitions;
        end
                
        //// Do Tall segmented PCT
        [t] = ica_pca_tall_pct_dnr(X, MaxRank, partitions);
        [u,s,v]=svd(t,'e');
                
        // Calculate PCT Loadings
        Techelle=pinv(u'*u)*u';
        Vpct=Techelle*X;
        B =  icajade(Vpct',ICs);

        // Calculate pure signals from unmixing matrix and X
        Signal=(B.d'*Vpct)';

        method_option='method=tall, partitions=' +string(partitions);
                

     case 'wide'
        if isfield(Options,'partitions');
           partitions=Options.partitions;
        else
           Options.partitions=1;
           partitions=Options.partitions;
        end

        Xi = icacolumnpartition(X, partitions);
        T_all=[];
        for i = 1:partitions
           [U_in,S_in,V_in]= svd(Xi(i),'e');
           minICS=min(ICs,size(U_in,2));
           T_all=[T_all U_in];
        end
        //// Do PCT on concatenated Scores of Segments
        [u,s,v]= svd(T_all,'e');

        //// Calculate PCT Loadings
        Techelle=pinv(u'*u)*u';
        Vpct=Techelle*X;

        B =  icajade(Vpct',ICs);

        // Calculate pure signals from unmixing matrix and X
        Signal=(B.d'*Vpct)';
                
        method_option='method=wide, partitions='+ string(partitions);

     
     case 'kernel'
        if nR>nC then 
          Kernel=(X'*X)/nC;

          [u,s,v]= svd(Kernel,'e');
          D=s*s;      // D= Eigenvalues
        
          Ut=u; // Ut here == v from SVD on X; Ut= Eigenvectors

          [puiss,k]= sort(diag(D),'descend')	;
          rangeW= 1:ICs			;   // indices to the ICs  most significant directions

          Ut=Ut(:,k(rangeW));

          W=(X*Ut)';// whitener
          X2= W*X;

          B =  icajade(X2',ICs);

          // Calculate pure signals from unmixing matrix and X
          Signal=(B.d'*X2)';
          
        else
          Kernel=(X*X')/nR;

          [u,s,v]= svd(Kernel,'e')	;
          D=s*s;      // D= Eigenvalues
                    
          Ut=u; // Ut here == v from SVD on X; Ut= Eigenvectors

          [puiss,k]= gsort(diag(D),'g'); //tri décroissant par défaut
          rangeW= 1:ICs;  // indices to the ICs  most significant directions 

          Ut=Ut(:,k(rangeW));

          //// Calculate PCT Loadings
          Techelle=pinv(Ut'*Ut)*Ut';
          Vpct=Techelle*X;

          B =  icajade(Vpct',ICs);

          // Calculate pure signals from unmixing matrix and X
          Signal=(B.d'*Vpct)';
        end
                
        method_option='method=kernel';

     
     case 'comdim'
        if isfield(Options,'partitions');
           partitions=Options.partitions;
        else
           Options.partitions=1;
           partitions=Options.partitions;
        end

        Xi = icacolumnpartition(X, partitions);

        // Do Segmented PCT based on ComDim trick
        // SVD on matrix of sums of X.X'
        XiXiT=zeros(nR,nR);
        for i = 1:partitions
           XiXiT=XiXiT+Xi(i)*Xi(i)';
        end

        // Do PCT on summed Sample-based variance matrices
        [u,s,v]= svd(XiXiT, 'e');

        // Calculate PCT Loadings
        Techelle=pinv(u'*u)*u';
        X0=Techelle*X;

        B =  icajade(X0',ICs);

        // Calculate pure signals from unmixing matrix and X
        Signal=(B.d'*X0)';
                
        method_option='method=comdim';

 
     case 'normal'
        B =  icajade(X',ICs);

        // Calculate pure signals from unmixing matrix and X
        Signal=(B.d'*X)';
        Scores=X*Signal*pinv(Signal'*Signal);
                
        method_option='method=normal';
        
  end



  
  // JCB - Mars 2014 managing the signs of the B line vectors 
  for i=1:ICs;
      if Signal(1,i)<0  then 
         Signal(:,i)=-Signal(:,i);
      elseif Signal(1,i)== 0 then 
          if Signal(2,i)<0 then
             Signal(:,i)=-Signal(:,i);
          elseif Signal(2,i)==0 then 
              if Signal(3,i) <0 then
                 Signal(:,i)=-Signal(:,i);
              end
          end
      end
  end
  
  Scores=X*Signal*pinv(Signal'*Signal); 
  
  
  
  Psignal.d=Signal;
  Psignal.i=x.v;
  Psignal.v='ic'+string([1:1:ICs]');

  Psignal=div(Psignal);
  
  Tscores.d=Scores;
  Tscores.i=x.i;
  Tscores.v='ic'+string([1:1:ICs]');

  Tscores=div(Tscores);
  
   
endfunction
