function[res]=kcmeans(xi,ngroup,nchanged)

  //nuee				- Nuee dynamique (KCmeans)
  //function[res]=kcmeans(X,ngroup,(nchanged))
  //Clusters the data into ngroup according to the KCmeans method ("nuée dynamique");
  //
  //Input arguments
  //==============
  //X: SAISIR data matrix
  //ngroup (integer): number of groups asked for
  //nchanged (optional):stop iteration when there are still nchanged groups
  //which have changed in the previous iteration
  //This allows sparing some time. nchanged must be small in comparison with
  //the number of rows of X
  //
  //Output argument
  //==============
  //res with fields
  //group: SAISIR vector of groups. Observations with the same group number
  //have been classified in the same group.
  //centre: barycenter of the groups.
  //
  //Warning: the function  may reduce the number of groups
  //
  // Attention: plusieurs exécutions -> résultats différents!!!

  saisir=div(xi);

  [nargout,nargin] = argn(0) 
  lines(10000000); 
  [n,p]=size(saisir.d);

  xgroup=ceil(rand(n,1)*ngroup);
  if(nargin<3) then nchanged=0;
  end
  moved=nchanged+1;
  iter=0;
  //disp('computing ...');
  while moved>nchanged;
    iter=iter+1;
    //disp(iter);
    moved=0;
    nlost=0;
    lost=[];
    for i=1:ngroup
      if(sum(xgroup==i)==0)
        nlost=nlost+1;
        disp(['Warning a group is empty. Reduced to ' string(ngroup-nlost)]);
        lost(nlost)=i;
      else centre(i,:)=mean(saisir.d(xgroup==i,:),1);
      end;
    end
    if(nlost>0) then 
      centre(lost,:)=[];
      ngroup=ngroup-nlost;
    end
    for j=1:n
      if(pmodulo(j,100000)==0)
        disp(string([j n]));
      end
      x=saisir.d(j,:);
      for g=1:ngroup
        d(g)=0;
        for var=1:p
          delta=centre(g,var)-x(var);
          d(g)=d(g)+delta*delta;
        end
      end
      [bid,index]=min(d(1:ngroup));
      if(index~=xgroup(j));
        xgroup(j)=index;
        moved=moved+1;
      end
    end
  end

  
  // sorties div:
  
  res.group.d=xgroup;
  res.group.i=saisir.i;
  res.group.v='group';
  res.group=div(res.group);
  
  res.barycenter.d=centre;
  res.barycenter.v=saisir.v;
  for i=1:ngroup
    chaine=['G' + string(i) + '                '];
    res.barycenter.i(i)=part(chaine,(1:10));
  end
  res.barycenter=div(res.barycenter);
  
  xdisp([string(ngroup), ' actually found']);

endfunction

