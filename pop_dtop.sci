function [resfinal]=pop_dtop(x,xsm,xss,maxdim)


xg=div(x);
[nxg,pxg]=size(xg.d);


[nxsm,psm]=size(xsm.d);

// initialisation de D
D.d=[];
D.i=[];
D.v=[];

// calcul de D
D.d=xsm.d-xss.d;
D.i='diff of standard' + string([1:1:nxsm]');
D.v=xsm.v;

 D=div(D);
 [nxg,pxg]=size(D.d);

  //ACP non centrée (IIR: pas demandé; EPO: déjà centré avec xgi)

  model_pca=pcana(D,0,0);
  ndim=min(maxdim,rank(D.d));

  pop=model_pca.eigenvec.d(:,1:ndim);     //POP=vecteurs propres utilisés pour projection orthogonale

  ev_pcent=model_pca.ev_pcent;
  ev_pcent.infos(1)='number of detrimental dimensions';

  xg0=[xsm;xss];
  disj_echant=[repmat(1,nxsm,1);repmat(2,nxsm,1)];


  proj_ortho=eye(pxg,pxg)-pop(:,1:ndim)*pop(:,1:ndim)';
  stcorr=xg0;
  stcorr.d=xg0.d*proj_ortho;
  calcorr=x;
  calcorr.d=calcorr.d*proj_ortho;



  // sorties Div:


  resfinal.epo_corr.d=proj_ortho;
  resfinal.epo_corr.i=x.v;
  resfinal.epo_corr.v=x.v;
  resfinal.epo_corr=div(resfinal.epo_corr);

  resfinal.xcorr.d=x.d*proj_ortho;
  resfinal.xcorr.i=x.i;
  resfinal.xcorr.v=x.v;
  resfinal.xcorr=div(resfinal.xcorr);

  resfinal.xsmcorr.d=xsm.d*proj_ortho;
  resfinal.xsmcorr.i=xsm.i;
  resfinal.xsmcorr.v=xsm.v;
  resfinal.xsmcorr=div(resfinal.xsmcorr);

  resfinal.xsscorr.d=xss.d*proj_ortho;
  resfinal.xsscorr.i=xss.i;
  resfinal.xsscorr.v=xss.v;
  resfinal.xsscorr=div(resfinal.xsscorr);


endfunction
