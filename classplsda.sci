function [res_classpls2da] = classplsda(x,y,lv)


    // distributed under the CeCILL-C license
    // copyright INRA-IRSTEA 2013    

    
    // Initialisation:
    [n,q]=size(x);   

    y=conj2dis(y);                 
    ngroups=size(y,2);
    
    [b,t,p,dof,r]=calsimpls(x,y,lv);
    
    // Sorties:
    res_classpls2da.scores=t;
    res_classpls2da.rloadings=r;

    
endfunction
