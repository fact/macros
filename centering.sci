function [saisir, xmean] = centering(x,varargin)

			
  //function [X1, xmean] = s_center(X)  - subtracts the average to each row 
  //
  //Input argument:
  //---------------
  //x : matrix or Saisir structure
  //
  //Output argument:
  //---------------
  //saisir:SAISIR matrix (n x p) centered (the average of each column of X1 is
  //equal to 0)
  //xmean: SAISIR vector (1 x p) of the average row.

  
  saisir1=div(x);
  
  if  argn(2)==1 then
    aux=mean(saisir1.d,'r')';

  elseif argn(2)==2 then 
    aux1=varargin(1);
    aux1=div(aux1);
    aux=aux1.d;
    if min(size(aux))~=1 then
        error('argument 2 shoul contain a vector')
    else 
        if size(aux,1)==1 then 
           aux=aux';
        end
        if size(aux,1)~=size(saisir1.d,2) then
           error('arg.1 and arg.2 should have the same number of variables') 
        end
    end
  end

  saisir.d=saisir1.d-ones(size(saisir1.d,1),1)*aux';
  saisir.i=saisir1.i;
  saisir.v=saisir1.v;

  xmean.d=aux;
  xmean.i=saisir1.v; 
  xmean.v='average';
  
  //div
  saisir=div(saisir);
  xmean=div(xmean);
  
endfunction

