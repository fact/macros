function [result]=cspcana(x,dim)
   
   x0=div(x);
   
   if argn(2)==1 then
      dim2=rank(x0.d);
   elseif argn(2)==2 then
      dim2=min(dim,rank(x0.d));
   else
      error('between 1 and 2 input arguments expected')
   end;
    
   result=pcana(x0,1,1,dim2)
    
    
endfunction
