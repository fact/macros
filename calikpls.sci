function [b,T,P,dof,W] = calikpls(x,y,nf)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  W=[]; 
  R=[];
  P=[];
  Q=[];
  T=[];
  
  XY=x'*y;
  for i=1:nf;
    w=XY;
    w=w/sqrt(w'*w);
    r=w;
    for j=1:i-1;
      r=r-(P(:,j)'*w)*R(:,j);
    end
    t=x*r;
    tt=t'*t;
    p=x'*t/tt;
    q=(r'*XY)'/tt;
    XY=XY-(p*q')*tt;
    T=[T t];
    P=[P p];
    Q=[Q q];
    R=[R r];
    W=[W w];  				// 23juil16
  end
  
  b=cumsum(R*diag(Q'),2);
   
  dof=[1:1:nf]';

endfunction
