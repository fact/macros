function [secv,predcv,dof,lot] = cvxx(x,y,split,func,cent,varargin)


    // Cross validation of any regression function
    //
    // Input:
    // =====
    // x:           matrix, n X p
    // y:           vector of reference values, n X 1
    // nf:          number of factors (latent variables for PLSR); usually an integer but it
    //              can be a vector for the 'ridge' regression
    // split:       how x is splitted during cross-validation
    //              if split is not a structure:
    //                  a scalar ns provokes a random ns subset CV
    //                  two scalars [ns,ni] provoke a double CV (ns random subsets) x ni iterations
    //                  a vector of n block numbers provokes a guided subset CV
    //                  a matrix of n x k block numbers provokes k guided subset CV
    //                  "vnbN" provokes a N blocks venitian blind CV (e.g. "vnb15")
    //                  "jckN" provokes a N blocks jackkniffe CV (e.g. "jck15")
    //              if split is a structure:
    //                  .method:        a string        the CV method: 'random','jack','venitian' or 'prefixed'
    //                  .nblocks:       the parameter attached to the method
    //                                  'random', 'jack' or 'venitian':'    -> the number of blocks
    //                                  'prefixed -> a conjunctive matrix (n*r) with the numbers of the k blocks repeated r times
    //                                               or a disjunctive matrix (n * k * r) for k blocks repeated r times
    //                  .nbrepeat:      the number of repetitions of the CV
    //                  .sameobs:       the identification of the observations that cannot be split in cal and val datasets
    // func:        function called for each step of the cross validation
    //              possible choices: 'pls' , 'pcr', 'vodka' ,'ridge'
    // cent :       if 1, data are centered
    //              if 0, data are not centered
    //
    //
    //
    // Output:
    // =======
    // secv:    cross validation errors. The size depends on the parameters of the function used (ex: nbr of LV)
    // predcv:  predictions of the cross validation
    // 
    // Example:
    // ========
    // [secv,pred] = xxcvn(x,y,[10 30],'pls',1 ,20); 
    //          cross validation applied to 10 blocks 30 times for a PLSR on x,y with 20 
    //          latent variables and with centering 
    // [secv,pred] = xxcvn(x,y,10,'vodka',20, 2,1); 
    //          cross validation applied to 10 blocks for a VODKA-PLSR on x,y with 20 
    //          latent variables, r option=2 and after centering 
    // 
    // Miscellaneous:
    // ==============
    // JM Roger (IRSTEA) and JC Boulet (INRA), France
    // Last update: 2018, Novembre, 5
    // -----------------------------------------------  


    // ------------------------------------------------------------------retro-compatibilité:
    if typeof(split)~='st' then
        
        [secv,predcv,dof] = cvxx_2(x,y,split,func,cent,varargin(:))


    // --------------------------------------------------------------------------------------
    else
        [mx,nx] = size(x);
        [my,ny] = size(y);
        if mx ~= my
            error('The number of samples of x and y does not match')
        end


        // -------------------------------------------------------------
        // gestion des arguments, champs et valeurs par défaut de split
        split2=split;

        if isfield(split2,'method') then
            method=split2.method;
        else
            method='random';
            split2.method='random';
        end

        if isfield(split2,'nblocks') then
            nblocks=split2.nblocks;
        else
            nblocs=2;
            split2.nblocks=2;
        end

        if isfield(split2,'nbrepeat') then
            if method ~='prefixed' then
                nbrepeat=split2.nbrepeat;
            else
                nbrepeat=size(split2.nbrepeat,2);               // nbre de répétitions = nbre de colonnes
                split2.nbrepeat=nbrepeat;
            end
        else
            nbrepeat=10;
        end

        if isfield(split2,'sameobs') then
            sameobs=split2.sameobs;
        else
            sameobs=[1:mx];                 // chaque observation est unique
            split2.sameobs=sameobs;
        end

        // validation des valeurs entrées:
        if method~='random' & method~='jack' & method~='venitian' & method~='prefixed' then
            error('CV method should be ', 'random'', ''jack'' ,''venitian'' or ''prefixed''.')
        end

//pause
        // ----------------------------------------------------------------------
        // utilisation des blocs en disjonctif
        lot=cvgroups(mx,split2);


        //----------------------------------------------------------------------
        // application de la CV avec les modèles
        [n,ni,nj] = size(lot);                                  // n=mx...

        for j=1:nj;                                             // pour chaque répétition
            for k=1:ni                                          // pour chaque lot
                ns = max(lot(:,k));                             // nombre de lots
                for i=1:ns
                    // splitting cal and test sets
                    indval =  find(lot(:,k)==i);
                    indcal =  find(lot(:,k)~=i);

                    // Calling of the calibration+test function:
                    if ~isempty(indval) &  ~isempty(indcal) then
                        [s,pr,biaisjc,last_argout]=caltestxx(x(indcal,:),y(indcal,:),x(indval,:),y(indval,:),func,cent,varargin(:));
                        dof=last_argout.dof;
                        clear biaisjc bcoeffjc tscoresjc ploadsjc  last_argout // dof is kept
                    end
                

                    if (i==1 & k==1 & j==1)  then                      // 1° appel -> initialisation  CONTINUER
                        press = 0*s;
                        sz = size(pr); 
                        sz=sz(2:$);
                        prcv = zeros(mx,prod(sz));
                    end;
                    // the following works whatever the size of the arguments returned by caltestxx
                    press = press + (s.^2) * length(indval);
                    prcv(indval,:) = prcv(indval,:) + matrix(pr(:),length(indval),prod(sz));

                end
            end;
        end;

        // reshape of the output arguments
        secv = sqrt(press/(mx*ni*nj));
        predcv = matrix(prcv(:)./ni,[mx,sz]);

    end;

endfunction







// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX







function [secv,predcv,dof] = cvxx_2(x,y,split,func,cent,varargin)

// function [secv,predcv] = cal_xxcvn(x,y,split,'func',(r -for vodka-),centering);
// Cross validation of any regression function
//
// Input:
// =====
// x:       matrix, n X p
// y:       vector of reference values, n X 1
// nf:      number of factors (latent variables for PLSR); usually an integer but it
//          can be a vector for the 'ridge' regression
// split:   how x is splitted during cross-validation
//            a scalar ns provokes a random ns subset CV
//            two scalars [ns,ni] provoke a double CV (ns random subsets) x ni iterations
//            a vector of n block numbers provokes a guided subset CV
//            a matrix of n x k block numbers provokes k guided subset CV
//            "vnbN" provokes a N blocks venitian blind CV (e.g. "vnb15")
//            "jckN" provokes a N blocks jackkniffe CV (e.g. "jck15")
// 'func':  function called for each step of the cross validation
//            possible choices: 'pls' , 'pcr', 'vodka' ,'ridge'
// cent :  if 1, data are centered
//         if 0, data are not centered
//
// Output:
// =======
// secv:    cross validation errors. The size depends on the function used
// predcv:  predictions of the cross validation
//
// Example:
// ========
// [secv,pred] = xxcvn(x,y,[10 30],'pls',1 ,20);
//          cross validation applied to 10 blocks 30 times for a PLSR on x,y with 20
//          latent variables and with centering
// [secv,pred] = xxcvn(x,y,10,'vodka',20, 2,1);
//          cross validation applied to 10 blocks for a VODKA-PLSR on x,y with 20
//          latent variables, r option=2 and after centering
//
// Miscellaneous:
// ==============
// JM Roger (IRSTEA) and JC Boulet (INRA), France
// Last update: 2012, March, 28
// -----------------------------------------------

[mx,nx] = size(x);
[my,ny] = size(y);
if mx ~= my
error('The number of samples of x and y does not match')
end

if part(func,[1 2])=='nn' then
flag='NL';
else
flag='L';
end


// determination of the blocks and iterations
//pause
lot=cvgroups(mx,split);

[n,ni] = size(lot);

press = 0;

for k=1 : ni    // for each repetition
ns = max(lot(:,k)); // number of splits
for i=1 : ns
// splitting cal and test sets
indval =  find(lot(:,k)==i);
indcal =  find(lot(:,k)~=i);

// Calling of the calibration+test function:
if ~isempty(indval) &  ~isempty(indcal) & flag=='L' then  //correction v.0.6


[s,pr,biaisjc,last_argout]=caltestxx(x(indcal,:),y(indcal,:),x(indval,:),y(indval,:),func,cent,varargin(:));
dof=last_argout.dof;
clear biaisjc bcoeffjc tscoresjc ploadsjc  last_argout // dof is kept
elseif ~isempty(indval) &  ~isempty(indcal) & flag=='NL' then
[s,pr]=caltestxx(x(indcal,:),y(indcal,:),x(indval,:),y(indval,:),func,cent,varargin(:));
dof=[];
end


if (i==1 & k==1) | ~isdef('prcv') then // if first call
press = s.*0;
sz = size(pr);
sz=sz(2:$);
prcv = zeros(mx,prod(sz));
end;
// the following works whatever the size of the arguments returned by caltestxx
press = press + (s.^2) * length(indval);
prcv(indval,:) = prcv(indval,:) + matrix(pr(:),length(indval),prod(sz));
//end

end
end;
// reshape of the output arguments
secv = sqrt(press/(mx*ni));
predcv = matrix(prcv(:)./ni,[mx,sz]);

endfunction



