function y=nns_simul(wh,wo,x)
    
    // mise de la fonction c_nnsimul au format Div
    
    wh=div(wh);
    wo=div(wo);
    x=div(x);
    
    y.d=c_nnsimul(wh.d,wo.d,x.d);
    y.i=x.i;
    y.v=wo.v;
    
    y=div(y);
    
    
endfunction
