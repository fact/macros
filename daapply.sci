function [res_appli]=daapply(model,xv,varargin);
    
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013  
  
  // applique un modele d'analyse discrimiante sur un nouveau jeu de donnees
  
  // model: un modele obtenu avec fda, plsda, covsel_da p.ex.
  // xv:    un nouveau jeu de donnees; une matrice 
  // (yv):  les classes associees a xv; un vecteur conjonctif ou une matrice disjonctive
  
  // res_appli: une structure avec les champs suivants:
  // res_appli.ypred:   les classes predites pour xv en utilisant model
  
  // et si yv est donne: 
  // res_appli.conf_test_nobs
  // res_appli.conf_test
  // res_appli.err_test
  // res_appli.errbycl_test
  // res_appli.notclassed
  // res_appli.notclassed_bycl
  // res_appli.xcal_scores
  // res_appli.xval_scores
  
  
  // --------------------------------------------------------------------------
  // CAS DES METHODES DE REGRESSION 
  // --------------------------------------------------------------------------
  
  // classtestxx calcule le modèle ce qui peut être long; on a déjà
  // ces infos (loadings, metric) donc on utilise plutôt memberpred  
  
  if ~isfield(model,'knn') then
 
    xv1=div(xv);
    xval=xv1.d;
    [n,q]=size(xval);
    table1=model.conf_cv(1).d;
    nbcl=size(table1,1);
    
    xcal=model.xcal.d;
    [nxc,qxc]=size(xcal);
    ycal=model.ycal.d;
    ycal=conj2dis(ycal);
    
    [nyc,qyc]=size(ycal);
    yval0=zeros(n,qyc);
  
    if q~=qxc then 
       error('not the same numbers of variables')
    end

    // extraction des infos du modèle
    metric=model.classif_metric;
    scale=model.scale;
    if isfield(model,'classif_opt') then
       class=model.classif_opt;
    else
       class=0;
    end
    seuil=model.threshold;
    func=model.method;
  
    xpr=model.rloadings;
    
    // identification 2D/3D                
    if max(size(size(xpr.d))) ==2 then
        flag_dim='2D';
        lv=size(model.err.d,1);
    elseif max(size(size(xpr.d))) ==3 then 
        flag_dim='3D';
        lv=size(xpr.d,2);
        nbvar=size(xpr.d,3);
    end
 
    // gestion de scaling: centrage / centrage-standardisation
    vm = mean(xcal,'r');
    vst = stdev(xcal,'r');
 
    for i=1:size(xcal,2);          //gestion des variances nulles... 
      if vst(i)<10*%eps then vst(i)=0.0000000001;
      end
    end
 
    if scale=='cs' then
      xcal =  ( xcal - ones(size(xcal,1),1)  * vm ) ./ ( ones(size(xcal,1),1) * vst );
      xval = ( xval - ones(n,1) * vm ) ./ ( ones(n,1) * vst );
     
    elseif scale=='c' then
      xcal =  xcal  - ones(size(xcal,1),1) * vm;
      xval = xval - ones(n,1) * vm;
    end;

    // ---- ce code se retrouve dans classtestxx ------------------------------ 
 
    // prédiction pour le jeu de test
    if flag_dim=='2D' then
        yp=zeros(n,nbcl,lv); 
    elseif flag_dim=='3D' then
        yp=zeros(n,nbcl,lv,nbvar);
    end
    
    // calcul des scores:    
    if flag_dim=='2D' then
        xcal_scores=xcal*xpr.d;
        xval_scores=xval*xpr.d;
    elseif flag_dim=='3D' then
        xcal_scores=(-1)*ones(size(xcal,1),size(xpr.d,2),size(xpr.d,3));
        for i=1:nbvar;
            xcal_scores(:,:,i)=xcal*xpr.d(:,:,i);
            xval_scores(:,:,i)=xval*xpr.d(:,:,i);
        end
    end
    
    if flag_dim=='2D' then
        for i=1:lv;    
            [pr]=memberpred(xcal_scores(:,1:i),ycal,xval_scores(:,1:i),yval0,metric,class); 

            // décision d'appartenance à une classe: PROBA. MAX avec la classe
            [prm,ind] = max(pr,'c');
            ypred = conj2dis(ind,nbcl,'C');
            ypred(prm<seuil,:) = 0;     // possible que des obs. ne soient rattachées à aucune classe! 
      
            yp(:,:,i)=ypred;
        end
                 
    elseif flag_dim=='3D' then
        for j=1:nbvar;
            for i=1:lv;
                [pr]=memberpred(xcal_scores(:,1:i,j),ycal,xval_scores(:,1:i,j),yval0,metric,class); 
                
                // décision d'appartenance à une classe: PROBA. MAX avec la classe
                [prm,ind] = max(pr,'c');
                ypred = conj2dis(ind,nbcl,'C');
                ypred(prm<seuil,:) = 0;     // possible que des obs. ne soient rattachées à aucune classe! 
                yp(:,:,i,j)=ypred;
            end
        end
    end

    // ------ fin du code commun à classtestxx (ici: pas de calcul de matrice de confusion)-------


    // ----------------------
    // cas où yv est connue: 
    // ----------------------
    if argn(2)==3 then
        yv=varargin(1);         // garder varargin pour knn et les autres modeles 
 
        if type(yv)== 16 then       // 16 = un div
            yv=yv.d;
        end

        // identification de ycal -> matrice disjonctive 
        if type(yv)==1 then
            if isconj(yv)=='T' then
                yval=conj2dis(yv);
            elseif isdisj(yv)=='T' then
                yval=yv;
            end
        end

        if type(yv)==10     // 10 = chaine de caractères                
                yval=str2conj(yv)
                yval=conj2dis(yval);
        end
      
        if flag_dim=='2D' then
            confus=zeros(nbcl,nbcl,lv);
            confus_pcent=confus; 
            err_test=zeros(lv,1);
            err_test_bycl=zeros(lv,nbcl);      
            nonclasse=zeros(lv,1);
            nonclasse_bycl=zeros(lv,nbcl);
      
            for i=1:lv;
                confus_i=yp(:,:,i)'*yval; 
                confus(:,:,i)=confus_i;            
                [confusi_pcent,erri,erribycl,nci,nci_bycl]=conf_obs2pcent(confus_i,sum(yval,'r'));
                confus_pcent(:,:,i)=confusi_pcent;
                err_test(i)=erri;
                err_test_bycl(i,:)=erribycl';
                nonclasse(i)=nci;        
                nonclasse_bycl(i,:)=nci_bycl';
            end
      
        elseif flag_dim=='3D' then 
            confus=zeros(nbcl,nbcl,lv,nbvar);
            confus_pcent=confus; 
            err_test=zeros(lv,nbvar);
            err_test_bycl=zeros(lv,nbcl,nbvar);      
            nonclasse=zeros(lv,nbvar);
            nonclasse_bycl=zeros(lv,nbcl,nbvar);
            
            for j=1:nbvar;
                for i=1:lv;
                    //pause
                    confus_i=yp(:,:,i,j)'*yval; 
                    confus(:,:,i,j)=confus_i;            
                    [confusi_pcent,erri,erribycl,nci,nci_bycl]=conf_obs2pcent(confus_i,sum(yval,'r'));
                    confus_pcent(:,:,i,j)=confusi_pcent;
                    err_test(i,j)=erri;
                    err_test_bycl(i,:,j)=erribycl';
                    nonclasse(i)=nci;        
                    nonclasse_bycl(i,:,j)=nci_bycl';
                end
            end

        end
      
    end


    // ---------------------------------------------
    // sorties
    // ---------------------------------------------
    
    // (1) que matrice de spectres 
    
    // res_appli.ypred
    res_appli.ypred=list();
    if flag_dim=='2D' then
        for i=1:lv;
            ypredi.d=yp(:,:,i);
            ypredi.i=xv1.i;
            ypredi.v='CL'+string([1:1:nbcl])';
            ypredi=div(ypredi);
            res_appli.ypred(i)=ypredi;
        end
    elseif flag_dim=='3D' then 
        
        for i=1:lv;
            ypredi.d=yp(:,:,i,:);
            ypredi.d=matrix(ypredi.d,[size(xval,1),nbcl,nbvar]);
            ypredi.i=xv1.i;
            ypredi.v.v1='CL'+string([1:1:nbcl])';
            ypredi.v.v2='NbVar'+string([1:1:nbvar])';
            ypredi=div(ypredi);
            res_appli.ypred(i)=ypredi;
        end
        
    end
    
    // res_appli.xval_scores
    res_appli.xval_scores.d=xval_scores;
    res_appli.xval_scores.i=xv1.i;
    res_appli.xval_scores.v=model.rloadings.v;
    res_appli.xval_scores=div(res_appli.xval_scores);


    // (2) aussi ytest -> matrices de confusion et d'erreur -------------------
    if  argn(2)>=3 then
        
        // conf_test_nobs
        res_appli.conf_test_nobs=list();
        if flag_dim=='2D' then
            for i=1:lv;
                confus_i.d=confus(:,:,i); 
                confus_i.i='pred_'+ string([1:1:nbcl]');
                confus_i.v='ref_'+ string([1:1:nbcl]');
                confus_i=div(confus_i);
                res_appli.conf_test_nobs(i)=confus_i;
            end      
        elseif flag_dim=='3D' then 
            for j=1:nbvar; 
                for i=1:lv;
                    confus_i.d=confus(:,:,i,:);
                    confus_i.d=matrix(confus_i.d,[nbcl,nbcl,nbvar]); 
                    confus_i.i='pred_'+ string([1:1:nbcl]');
                    confus_i.v.v1='ref_'+ string([1:1:nbcl]');
                    confus_i.v.v2=ypredi.v.v2;
                    confus_i=div(confus_i);
                    res_appli.conf_test_nobs(i)=confus_i;
                end
            end  
        end 
      
        // conf_test
        res_appli.conf_test=list();
        if  flag_dim=='2D' then
            for i=1:lv;
                conftesti.d=confus_pcent(:,:,i);
                conftesti.i=confus_i.i;
                conftesti.v=confus_i.v;
                conftesti=div(conftesti);
                res_appli.conf_test(i)=conftesti;
            end  
        elseif flag_dim=='3D' then 
            for j=1:nbvar; 
                for i=1:lv;
                    conftesti.d=confus_pcent(:,:,i,:);
                    conftesti.d=matrix(conftesti.d,[nbcl,nbcl,nbvar]); 
                    conftesti.i=confus_i.i;
                    conftesti.v=confus_i.v;
                    conftesti=div(conftesti);
                    res_appli.conf_test(i)=conftesti;
                end
            end  
        end 
      
        // err_test
        res_appli.err_test.d=err_test;
        res_appli.err_test.i='DV'+string([1:1:lv]');
        if  flag_dim=='2D' then
            res_appli.err_test.v='error of prediction, p.cent';
        elseif flag_dim=='3D' then
            res_appli.err_test.v=ypredi.v.v2; 
        end
        res_appli.err_test=div(res_appli.err_test);
        
        // errbycl_test
        res_appli.errbycl_test.d=err_test_bycl;
        res_appli.errbycl_test.i=res_appli.err_test.i; 
        res_appli.errbycl_test.v=ypredi.v;
        res_appli.errbycl_test=div(res_appli.errbycl_test); 
        
        // notclassed
        res_appli.notclassed.d=nonclasse;
        res_appli.notclassed.i=res_appli.err_test.i;
        if  flag_dim=='2D' then
            res_appli.notclassed.v='not classed, p.cent of all the observations';
        elseif flag_dim=='3D' then
            res_appli.notclassed.v=ypredi.v.v2;
        end
        res_appli.notclassed=div(res_appli.notclassed);
       
        //notclassed_bycl
        res_appli.notclassed_bycl.d=nonclasse_bycl;
        res_appli.notclassed_bycl.i='DV'+string([1:1:lv])';
        res_appli.notclassed_bycl.v=ypredi.v;
        res_appli.notclassed_bycl=div(res_appli.notclassed_bycl);
      
    end
  



  //---------------------------------------------------------------------------
  // CAS DES AUTRES METHODES: 
  //  knn pour l'instant  
  // --------------------------------------------------------------------------
  elseif isfield(model,'knn') then 
    
    
    res = applyknnda(model,xv,varargin(:));
    

    // sorties  
    res_appli.ypred=[];
    res_appli.ypred=res.yclass; 

    if argn(2)==3 then
      res_appli.conf_test_nobs=[]; 
      res_appli.conf_test_nobs=res.conf; 
      res_appli.err_pcent=res.err_pcent; 
    end

  end
    

     
endfunction
